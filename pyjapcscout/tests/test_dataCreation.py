'''
Idea would be to "fake" all types of JAPC parameters types starting from a python dictionary,
then use data conversion functions to go to JAPC-like parameters, and back to python-like parameters 
and see that we get back the same values/structure as at the beginning...
'''

import numpy as np
from pyjapcscout import PyJapcScoutConverter
import pytest

def simpleTest(myValue):
    myDataConverter = PyJapcScoutConverter()
    aux = myValue == myDataConverter._convertValueValueToPy(myDataConverter.PyToJValue(myValue, None))
    if isinstance(aux, np.ndarray):
        assert aux.all()
    else:
        assert aux


def functionTest(myValue):
    myDataConverter = PyJapcScoutConverter()
    roundTrip = myDataConverter._convertValueValueToPy(myDataConverter.PyToJValue(myValue, None))
    if isinstance(myValue, list):
        # old way.... so make it ~new way...
        myValue = np.array(myValue)
    if isinstance(myValue, np.ndarray):
        myValue = myValue.flatten()
        for i in range(len(myValue)):
            if 'JAPC_FUNCTION' in myValue[i].keys():
                assert(roundTrip[i]['JAPC_FUNCTION']['X'] == myValue[i]['JAPC_FUNCTION']['X']).all()
                assert(roundTrip[i]['JAPC_FUNCTION']['Y'] == myValue[i]['JAPC_FUNCTION']['Y']).all()
            else:
                assert(roundTrip[i]['JAPC_FUNCTION']['X'] == myValue[i]['X']).all()
                assert(roundTrip[i]['JAPC_FUNCTION']['Y'] == myValue[i]['Y']).all()
    else:
        if 'JAPC_FUNCTION' in myValue.keys():
            assert(roundTrip['JAPC_FUNCTION']['X'] == myValue['JAPC_FUNCTION']['X']).all()
            assert(roundTrip['JAPC_FUNCTION']['Y'] == myValue['JAPC_FUNCTION']['Y']).all()
        else:
            assert(roundTrip['JAPC_FUNCTION']['X'] == myValue['X']).all()
            assert(roundTrip['JAPC_FUNCTION']['Y'] == myValue['Y']).all()

def test_data_conversion(jvm):
    # DOUBLE
    simpleTest(np.float64(1.1))
    simpleTest(np.array([1.5, 3.2], dtype=np.float64))
    simpleTest(np.array([[1.5], [3.2]], dtype=np.float64))
    simpleTest(np.array([[1.5, 1.1], [4, 5], [1, 2]], dtype=np.float64))
    # FLOAT
    simpleTest(np.float32(1.1))
    simpleTest(np.array([1.5, 3.2], dtype=np.float32))
    simpleTest(np.array([[1.5], [3.2]], dtype=np.float32))
    simpleTest(np.array([[1.5, 1.1], [4, 5], [1, 2]], dtype=np.float32))
    # LONG
    simpleTest(np.int64(1))
    simpleTest(np.array([1, 12, 4, 5], dtype=np.int64))
    simpleTest(np.array([[1], [12], [4], [5]], dtype=np.int64))
    simpleTest(np.array([[1, 12], [4, 5], [1, 2]], dtype=np.int64))
    # INT
    simpleTest(np.int32(1))
    simpleTest(np.array([1, 12, 4, 5], dtype=np.int32))
    simpleTest(np.array([[1, 12], [4, 5], [1, 2]], dtype=np.int32))
    # SHORT
    simpleTest(np.int16(1))
    simpleTest(np.array([1, 12, 4, 5], dtype=np.int16))
    simpleTest(np.array([[1, 12], [4, 5], [1, 2]], dtype=np.int16))
    # BYTE
    simpleTest(np.int8(1))
    simpleTest(np.array([1, 12, 4, 5], dtype=np.int8))
    simpleTest(np.array([[1, 12], [4, 5], [1, 2]], dtype=np.int8))
    # BOOL
    simpleTest(True)
    simpleTest(np.array([True, False, True, True], dtype=np.bool_))
    simpleTest(np.array([[True, False], [True, False], [True, True]], dtype=np.bool_))
    # STRING
    simpleTest('This is string')
    simpleTest(['my', 'list']) # old way < May 2021
    simpleTest(np.array(['my', 'list'], dtype=np.str_))
    simpleTest(np.array([['my'], ['list'],['long']], dtype=np.str_))
    simpleTest(np.array([['my', 'list'], ['of', 'more'], ['val', 'string']], dtype=np.str_))

    # DISCRETE_FUNCTION
    functionTest({'JAPC_FUNCTION': {'X':[2.1, 8], 'Y':[33.4, 22.2]}}) # old way < May 2021
    functionTest({'JAPC_FUNCTION': {'X':np.array([2.1, 8]), 'Y':np.array([33.4, 22.2])}})
    functionTest(np.array([{'JAPC_FUNCTION': {'X':np.array([2.1, 8]), 'Y':np.array([33.4, 22.2])}}, {'JAPC_FUNCTION': {'X':np.array([4.1, 123]), 'Y':np.array([12, 14.])}}], dtype=object))
    functionTest({'JAPC_FUNCTION': {'X':[2.1, 8], 'Y':[33.4, 22.2]}}) # old way < May 2021
    functionTest(np.array([{'JAPC_FUNCTION': {'X':[2.1, 8], 'Y':[33.4, 22.2]}}, {'JAPC_FUNCTION': {'X':[4.1, 123], 'Y':[12, 14]}}], dtype=object))
    functionTest({'X':[2.1, 8], 'Y':[33.4, 22.2]}) # old way < May 2021
    functionTest(np.array([{'X':[2.1, 8], 'Y':[33.4, 22.2]}, {'X':[4.1, 123], 'Y':[12, 14]}], dtype=object)) # old way < May 2021
    functionTest([{'X':[2.1, 8], 'Y':[33.4, 22.2]}, {'X':[4.1, 123], 'Y':[12, 14]}]) # old way < May 2021


# This still does not work... ([1, 2, 3], ['Item 1', 'Item 2']),

@pytest.mark.parametrize(
    ['my_new_py_value', 'enumset_str'],
    [
        ([0, 1, 2], ['Item 1', 'Item 2']),
        ([1], ['Item 1']),
        (['Item 1', 'Item 2'], ['Item 1', 'Item 2']),
        (['Item 1', 'Item 1'], ['Item 1']),
        ([1, 2, 3], ['Item 1', 'Item 2']),
        ([3], ['Item 1', 'Item 2']),
        (3, ['Item 1', 'Item 2']),
        (0, []),
        ([0], []),
        (['Item 1', 'Item 2'], ['Item 1', 'Item 2']),
        (['Item 1', 'Item 1'], ['Item 1']),
        (['Item 1'], ['Item 1']), 
        ({'strings': ['Item 1', 'Item 3']}, ['Item 1', 'Item 3']),
        ({'strings': np.array(['Item 1', 'Item 3'], dtype='<U6')}, ['Item 1', 'Item 3']),
        ({'codes': np.array([1, 2])}, ['Item 1', 'Item 2']),
        ({'codes': np.array([1, 2, 2, 3, 4])}, ['Item 1', 'Item 2', 'Item 3']),
        ({'JAPC_ENUM_SET': {'codes': [1, 2, 2]}}, ['Item 1', 'Item 2']),
        ({'JAPC_ENUM_SET': {'asLong': 3}}, ['Item 1', 'Item 2']), 
    ]
)
def test_enumSet(jvm, enumset_descriptor, my_new_py_value, enumset_str):
    myConverter = PyJapcScoutConverter()

    # Create a java object and see that all keys are generated as expected
    JNewValue = myConverter.PyToJValue(my_new_py_value, enumset_descriptor)
    assert JNewValue.getEnumItemSet().toString() == '['+', '.join(enumset_str)+']'

    # go back to Python, and check that all keys are re-constructed
    Py_reconverted_value = myConverter._convertValueValueToPy(JNewValue)
    assert set(Py_reconverted_value['JAPC_ENUM_SET']['strings']) == set(enumset_str)

    # Go back to JAVA, this time from a "complete" description of the enum in Python
    JNewNewValue = myConverter.PyToJValue(Py_reconverted_value, enumset_descriptor)
    Py_re_reconverted_value = myConverter._convertValueValueToPy(JNewNewValue)
    assert set(Py_re_reconverted_value['JAPC_ENUM_SET']['strings']) == set(enumset_str)


@pytest.mark.parametrize(
    ['my_new_py_value', 'enum_str'],
    [
        (42, 'ON'),
        ('ON', 'ON'),
        ({'JAPC_ENUM':{'code':np.int64(42)}}, 'ON'),
        ({'code':-5}, 'OFF'),
        ({'string':'OFF'}, 'OFF'),
    ]
)
def test_enum(jvm, std_meaning_enum_descriptor, my_new_py_value, enum_str):
    myConverter = PyJapcScoutConverter()

    # Create a java object and see that all keys are generated as expected
    JNewValue = myConverter.PyToJValue(my_new_py_value, std_meaning_enum_descriptor)
    assert JNewValue.getEnumItem().getString() == enum_str

    # go back to Python, and check that all keys are re-constructed
    Py_reconverted_value = myConverter._convertValueValueToPy(JNewValue)
    assert Py_reconverted_value['JAPC_ENUM']['string'] == enum_str

    # Go back to JAVA, this time from a "complete" description of the enum in Python
    JNewNewValue = myConverter.PyToJValue(Py_reconverted_value, std_meaning_enum_descriptor)
    Py_re_reconverted_value = myConverter._convertValueValueToPy(JNewNewValue)
    assert Py_re_reconverted_value['JAPC_ENUM']['string'] == enum_str


