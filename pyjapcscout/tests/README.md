PyJapcScout testing
===

The tests provided so far (June 2021) are pretty limited, and mainly "copied" from pyjapc.
Testing relies on using mypy for some "special" configurations and to allow for using JAPC 
outside of the control system using "cern.japc.ext.mockito.JapcMock".
See https://gitlab.cern.ch/acc-co/japc/japc-core/blob/develop/japc-ext-mockito2/src/java/cern/japc/ext/mockito/demo/Demo.java

To set this up, I copied several files from [pyjapc](https://gitlab.cern.ch/scripting-tools/pyjapc/-/tree/master):
- pyjapcscout/tests/mypy.ini
- pyjapcscout/tests/conftest.py
and edited `setup.py` 'package_data' to load `mypy.ini`

Initially, I had to struggle to make the testing working, so I had to replicate the environment
on my computer, outside of TN, to see what was going wrong.
Here the final step I used for my debugging (June 2021):

- Make sure you have the right python and openjdk. If you use conda, this can be done as:
```
conda install python=3.7
conda install openjdk
```

- create a virtual environment
```
python -m venv ./my-venv
source ./my-venv/bin/activate
```

- Add accpy repository, so you can install whatever is needed from the CERN custom packages
```
pip install git+https://gitlab.cern.ch/acc-co/devops/python/acc-py-pip-config
```

- I also had to install jpype, for some reason.
python -m pip install 'jpype1==1.1.*' 

- Now, finally starting to work on the package:
    - start cloning repository:
    ```
    git clone https://gitlab.cern.ch/abpcomputing/sandbox/pyjapcscout.git
    cd pyjapcscout
    ```

    - checkout the branch you are developing onto
    ```
    git checkout add_sanity_check
    ```

    - install the "test" version (with its dependencies, see setup.py)
    ```
    pip install -e '.[test]'
    ```

    - test it using pdb -> it will stop at at the first failing `assert` (you can add `assert 0` where you want)
      and this allows you to debug what is going wrong
    ```
    pytest ./pyjapcscout --mypy  --pdb
    ```
