### working example, but not working with parquet
from pyjapcscout import PyJapcScout


def test_acquisition(japc, japc_mock):
    ''' try to get a single parameter and check it returns the expected value
    '''
    parameter  = 'TEST/TestProperty'
    selector   = 'SCT.USER.SETUP'
    mockParameter = japc_mock.mockParameter(parameter)
    mockSelector  = japc_mock.sel(selector)
    mockError     = japc_mock.pe('Some error.')

    # create a round buffer of values (and errors) when GET values
    japc_mock.whenGetValueThen(
        mockParameter, mockSelector, japc_mock.acqVal(mockParameter, 42, 0), japc_mock.acqVal(mockParameter, 43, 0), mockError)
    assert japc.getSimpleValue(parameter, selectorOverride = selector) == 42
    assert japc.getSimpleValue(parameter, selectorOverride = selector) == 43
    assert 'Some error.' in japc.getValues(parameter, selectorOverride = selector)[parameter]['exception']
    assert japc.getValues(parameter, selectorOverride = selector)[parameter]['value'] == 42
