"""
High-level tests for the  package.

"""

import pyjapcscout


def test_version():
    assert pyjapcscout.__version__ is not None
