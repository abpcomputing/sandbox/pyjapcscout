"""
Documentation for the pyjapcscout package

"""

from ._version import version as __version__

from ._japc_scout import PyJapcScout
from ._japc_scout import PyJapcExt
from ._japc_converter import PyJapcScoutConverter

__cmmnbuild_deps__ = [
    {"product": "ctf-customjars-matlab", "groupId": "cern.ctf"}, 
    {"product": "nxcals-extraction-api-thin", "groupId": "cern.nxcals"}, 
    {"product": "nxcals-metadata-api", "groupId": "cern.nxcals"}, 
    {"product": "cmw-datax", "groupId": "cern.cmw"}, 
]

# Make sure that the PyJapc class is seen to be coming from the top level
# module, rather than the private "_japc" one.
PyJapcScout.__module__ = __name__
