"""
PyJapcScout is a dressing of PyJapc that uses a different group subscription strategy
and implements some "standard" methods to save data.

"""

from tkinter.messagebox import NO
from pyjapc import PyJapc
import pyjapc
import datascout
import jpype as jp
from ._japc_converter import PyJapcScoutConverter
import six
import sys
import logging
import traceback
from datetime import datetime
import cmmnbuild_dep_manager
#import pjlsa
import pyccda
import numpy as np

######################################################
# Single parameter acquisition multiple time per BP
######################################################

class PyJapcScout(object):
    def __init__(self, incaAcceleratorName = None, defaultSelector = "", noSet=False, timeZone="utc", unixtime=True,
                 logLevel=None, enableCCSDescriptorsIfNoInca=True, **kwargs):
        """Initialise a PyJapcScout object.

        This will be the entry point to SET, GET, MONITOR any JAPC-reachable device you might want to use in your
        Python script or application.

        .. warning::
        
            IMPORTANT: In principle, only ONE PyJapcScout object should be initialised, as this initialises the java virtual machine
            as well as all JAPC and eventually InCA infrastructure.
            Subsequent creation of PyJapcScout will also work, but you will not be able to change the InCA-ification, InCA server,
            or JAPC infrastructure behavior.

        The syntax is very similar to plane PyJapc.

        Args:
             incaAcceleratorName (str): See PyJapc docs: it can be one of typical accelerator ['LNA', 'PSB', ...]
                or 'auto' (to guess it from the provided *defaultDescriptor*)
                or None (default) for non-InCA application
             defaultSelector (str): This selector will be the default for any call of GET/SET/MONITOR if without the need of
                specifying a *selectorOverride* every time.
                It can be later on changed with method **setDefaultSelector**
             noSet (bool): This options is passed down to PyJAPC to avoid doing any real SET to any device. (default=False)
             timeZone (str): (default="utc")
             unixtime (str): If to use unixtime timestamps instead of *datetime* (default=True)
             logLevel (int): Sets the initial logging level for both JAPC, PyJapc, PyJapcScout.
                It can be later changed with static method **setLoggingLevel** (default=None, i.e. "WARNING")
             enableCCSDescriptorsIfNoInca (bool): In case of non-InCA applications (incaAcceleratorName=None), it is used to
                enable the CCDA as source for the ParameterValueDescriptors. (default=True)
        """

        # create my own logger
        logging.basicConfig()
        self.log = logging.getLogger(__package__)

        ############################
        # ideally one could just do
        #  > self._mainPyJapc = PyJapc(selector=defaultSelector, incaAcceleratorName=incaAcceleratorName, noSet=noSet,
        #               timeZone=timeZone, logLevel=logLevel, enableCCSDescriptorsIfNoInca=enableCCSDescriptorsIfNoInca)
        # if "enableCCSDescriptorsIfNoInca" will be added to plane PyJAPC.
        # For the time being, just assume PyJAPC does not have this feature, so force it here like this:
        if enableCCSDescriptorsIfNoInca:
            PyJapcScout.enableCCSDescriptors(True)
        self._mainPyJapc = PyJapc(selector=defaultSelector, incaAcceleratorName=incaAcceleratorName, noSet=noSet, timeZone=timeZone, logLevel=logLevel)
        ############################
        
        # Add pjlsa support
        ''' presently had to re-implement most of pjlsa method. Will come back to that.
        if incaAcceleratorName == None:
            self.log.warning('If no inCA server is specified, LSA-related functionalities are disabled!')
            self._mainpjlsa = None
        else:
            self._mainpjlsa = pjlsa.LSAClient(server=incaAcceleratorName.lower())
        '''
        # Add pyccda support
        self._mainpyccda = pyccda.SyncAPI()

        # Add NXCALS support via thin API
        System = jp.JClass("java.lang.System")
        System.setProperty("service.url",
                           "https://cs-ccr-nxcals5.cern.ch:19093,https://cs-ccr-nxcals5.cern.ch:19094,https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals6.cern.ch:19094,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19094,https://cs-ccr-nxcals8.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19094")

        self._unixtime = unixtime
        self.setDefaultSelector(defaultSelector)

        # create here object that handle Java <-> Python conversion for this object
        self._dataConverter = PyJapcScoutConverter()
    
    @staticmethod
    def enableCCSDescriptors(enable=True):
        """ Enables the use of japc-svc-ccs to find parameter descriptors. 
        
        This is only useful if you **will not** InCAify your session.
        See https://wikis.cern.ch/display/JAPC/JAPC+Descriptors
        
        .. warning::
        
            NOTE: this must be enabled **BEFORE** initialising the JAPC infrastructure!!
            I.e. even before you build PyJAPC and/or PyJAPCScout!

        Args:
            enable (bool): default=True
        """
        # make sure the JVM is started
        cmmnbuild_dep_manager.Manager().jvm_required()

        System = jp.JClass("java.lang.System")
        ParameterFactory = jp.JClass("cern.japc.core.factory.ParameterFactory")
        if enable:
            System.setProperty(ParameterFactory.SYSPROP_JAPC_CCS_DESCRIPTORS_ENABLED, "true")
        else:
            System.setProperty(ParameterFactory.SYSPROP_JAPC_CCS_DESCRIPTORS_ENABLED, "false")
    
    @staticmethod
    def enableWildcardSelectors(enable=True):
        """ Enable `*` Wildcard selectors (see https://wikis.cern.ch/display/JAPC/Wildcard+Selectors)

        .. warning::
        
            NOTE: this must be enabled **BEFORE** initialising the JAPC infrastructure!!
            I.e. even before you build PyJAPC and/or PyJAPCScout!

        Args:
            enable (bool): default=True
        """

        # make sure the JVM is started
        cmmnbuild_dep_manager.Manager().jvm_required()

        System = jp.JClass("java.lang.System")
        if enable:
            System.setProperty("default.wildcard.subscription.on", "true")
        else:
            System.setProperty("default.wildcard.subscription.on", "false")

    @staticmethod
    def setLoggingLevel(logLevel=None):
        """Configures the log level for both python and java loggers

        Args:
            logLevel (int): logging level. See https://docs.python.org/3/library/logging.html#levels
                default=None, which will use standard "WARNING" values.
        """
        # look at python logger of pyjapcscout
        logging.basicConfig()
        log = logging.getLogger(__package__)
        if logLevel is not None:
            log.setLevel(logLevel)
        else:
            log.setLevel('WARNING')

        # look at python logger of pyjapc
        log = logging.getLogger(pyjapc.__package__)
        if logLevel is not None:
            log.setLevel(logLevel)
        else:
            log.setLevel('WARNING')

        # look at java logger
        log4j = jp.JPackage('org').apache.log4j
        if log4j.BasicConfigurator is not None and callable(log4j.BasicConfigurator.configure):
            log4j.BasicConfigurator.configure()
        if logLevel is not None:
            log4j.Logger.getRootLogger().setLevel(log4j.Level.toLevel(logLevel))
        else:
            log4j.Logger.getRootLogger().setLevel(log4j.Level.WARN)

    @staticmethod
    def simpleDataSanityCheck(data, *args):
        """ An example of simple data check that ensures you got no data with an exception
        
        Args:
            data (dict): data coming from pyjapcscout acquisition, i.e. the usual dict of dicts
        
        Returns:
            (bool): True if `data` has no exceptions, False otherwise
        """
        for key in data:
            if data[key]['exception'] != '':
                return False
        return True
    
    def setDefaultSelector(self, selector):
        """
        Set the default Selector used for any GET/SET/MONITOR of parameters without the need of specifying a
        *selectorOverride* every time.

        Args:
             selector (str): desired selector as string
        """
        self._defaultSelector = selector

    def rbacLogin(self, username=None, password=None, loginDialog=False, readEnv=True):
        """
        Just an wrapper of PyJapc  rbacLogin() function. See PyJapc doc.

        Args:
             username (str): (default = None)
             password (str): (default = None)
             loginDialog (bool): (default = False)
             readEnv (bool): (default = True)
        """
        self._mainPyJapc.rbacLogin(username=username, password=password, loginDialog=loginDialog, readEnv=readEnv)
    
    def rbacLogout(self):
        """Just an wrapper of PyJapc rbacLogout() function. See PyJapc doc.

        """
        self._mainPyJapc.rbacLogout()
    
    # A couple of functions for the simplest ever cases: get and set an actual value (e.g. a DC power converter current value)
    def getSimpleValue(self, parameterName, selectorOverride = None, completeOutput=False):
        """
        Asynchronous GET of a given JAPC parameter.

        Args:
             parameterName (str): The parameter you want to GET as string.
             selectorOverride (str): The USER selector you want to use (default=None, i.e. use defaultSelector specified in PyJapcScout instance)
             completeOutput (bool): If True, it will return a dict with 'value' and possibly other keys like 'header' and 'exception', the latter containing timestamps etc... (default=False)
        
        Returns:
             (numpy or dict) : Python interpretation of the data provided via JAPC
        """
        values = self.getValues([parameterName], selectorOverride)
        if completeOutput:
            return values[parameterName]
        else:
            return values[parameterName]['value']

    def setSimpleValue(self, parameterName, value, selectorOverride = None, dataFilter = None, forceCCSDescriptorSource = False):
        """
        Asynchronous SET of a given python value to a JAPC parameter.

        Args:
             parameterName (str): The parameter you want to SET as string.
             value: The Python value you want to SET
             selectorOverride (str): The USER selector you want to use (default=None, i.e. use defaultSelector specified in PyJapcScout instance)
             dataFilter (dict): a dictionary with suitable data filter for your SET (expert use. default=None)
             forceCCSDescriptorSource (bool): force to use CCS descriptor, no matter how JAPC was configured (or InCAified)
                to obtain the parameter value descriptor.
        """
        # create a temporary dict object
        auxData = {parameterName: {'value': value}}
        # use wider method
        self.setValues([parameterName], auxData, selectorOverride, dataFilter, forceCCSDescriptorSource)

    def getValues(self, parameterNames, selectorOverride = None):
        """Get the latest value of a list of parameters on a given selector

        The output structure is a dict of dicts in which all **value**, **header**, and eventually **exception** are
        given for each requested parameter.

        Args:
            parameterNames (str or list[str]): list of parameters
            selectorOverride (str): default = None
        
        Returns:
            (dict): dictionary with all data from the required parameters with header data
        """
        # Decide which selector to use:
        if selectorOverride == None:
            selector = self._defaultSelector
        else:
            selector = selectorOverride
        
        # force parameterNames to be a list
        if isinstance(parameterNames, six.string_types):
            parameterNames = [parameterNames]

        # create a dict object and fill it
        output = dict()
        for parameterName in parameterNames:
            try:
                # Make a standard get from local PyJapc object
                value = self._mainPyJapc.getParam(parameterName, noPyConversion = True, timingSelectorOverride = selector)
                # convert
                value = self._dataConverter.JAcquiredValueToPy(value, unixtime=self._unixtime)
            except Exception as e:
                self.log.error('PyJapcScout:: did not manage to GET [{0}]\n{1}'.format(
                    parameterName, traceback.format_exc()))
                sys.stdout.flush()
                value = {'value':'', 'exception':str(e), 'header':''}

            # store
            output[parameterName] = value
        return output

    def findDevicesByName(self, pattern, operationalOnly=True, simplifiedOutput=True, includeAlias=True, verbose=True):
        """Get list of devices matching given pattern 

        Args:
            pattern (str): the device name (or alias) pattern you are looking for. Wild character is "*".
            operationalOnly (bool): match only operational devices (default=True)
            simplifiedOutput (bool): just return a list of matching devices (default=True)
            includeAlias (bool): look both for device name or alias (default=True)
            verbose (bool): print the devices found with some minimal information (default=True)
        
        Returns:
            a list of devices matching the request or a dict with a full class-like structure with all informations
        """
        #See https://acc-py.web.cern.ch/gitlab/controls-configuration-service/controls-configuration-data-api/accsoft-ccs-ccda/docs/stable/examples.html#pyccda-examples
        if includeAlias:
            devices = self._mainpyccda.Device.search('name=="' + pattern + '",alias=="' + pattern + '"')
        else:
            devices = self._mainpyccda.Device.search('name=="' + pattern + '"')

        output = dict()
        for device in devices:
            if operationalOnly and device.state != 'operational':
                continue
            #output[device.name] = device._to_dict()
            output[device.name] = device
            if verbose:
                print(f'{device.name} [{device.device_class_info.name} v{device.device_class_info.version}]: {device.description}')
            #if simplifiedOutput:
            #    output[device.name] = {'name':device.device_class_info.name, 'version':device.device_class_info.version}
            #else:
            #    output[device.name] = device._to_dict()
        if simplifiedOutput:
            return list(output.keys())
        else:
            return output

    def findDeviceClassByName(self, pattern, operationalOnly=True, simplifiedOutput=True, verbose=True):
        """Gets a list of device classes which name match the given pattern

        Args:
             pattern (str): the class name pattern you are looking for. Wild character is "*".
             operationalOnly (bool): match only operational devices (default=True)
             simplifiedOutput (bool): just return a list of matching devices (default=True)
             verbose (bool): print the classes found with some minimal information (default=True)
        Returns:
             (list): a list of classes matching the request or a dict with a full class-like structure with all informations
        """
        deviceclasses = self._mainpyccda.DeviceClassInfo.search('name=="' + pattern + '"')
        output = dict()
        for deviceclass in deviceclasses:
            if operationalOnly and deviceclass.state != 'operational':
                continue
            if deviceclass.name not in output:
                output[deviceclass.name] = dict()
            output[deviceclass.name][deviceclass.version] = deviceclass
            if verbose:
                print(f'{deviceclass.name} v{deviceclass.version} [{deviceclass.implementation}]: {deviceclass.description}')
        if simplifiedOutput:
            return list(output.keys())
        else:
            return output

    def findDeviceClassProperties(self, name, version=None, operationalOnly=True, simplifiedOutput=True, verbose=True):
        """Get list of properties for a given device class and version

        Args:
            name (str): the class name (e.g. "LTIM") 
            version (str): the class version as string (e.g. "6.1.0") (default=None: will return the last available class version)
            operationalOnly (bool): match only operational parameters (default=True)
            simplifiedOutput (bool): just return a list of the (operational) parameters (default=True)
            verbose (bool): print the parameters and their fields with some minimal information (default=True)
        
        Returns:
            (list): a list of devices matching the request
        """
        # See https://acc-py.web.cern.ch/gitlab/controls-configuration-service/controls-configuration-data-api/accsoft-ccs-ccda/docs/stable/api_docs/pyccda.async_models.DeviceClass.find.html#

        # if version is not specified, then get the latest one
        if version is None:
            all_versions_data = self.findDeviceClassByName(name, operationalOnly=operationalOnly, simplifiedOutput=False,
                                                           verbose=False)
            if operationalOnly and name not in all_versions_data:
                self.log.warning(f'I did not find an operational class named {name}... I will try with non-operational ones...')
                all_versions_data = self.findDeviceClassByName(name, operationalOnly=False, simplifiedOutput=False,
                                                               verbose=False)
            if name not in all_versions_data:
                raise ValueError(f'I cannot find the class named {name}')

            all_versions = list(all_versions_data[name].keys())
            all_versions.sort()
            version = all_versions[-1]

        deviceClass = self._mainpyccda.DeviceClass.find(name=name, version=version)
        if verbose:
            print(f'--- Properties for class name={name} version={version} :')
        output = dict()
        for prop in deviceClass.device_class_properties:
           if operationalOnly and prop.visibility != 'operational':
               continue
           #output[prop.name] = prop._to_dict()
           output[prop.name] = prop
           if verbose:
               print(f' {prop.name}: {prop.description}')
               for field in prop.data_fields:
                  print(f'  {field.name} [{field.data_type}]: {field.description}')
                  if field.data_type == 'enum':
                      for enum in field.property_field_enums:
                          print(f'   [{enum.value}] {enum.enum_symbol}: {enum.value_meaning}')
        if simplifiedOutput:
            return list(output.keys())
        else:
            return output

    @staticmethod
    def getConfiguredLSAServer(raiseError=True):
        '''
        Just tries to understand if LSA is already configure, or not...

        Returns:
            (RuntimeError): raise a RuntimeError if LSA has been already configured for some server
        '''
        # not a static function just to make sure Java is already configured by PyJapc...
        System = jp.JClass("java.lang.System")
        configuredServer = System.getProperty("lsa.server")
        if (configuredServer is None):
            if raiseError:
                raise RuntimeError("LSA is not configured. You must restart python and InCAify your pyjapcscout for using LSA-related features...")
            else:
                return configuredServer
        else:
            return configuredServer.upper()

    def getCycleNameFromUser(self, selector):
        '''
        Returns the cycle that is presently mapped to a given user

        Args:
             selector (str): string selector name
        
        Returns:
             (str): cycle name
        '''
        # see https://gitlab.cern.ch/scripting-tools/pjlsa and https://gitlab.cern.ch/scripting-tools/MATLAB/matlabJAPC/-/blob/master/matlabLib/matlabLSA.m
        self.getConfiguredLSAServer()
        cern = jp.JPackage("cern")
        serviceLocator = cern.lsa.client.ServiceLocator
        contextService = serviceLocator.getService(cern.lsa.client.ContextService)
        context = contextService.findDrivableContextByUser(selector)
        if context is not None:
            return context.toString()
        else:
            return None

    def getUserFromCycleName(self, cycleName):
        '''
        Returns the user (PLS) where a given cycle name is mapped to (if any)

        Args:
             cycleName (str): string name of the cycle
        
        Returns:
             (str): name of the user
        '''
        # see https://gitlab.cern.ch/scripting-tools/pjlsa and https://gitlab.cern.ch/scripting-tools/MATLAB/matlabJAPC/-/blob/master/matlabLib/matlabLSA.m
        self.getConfiguredLSAServer()
        cern = jp.JPackage("cern")
        serviceLocator = cern.lsa.client.ServiceLocator
        contextService = serviceLocator.getService(cern.lsa.client.ContextService)
        user = contextService.findStandAloneCycle(cycleName)
        if user is not None:
            return user.getUser()
        else:
            return None

    def findLSAParametersByName(self, pattern):
        '''
        Look for LSA parameters mathing the given pattern. Note that wildcard character is '%'

        Args:
             pattern (str): pattern to look for
        
        Returns:
             (list): sorted list of parameter names
        '''
        self.getConfiguredLSAServer()

        cern = jp.JPackage("cern")
        reqBuilder = cern.lsa.domain.settings.factory.ParametersRequestBuilder()
        serviceLocator = cern.lsa.client.ServiceLocator
        parameterService = serviceLocator.getService(cern.lsa.client.ParameterService)

        request = reqBuilder.byParameterNamePattern(pattern)
        parameters = parameterService.findParameters(request)
        return sorted([pp.getName() for pp in parameters])

    def getTrims(self, parameterNames, selectorOverride = None, cycleOverride = None, referenceTime_ns = None, maxAge_s=None, part='value', returnAllRecords=False):
        '''
        Returns the last trims *before* given referenceTime_ns (or now)

        Args:
             parameterNames (list): list of string parameter names to look for in LSA
             selectorOverride (str): selector where to look for a trim. (default=None)
             cycleOverride (bool): cycle name to use (it has priority over selector) (default=None)
             referenceTime_ns (int): reference time (as timestamp in ns) with respect to when to look for a trim
             maxAge_s (int): Max age in s with respect to `referenceTime_ns` where to look for data. (default=None, i.e. anytime in the past wrt referenceTime),
             part (str): which part of the trim: it can be ['value', 'target', 'correction'] (default='value')

        Returns:
             the value found in the trim history as python object compatible with datascout package
        '''
        # force parameterNames to be a list
        if isinstance(parameterNames, six.string_types):
            parameterNames = [parameterNames]

        output = dict()
        for parameterName in parameterNames:
            output[parameterName] = self.getSimpleTrim(parameterName, selectorOverride = selectorOverride, cycleOverride = cycleOverride, referenceTime_ns = referenceTime_ns, maxAge_s = maxAge_s, part=part, completeOutput=True, returnAllRecords = returnAllRecords)

        return output

    def getSimpleTrim(self, parameterName, selectorOverride = None, cycleOverride = None, referenceTime_ns = None, maxAge_s=None, part='value', completeOutput=False, returnAllRecords = False, MAX_TRIMS=100):
        '''
        Returns the last trim *before* given referenceTime_ns (or now)

        Args:
             parameterName (str): string name of the parameter
             selectorOverride (str): selector where to look for a trim. (default=None)
             cycleOverride (bool): cycle name to use (it has priority over selector) (default=None)
             referenceTime_ns (int): reference time (as timestamp in ns) with respect to when to look for a trim
             maxAge_s (int): Max age in s with respect to `referenceTime_ns` where to look for data. (default=None, i.e. anytime in the past wrt referenceTime),
             part (str): which part of the trim: it can be ['value', 'target', 'correction'] (default='value')
             completeOutput (bool): if to return a more complete dict with additional info on the trim (default=False)
             returnAllRecords (bool): if to return all found records in the time period identified by input parameters (default=False)
             MAX_TRIMS (int): maximum number of trims to be loaded from history (default=100)

        Returns:
             the value found in the trim history as python object compatible with datascout package
        '''
        self.getConfiguredLSAServer()

        # initialise a few objects
        cern = jp.JPackage("cern")
        java = jp.JPackage("java")
        serviceLocator   = cern.lsa.client.ServiceLocator
        contextService   = serviceLocator.getService(cern.lsa.client.ContextService)
        parameterService = serviceLocator.getService(cern.lsa.client.ParameterService)
        settingService   = serviceLocator.getService(cern.lsa.client.SettingService)
        trimService      = serviceLocator.getService(cern.lsa.client.TrimService)
        headerReqBuilder = cern.lsa.domain.settings.TrimHeadersRequestBuilder()
        requestBuilder   = cern.lsa.domain.settings.ContextSettingsRequestBuilder()
        ParameterValueConverter = cern.lsa.domain.exploitation.ParameterValueConverter

        # get parameter
        parameter = parameterService.findParameterByName(parameterName)
        
        # put parameter in a list of parameters..
        parameters = java.util.LinkedList()
        parameters.add(parameter)
        
        # get cycleName and then "Java" cycle where to get trim history from
        if cycleOverride == None:
            if selectorOverride == None:
                selector = self._defaultSelector
            else:
                selector = selectorOverride
            if selector == '':
               cycleName = ''
            else:
               cycleName = self.getCycleNameFromUser(selector)
        else:
            cycleName = cycleOverride
        
        if cycleName == '':
            acceleratorJObj = parameter.getAccelerator()
            cycle = contextService.findResidentNonMultiplexedContext(acceleratorJObj)
            cycleName = cycle.getName()
        else:
            cycle = contextService.findStandAloneCycle(cycleName)
        
        if cycle.isMultiplexed() and not parameter.isMultiplexed():
            raise ValueError('You are trying to GET a Trim for a NON-multiplexed parameter with a multiplexed cycle... Please review your inputs!')
        if not cycle.isMultiplexed() and parameter.isMultiplexed():
            raise ValueError('You are trying to GET a Trim for a multiplexed parameter with a NON-multiplexed cycle... Please review your inputs!')
        

        # build up a request for trim headers according to specified needs
        #beamProcesses = cern.lsa.domain.settings.Contexts.getBeamProcesses(Collections.singleton(drivableContext))
        myHeaderRequest = headerReqBuilder.beamProcesses(cycle.getBeamProcesses()).parameters(parameters)
        # if age provided, then get headers only from a given time.
        if maxAge_s is not None:
            if referenceTime_ns is None:
                startTime_ms = (datetime.now().timestamp() - maxAge_s)*1e3
            else:
                startTime_ms = referenceTime_ns/1e6 - maxAge_s*1e3
            myHeaderRequest = myHeaderRequest.startingFrom(java.util.Date(int(startTime_ms)).toInstant())
        # just to protect on being too slow....
        myHeaderRequest = myHeaderRequest.maxLastTrimsNumber(jp.JInt(MAX_TRIMS))
        # build it up
        trimHeadersRequest = myHeaderRequest.build()
        # get the list of trims
        raw_headers = trimService.findTrimHeaders(trimHeadersRequest)
        #return (myHeaderRequest, startTime_ms, maxAge_s, trimService)

        raw_headers = list(raw_headers)
        # they should already start at asked time (if any), now filter on end time
        if referenceTime_ns is not None:
            referenceTime_java = java.util.Date(int(referenceTime_ns/1e6))
            raw_headers = [
                    th
                    for th in raw_headers
                    if th.getCreatedDate().before(referenceTime_java)
                ]
        if not returnAllRecords and len(raw_headers) > 0:
            # just keep the last one
            raw_headers = [raw_headers[-1]]
        
        # prepare output
        output = []
        for trim_header in raw_headers:
            # build up single request
            myRequest = requestBuilder.standAloneContext(cycle) \
                .parameters(parameters)
            # specify time from headers we got before
            myRequest = myRequest.at(trim_header.getCreatedDate().toInstant())
            # ok, build up request
            myRequest = myRequest.build()
            
            # get parameter settings from trim history according to request
            contextSettings = settingService.findContextSettings(myRequest)
            parameterSettings = contextSettings.getParameterSettings(parameter)
            # prepare output
            partial_output = []
            if parameterSettings is not None:
                _allSettings=parameterSettings.getSettings()
                for _singleSetting in _allSettings:
                    # extract header info
                    creationStamp = np.int64(_singleSetting.getCreationDate().toInstant().toEpochMilli()*10**6)
                    trimId        = np.int64(_singleSetting.getTrimId())
                    beamProcess   = np.str_(_singleSetting.getBeamProcess().toString())
                    description   = np.str_(trim_header.getDescription())
            
                    # extract interesting part
                    if part.upper() == 'VALUE':
                        _singleValue = _singleSetting.getValue()
                    elif part.upper() == 'TARGET':
                        _singleValue = _singleSetting.getTargetValue()
                    elif part.upper() == 'CORRECTION':
                        _singleValue = _singleSetting.getCorrectionValue()
                    else:
                        raise ValueError("'part' must be one of ['VALUE','TARGET','CORRECTION']")
            
                    # convert to Python
                    _singleParamValue = ParameterValueConverter.convertToParameterValue(_singleValue)
                    _singleParamValuePy = self._dataConverter._convertValueValueToPy(_singleParamValue)
            
                    # wrap up
                    if completeOutput:
                        partial_output.append({'value': _singleParamValuePy, 
                            'header': {'trimId': trimId, 
                                'creationStamp':creationStamp, 
                                'beamProcess':beamProcess,
                                'trimDescription':description}})
                    else:
                        partial_output.append(_singleParamValuePy)
            # just return the minimum need...
            if len(partial_output) == 0:
                print('Did not found any trim for the given parameter/time')
                partial_output = ''
            elif len(partial_output) == 1:
                partial_output = partial_output[0]
            else:
                raise ValueError('interesting... got many values... did not expect this...')
            output.append(partial_output)
        
        if not returnAllRecords:
            if len(output) == 0:
                print('Did not found any trim for the given parameter/time')
                output = ''
            elif len(output) == 1:
                output = output[0]
            else:
                raise ValueError('interesting... got many values... did not expect this...')

        return output

    def setSimpleTrim(self, parameterName, value, description=None, selectorOverride=None, cycleOverride=None, part='value', transient=False, drive=True):
        '''
        Sets a new value to a given parameter in LSA (not necessarily to the hardware!)

        Args:
             parameterName (str): string name of the parameter
             value: the new value you want to send
             description (str): string description of your trim - it will be available in the trim history (default = None)
             selectorOverride (str): selector where to look for a trim. (default = None)
             cycleOverride (str): cycle name to use (it has priority over selector) (default = None)
             part (str): which part to trim: it can be ['value', 'target', 'correction'] (default = 'value')
             transient (bool): If the trim has to be flagged as "Transient Trim": if True, trim will stay in the Trim History only for one week - useful for feedback applications (default = False). 
             drive (bool): If to drive the HARDWARE or not (default = True)

        Returns:
             (bool): return True if set was correctly driven to the hardware
        '''
        return self.setTrims([parameterName], values={parameterName: value}, description=description, selectorOverride=selectorOverride, cycleOverride=cycleOverride, part=part, transient=transient, drive=drive)

    def setTrims(self, parameterNames, values, description=None, selectorOverride=None, cycleOverride=None, part='value', transient=False, drive=True):
        '''
        Sets a new value to a given parameter in LSA (not necessarily to the hardware!)

        Args:
             parameterNames (list(str)): list of string parameter names you want to SET
             value: a dictionary of values you want to send for each parameter
             description (str): string description of your trim - it will be available in the trim history (default = None)
             selectorOverride (str): selector where to look for a trim. (default = None)
             cycleOverride (str): cycle name to use (it has priority over selector) (default = None)
             part (str): which part to trim: it can be ['value', 'target', 'correction'] (default = 'value')
             transient (bool): If the trim has to be flagged as "Transient Trim": if True, trim will stay in the Trim History only for one week - useful for feedback applications (default = False). 
             drive (bool): If to drive the HARDWARE or not (default = True)
        
        Returns:
             (bool): return True if set was correctly driven to the hardware
        '''
        self.getConfiguredLSAServer()

        # force parameterNames to be a list
        if isinstance(parameterNames, six.string_types):
            parameterNames = [parameterNames]

        # add some meaningful description
        if description is None:
            description = 'Set by PyJapcScout during MD...'

        # initialise a few objects
        cern = jp.JPackage("cern")
        serviceLocator = cern.lsa.client.ServiceLocator
        trimService = serviceLocator.getService(cern.lsa.client.TrimService)
        parameterService = serviceLocator.getService(cern.lsa.client.ParameterService)
        contextService = serviceLocator.getService(cern.lsa.client.ContextService)
        TrimRequest = cern.lsa.domain.settings.TrimRequest
        ValueFactory = cern.accsoft.commons.value.ValueFactory
        ParameterValueConverter = cern.lsa.domain.exploitation.ParameterValueConverter
        
        # get cycleName and then "Java" cycle where to get trim history from
        if cycleOverride == None:
            if selectorOverride == None:
                selector = self._defaultSelector
            else:
                selector = selectorOverride
            if selector == '':
               cycleName = ''
            else:
               cycleName = self.getCycleNameFromUser(selector)
        else:
            cycleName = cycleOverride

        if cycleName == '':
            # take first parameter as reference to find cycle Name - TODO: not nice
            parameter = parameterService.findParameterByName(parameterNames[0])
            acceleratorJObj = parameter.getAccelerator()
            cycle = contextService.findResidentNonMultiplexedContext(acceleratorJObj)
            cycleName = cycle.getName()
        else:
            cycle = contextService.findStandAloneCycle(cycleName)

        # set a specific part
        if part.upper() == 'VALUE':
            part = cern.lsa.domain.settings.SettingPartEnum.VALUE
        elif part.upper() == 'TARGET':
            part = cern.lsa.domain.settings.SettingPartEnum.TARGET
        elif part.upper() == 'CORRECTION':
            part = cern.lsa.domain.settings.SettingPartEnum.CORRECTION
        else:
            raise ValueError("'part' must be one of ['VALUE','TARGET','CORRECTION']")

        # preapare trim request
        trimRequest = TrimRequest.builder().setContext(cycle).setDescription(description).setSettingPart(part).setTransient(transient).setDrive(drive)

        for parameterName in parameterNames:
            # get LSA parameter
            parameter = parameterService.findParameterByName(parameterName)

            # incapsulate python value into JAPC value
            auxDescriptor = self.getJavaValueDescriptor(parameterName, forceCCSDescriptorSource=False, verbose=False)
            # convert to Java (JAPC):
            if isinstance(values[parameterName], dict) and 'value' in values[parameterName]:
                value = values[parameterName]['value']
            else:
                value = values[parameterName]
            auxData = self._dataConverter.PyToJValue(value, auxDescriptor)
            # convert to LSA value-liked value
            auxData = ParameterValueConverter.convertToImmutableValue(parameter, auxData)

            # see what to put in the trim request
            if parameter.getValueType().isScalar():
                trimRequest = trimRequest.addScalar(parameter, cycle, auxData)
            elif parameter.getValueType().isFunction():
                trimRequest = trimRequest.addFunction(parameter, auxData)
            else:
                raise ValueError(f'Unknwon parameter type {parameter.getValueType().toString()}')

        # drive the trim
        response = trimService.trimSettings(trimRequest.build())
        # return True if successful
        return response.isDrivePerformed()

    def setValues(self, parameterNames, values, selectorOverride = None, dataFilter = None, forceCCSDescriptorSource = False):
        """
        Asynchronous SET of a given python value to a list of JAPC parameters.

        Args:
             parameterNames (list): a list of parameters to be set
             values (dict): must be a dict of dicts that contains "value" key for all <parameterName> key elements. The final
                "value" key can contain as well a dict in case of full property being set.
             selectorOverride (str): The USER selector you want to use (default=None, i.e. use defaultSelector specified in PyJapcScout instance)
             dataFilter (dict): a dictionary with suitable data filter for your SET (expert use. default=None)
             forceCCSDescriptorSource (bool): force to use CCS descriptor, no matter how JAPC was configured (or InCAified)
                to obtain the parameter value descriptor.
        """

        # Decide which selector to use:
        if selectorOverride == None:
            selector = self._defaultSelector
        else:
            selector = selectorOverride

        for parameterName in parameterNames:
            # actual data to be set:
            auxData = values[parameterName]['value']
            # get parameter descriptor
            auxDescriptor = self.getJavaValueDescriptor(parameterName, forceCCSDescriptorSource, verbose=False)
            # convert to Java: 
            auxData = self._dataConverter.PyToJValue(auxData, auxDescriptor)
            # auxData should now be already a "cern.japc.value.ParameterValue", so PyJapc should not try to crate a new one...
            self._mainPyJapc.setParam(parameterName, auxData, timingSelectorOverride = selector, dataFilterOverride = dataFilter, checkDims=False, dtype=None)

    def getJavaValueDescriptor(self, parameterName, forceCCSDescriptorSource = False, verbose=True):
        """
        Returns the Java **Parameter Value Descriptor** of a given paramter.

        It can be convenient for the user to debug or to know some properties
        of a given parameter.

        Args:
             parameterName (str): string identifying your parameter
             forceCCSDescriptorSource (bool): force to look into CCS descriptor instead of whatever is configured (default=False)
             verbose (bool): if to printout the eventually found descriptor (default=True)
        
        Returns:     
             (Java): JAPC Value Descriptor
        """
        if forceCCSDescriptorSource:
            cern = jp.JPackage("cern")
            ccs_descriptor_provider = cern.japc.svc.ccs.CcsDescriptorProvider()
            descriptor = ccs_descriptor_provider.findValueDescriptor(parameterName)
            #return ccs_descriptor.findParameterDescriptor(parameterName)
        else:
            descriptor = self._mainPyJapc._getJapcPar(parameterName).getValueDescriptor()

        if verbose and descriptor is not None:
            print(descriptor.toString())
        return descriptor

    def getJavaParameterDescriptor(self, parameterName, forceCCSDescriptorSource = False, verbose=True):
        """
        Returns the Java **Parameter Descriptor** of a given parameter.

        It can be convenient for the user to debug or to know some properties
        of a given parameter.

        Args:
             parameterName (str): string identifying your parameter
             forceCCSDescriptorSource (bool): force to look into CCS descriptor instead of whatever is configured (default=False)
             verbose (str): if to printout the eventually found descriptor (default=True)
       
        Returns:    
             (Java): JAPC Parameter Descriptor
        """
        if forceCCSDescriptorSource:
            cern = jp.JPackage("cern")
            ccs_descriptor_provider = cern.japc.svc.ccs.CcsDescriptorProvider()
            descriptor = ccs_descriptor_provider.findParameterDescriptor(parameterName)
        else:
            descriptor = self._mainPyJapc.getParamInfo(parameterName, noPyConversion=True)
        if verbose and descriptor is not None:
            print(descriptor.toString())
        return descriptor

    def createMonitor(self, parameterNames, onValueReceived=None, selectorOverride = None,
                        groupStrategy = 'standard',
                        allowManyUpdatesPerCycle=False, strategyTimeout=-1, forceGetOnChangeAndConstantValues=False, # only for groupStrategy=extended
                        dataSanityCheck = None,
                        **kwargs):
        """It creates a `PyJapcScoutMonitor` object which is handling a subscription to a group of parameters.

        Args:
            parameterNames (list): list of parameters to monitor (default=None)
            onValueReceived (function(dict, handler)): user-defined callback function
            selectorOverride (str): selector to be used for the subscription (default=None)
            groupStrategy (str): 
                'standard' -> (default) simple PyJapc group subscription 
                'extended' -> using PyJapcExt which uses CTF-based 
                'multiple' -> multiple independent subscriptions that call back the same function, no attempt to sync data
                the 'extended' strategy have several options:
                    allowManyUpdatesPerCycle (default=False)
                    strategyTimeout (default=-1, i.e. disabled)
                    forceGetOnChangeAndConstantValues (default=False) 
            dataSanityCheck (function(dict, handler)): user-defined function used to validate the received data set
                (default=None, i.e. a function returning always True)
        
        Returns:
            the handler to the monitoring object which allows you to start/stop the subscription.
        """
        # Decide which selector to use:
        if selectorOverride == None:
            selector = self._defaultSelector
        else:
            selector = selectorOverride
        
        # Create and return a monitoring object
        return self.PyJapcScoutMonitor(selector, parameterNames, onValueReceived=onValueReceived, unixtime=self._unixtime,
                        groupStrategy = groupStrategy,
                        allowManyUpdatesPerCycle = allowManyUpdatesPerCycle, # only for groupStrategy=extended
                        strategyTimeout = strategyTimeout, # only for groupStrategy=extended
                        forceGetOnChangeAndConstantValues = forceGetOnChangeAndConstantValues, # only for groupStrategy=extended
                        dataSanityCheck = dataSanityCheck,
                        **kwargs)

    class PyJapcScoutMonitor(object):
        def __init__(self, selector, parameterNames, onValueReceived=None, unixtime=True, timeZone="utc",
                        groupStrategy = 'standard',
                        allowManyUpdatesPerCycle=False, strategyTimeout=-1, forceGetOnChangeAndConstantValues=False, # only for groupStrategy=extended
                        dataSanityCheck = None,
                        **kwargs):
            """
            Initialises a PyJapcScoutMonitor monitoring object.

            This object is used to control the GroupSubscription to a set of parameters.

            Args:
                 selector:
                 parameterNames:
                 onValueReceived:
                 unixtime:
                 timeZone:
                 groupStrategy:
                 allowManyUpdatesPerCycle:
                 strategyTimeout (int): (default=-1, i.e. feature disabled)
                 forceGetOnChangeAndConstantValues: (default=False)
                 dataSanityCheck:
            Returns:
                 (PyJapcScoutMonitor): object to control subscription
            """
            # just initialize some vars
            self.lastData = dict()

            self.saveData = False
            self.saveDataPath = "."
            self.saveDataFormat = "pickle"

            # create my own logger
            logging.basicConfig()
            self.log = logging.getLogger(__package__)

            # for synchronization purposes... badly implemented
            self.synchronizedCalls = True
            self._isTreatingData = False

            # mandatory properties fields
            self._userCallback = onValueReceived
            self._myPyJapc = None

            # a sanity check function. By default always returning true
            if dataSanityCheck == None:
                self.dataSanityCheck = lambda *args : True
            else:
                self.dataSanityCheck = dataSanityCheck

            # let's save those options here - just in case
            self._unixtime = unixtime
            self._timeZone = timeZone
            
            # create here object that handle Java -> Python conversion for this object
            self._dataConverter = PyJapcScoutConverter()

            # force parameterNames to be a list
            if isinstance(parameterNames, six.string_types):
                parameterNames = [parameterNames]
            
            # depending on the strategy chosen, a different pyjapc object will be created and used
            # NOTE: I am afraid several options below are not needed as not taken into account by PyJapc if noPyConversion=True..., getHeader=True, unixtime=unixtime, timeZone = timeZone ...
            if groupStrategy == 'standard':
                # make a standard group subscription with pyJapc
                self._myPyJapc = PyJapc(selector, timeZone=timeZone, incaAcceleratorName=PyJapcScout.getConfiguredLSAServer(raiseError=False))
                self._myPyJapc.subscribeParam(parameterNames, onValueReceived=self._masterCallback, 
                        getHeader=True, noPyConversion=True, unixtime=unixtime, **kwargs)
            elif groupStrategy == 'extended':
                # Create a PyJapcExt object (using CTF-like subscription strategy)
                self._myPyJapc = PyJapcExt(selector, timeZone=timeZone, incaAcceleratorName=PyJapcScout.getConfiguredLSAServer(raiseError=False))
                self._myPyJapc.subscribeParamExt(parameterNames, onValueReceived=self._masterCallback, 
                        getHeader=True, noPyConversion=True, unixtime=unixtime, 
                        allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout, forceGetOnChangeAndConstantValues=forceGetOnChangeAndConstantValues,
                        **kwargs)
            elif groupStrategy == 'multiple':
                # make several subscriptions within the same pyjapc object.
                # Still, "force" to use group subscription for each parameter to simplify the code of PyJapcScoutMonitor class
                self._myPyJapc = PyJapc(selector, timeZone=timeZone, incaAcceleratorName=PyJapcScout.getConfiguredLSAServer(raiseError=False))
                for parameterName in parameterNames:
                    self._myPyJapc.subscribeParam([parameterName], onValueReceived=self._masterCallback, 
                        getHeader=True, noPyConversion=True, unixtime=unixtime, **kwargs)
            else:
                raise ValueError('Unknown group subscription strategy')
            
        def startMonitor(self):
            """
            Starts the subscription

            """
            self._myPyJapc.startSubscriptions()
        def stopMonitor(self):
            """
            Stops the subscription

            """
            self._myPyJapc.stopSubscriptions()
        def _masterCallback(self, values):
            """
            Internal function that is used as initial callback function for the monitored parameters
            It is called by JAPC.

            Args:
                 values: JAPC Java values
            Returns:    
            """

            # just some lock mechanism to ensure data synchronisation... surely not the best one!
            if self.synchronizedCalls & self._isTreatingData :
                self.log.warning('The monitor is already working on a previous acquisition... You will be loosing this dataset...')
                return
            else:
                self._isTreatingData = True

            # create dict to hold all acquired data
            outputData = dict()
            # by construction, values is always a Java array of FailSafeParameterValues.
            #   treat each of them and store them
            for failsSafeValue in values:
                try:
                    outputData[failsSafeValue.getParameterName()] = self._dataConverter.JAcquiredValueToPy(failsSafeValue, unixtime=self._unixtime)
                except Exception as e:
                    self.log.error('PyJapcScout:: not managing to extract data for [{0}]\n{1}'.format(
                        failsSafeValue.getParameterName(), traceback.format_exc()))
                    sys.stdout.flush()
                    outputData[failsSafeValue.getParameterName()] = ''

            # Sanity check of received data
            try:
                if (self.dataSanityCheck != None) and (not self.dataSanityCheck(outputData, self)):
                    self.log.warning('Last dataset is not good... waiting for the next data.')
                    self._isTreatingData = False
                    return
            except Exception as e:
                self.log.error('PyJapcScout::Cannot check if received data is good... please debug it... I will continue assuming data is good...\n{0}'.format(
                    traceback.format_exc()))
                sys.stdout.flush()
                
            
            # store it as last dataset - TODO: evaluate if to make a deepcopy here!
            self.lastData = outputData.copy()

            # save the data if requested
            if self.saveData:
                try:
                    filename = datetime.now().strftime("%Y.%m.%d.%H.%M.%S.%f")
                    datascout.save_dict(outputData, folderPath=self.saveDataPath, filename=filename, fileFormat=self.saveDataFormat)
                except Exception as e:
                    self.log.error('PyJapcScout::Some error saving the data... please debug it... I will continue\n{0}'.format(
                        traceback.format_exc()))
                    sys.stdout.flush()

            # Now I can call the user with all the data structure
            if self._userCallback is not None:
                try:
                    # TODO: make it better such that user callback does not necessarily need two inputs...
                    self._userCallback(outputData, self)
                except Exception as e:
                    self.log.error('PyJapcScout::Some error calling user function... please debug it... I will continue\n{0}'.format(
                        traceback.format_exc()))
                    sys.stdout.flush()

            # If I am here, all is good - I can close
            self._isTreatingData = False

    def getNXCALS(self, parameterNames, selectorOverride=None, referenceTime_ns=None, maxAge_s=600,
                         SYSTEM_NAME='CMW', rawJavaOutput=False, rawPyOutput=False, returnAllRecords=False):
        '''
        Returns the last acquisition recorded in NXCALS

        By default it gives the last value recorded over the last (`maxAge_s` = 600) seconds for each parameter requested.
        One can modify the reference time (in the format of ns timestamp or even as string (in principle...)

        **NOTE: This is an EXPERIMENTAL feature!**

        Based on: https://gitlab.cern.ch/acc-logging-team/nxcals-examples/-/tree/master/extraction-api-thin-examples
        See also: https://nxcals-docs.web.cern.ch/current/user-guide/examples/df-with-sqlapi-example/

        Args:
            parameterNames list(str): list of parameters for which you are trying to extract data
            selectorOverride (str): default=None,
            referenceTime_ns (int): Time at which you want to obtain the data form NXCALS (default=None, use now)
            maxAge_s (int): Max age in s with respect to `referenceTime_ns` where to look for = 600,
            SYSTEM_NAME (str): NXCALS system where to look for data, (default = 'CMW')
            rawJavaOutput (bool): if to return the pure Avro Record instead of a Python-converted object (default=False)
            rawPyOutput (bool): if to return Avro Records simply converted to Python, without attempting to build a PyJapcScout-like dict
            returnAllRecords (bool): instead of the last record, return all found data in the given interval (default=False)

        Returns:
            (dict): dictionary with all data hopefully converted as Python objects...
        '''

        # See nxcals docs to eventually update servers...
        SPARK_SERVERS_URL = "cs-ccr-nxcals5.cern.ch:15000,cs-ccr-nxcals6.cern.ch:15000,cs-ccr-nxcals7.cern.ch:15000,cs-ccr-nxcals8.cern.ch:15000"

        # prepare some objects used later
        cern = jp.JPackage("cern")
        java = jp.JPackage("java")

        # force parameterNames to be a list
        if isinstance(parameterNames, six.string_types):
            parameterNames = [parameterNames]

        # define time for query
        if referenceTime_ns is None:
            referenceTime_ns = datascout.datetime_to_unixtime(datetime.now().astimezone())
        elif isinstance(referenceTime_ns, str):
            referenceTime_ns = datascout.string_to_unixtime(referenceTime_ns)
        start_time_ns = referenceTime_ns - int(maxAge_s*1e9)

        # define selector
        if selectorOverride is None:
            selector = self._defaultSelector
        else:
            selector = selectorOverride

        # I might need the Value Descriptors to properly extract data as a pyjapcscout data
        if not rawJavaOutput and not rawPyOutput:
            allDescriptors = dict()
            for parameterName in parameterNames:
                allDescriptors[parameterName] = self.getJavaValueDescriptor(parameterName, verbose=False)

        # cycle on parameters, and extract unique parameter names and fields
        requested_bare_parameters = dict()
        for parameterName in parameterNames:
            # split parameter from field
            parameterName_split = parameterName.split('#')
            if len(parameterName_split) == 2:
                _parameter, _field = parameterName_split
            elif len(parameterName_split) == 1:
                _parameter, _field = (parameterName, None)
            else:
                raise ValueError(f'Cannot understand your parameter name {parameterName}....')   
            if _parameter not in requested_bare_parameters:
                requested_bare_parameters[_parameter] = dict()
            if _field is not None:
                requested_bare_parameters[_parameter][_field] = None
        # put bare parameters in a Java list
        _Jparameters = java.util.LinkedList()
        for _key in requested_bare_parameters.keys():
            _Jparameters.add(_key)

        # create thin API query
        script = cern.nxcals.api.extraction.thin.data.builders.DevicePropertyDataQuery.builder().system(
            SYSTEM_NAME).startTime(start_time_ns).endTime(referenceTime_ns).entity().parameters(_Jparameters).build()
        
        # make some filtering already on the server side ##########
        # filter on selector, if needed
        if isinstance(selector, str) and 'ALL' not in selector:
            script += f'.where(\"selector == \'{selector}\'\")'
        # In principle, I could just ask to get the given field only - this would probably make it faster!:
        #  but I want to preserve all other "header" fields which I don't know...
        #if field is not None:
            #script += f'.select(\"acqStamp\",\"selector\",\"{field}\")'

        # order data by record time stamp (which could be the cyclestamp or the timestamp)
        script += f'.orderBy(\"__record_timestamp__\")'

        # define compression to use to get data
        compression = cern.nxcals.api.extraction.thin.AvroQuery.Codec.BZIP2
        # make the request and extract data
        avroQuery = cern.nxcals.api.extraction.thin.AvroQuery.newBuilder().setScript(script).setCompression(
            compression).build()

        extractionService = cern.nxcals.api.extraction.thin.ServiceFactory.createExtractionService(SPARK_SERVERS_URL)
        try:
            avroData = extractionService.query(avroQuery)
        except Exception:
            # make sure to shutdown the channel
            extractionService.getChannel().shutdown()
            raise
        # shutdown channel
        extractionService.getChannel().shutdown()

        # treat AVRO data as Avro records
        records = cern.nxcals.api.extraction.data.Avro.records(avroData)
        # One could in principle convert them to cmw records, but this conversion often fails... forgetting about it
        #cmwRecords = cern.nxcals.api.extraction.data.Datax.records(avroData)

        # see what we got...
        if len(records) > 0:
            print(f'Found {len(records)} records...')
            if rawJavaOutput:
                return records
            else:
                # in one way or another, I need to convert all Java records to python
                allPyRecords = [self._dataConverter.JAvroRecordToPy(record) for record in records]

                # start filling output
                output = dict()
                for parameterName in parameterNames:
                    if rawPyOutput:
                        output[parameterName] = [record 
                            for record in allPyRecords
                            if record['device'] + '/' + record['property'] in parameterName ]
                    else:
                        output[parameterName] = [self._dataConverter.PyAvroRecordToScoutDict(record, allDescriptors[parameterName])
                            for record in allPyRecords
                            if record['device'] + '/' + record['property'] in parameterName ]
                    # see if I need to keep the last one only:
                    if not returnAllRecords:
                        if len(output[parameterName]) > 0 :
                            output[parameterName] = output[parameterName][-1]
                        else:
                            self.log.warning(f"I did not find any value for {parameterName} for the given time frame...")
                            output[parameterName] = {'value':'', 'header':'', 'exception':f"I did not find any value for {parameterName} for the given time frame..."}
                return output
        else:
            output = dict()
            for parameterName in parameterNames:
                self.log.warning(f'No data found for {parameterName} in NXCALS for given time windows...')
                output[parameterName] = {'value':'', 'header':'', 'exception':f"I did not find any value for {parameterName} for the given time frame..."}
            return output

    def getSimpleNXCALS(self, parameterName, selectorOverride=None, referenceTime_ns=None, maxAge_s=600,
                         SYSTEM_NAME='CMW', completeOutput=False, rawJavaOutput=False, returnAllRecords=False):
        '''
        Returns the last acquisition recorded in NXCALS for a single parameter.

        This function is a simple wrapper of `getNXCALS` function, and it shares the same arguments.
        The only difference, if completeOutput==False (default), only the desired field value is returned.
        '''
        completeData = self.getNXCALS([parameterName], selectorOverride=selectorOverride, referenceTime_ns=referenceTime_ns,
                                maxAge_s=maxAge_s, SYSTEM_NAME=SYSTEM_NAME, rawJavaOutput=rawJavaOutput, rawPyOutput=False,
                                returnAllRecords=returnAllRecords)
        if completeOutput:
            return completeData[parameterName]
        else:
            if returnAllRecords:
                return [single_acq['value'] for single_acq in completeData[parameterName]]
            else:
                return completeData[parameterName]['value']

class PyJapcExt(PyJapc):
    """
    Extension of PyJapc using CTF-based subscription strategy
    """
    def __init__(self, *args, **kwargs):
        super(PyJapcExt, self).__init__(*args, **kwargs)
     
    def subscribeParamExt(self, parameterName, onValueReceived=None, onException=None, getHeader=False,
                       noPyConversion=False, unixtime=True,
                       allowManyUpdatesPerCycle=False, strategyTimeout=-1, forceGetOnChangeAndConstantValues=False,
                       **kwargs):
        # --------------------------------------------------------------------
        # Create a unique string key for this subscription
        # --------------------------------------------------------------------
        parameterKey = self._getDictKeyFromParameterName(parameterName)
        try:
            sel = kwargs['timingSelectorOverride']
        except KeyError:
            sel = None
        parameterKey = self._transformSubscribeCacheKey(parameterKey=parameterKey,
                                                        selector=sel)
        
        # Get the (cached) JAPC parameter object
        ################################################################
        ###### Davide - start modification - Basically re-implemnting locally 
        ######          "self._getJapcPar" using my strategy from CTF
        #par = self._getJapcPar(parameterName)
        parameterKey = self._getDictKeyFromParameterName(parameterName)

        # Davide: one might want to subscribe to same parameter, same selector, but different strategy - need to comment out this
        #if parameterKey in self._paramDict:
        #    # Parameter object exists already in dict
        #    p = self._paramDict[parameterKey]
        #else:
        # Make group subscription in all cases - single parameter or many parameters - there is no difference!

        # Create a new ParameterGroup object, populate it
        #p = jp.JPackage("cern").japc.core.spi.group.ParameterGroupImpl()
        strategy = jp.JPackage("cern").ctf.customjarsmatlab.GroupSubscriptionStrategyFactoryCTF(True)
        strategy.setFastStrategyAllowManyUpdatesPerCycle(bool(allowManyUpdatesPerCycle))
        strategy.setFastStrategyForceGetOnChangeAndConstantValues(bool(forceGetOnChangeAndConstantValues)) 
        strategy.setFastStrategyTimeOut(int(strategyTimeout)) # this should be a long...

        # create standard group Jobject with injected strategy
        p = jp.JPackage("cern").japc.core.spi.group.ParameterGroupImpl(strategy)
        for parName in parameterName:
            p.add(self._getJapcPar(parName))
        # Store the newly created object in the dict
        #self._paramDict[parameterKey] = p
        
        # To continue as before...
        par = p
        ######## Davide - end modification, same function as PyJapc 
        ################################################################
        
        lisJ = self._createValueListener(par=par,
                                        getHeader=getHeader,
                                        noPyConversion=noPyConversion,
                                        unixtime=unixtime,
                                        onValueReceived=onValueReceived,
                                        onException=onException)

        # --------------------------------------------------------------------
        # !!! Subscribe !!!
        # --------------------------------------------------------------------
        s = self._giveMeSelector(**kwargs)
        sh = par.createSubscription(s, lisJ)

        # --------------------------------------------------------------------
        # Add SubscriptionHandle to cache for later access
        # --------------------------------------------------------------------
        # Davide - this key is too weak now... it should be made differently...
        self._subscriptionHandleDict.setdefault(parameterKey, []).append(sh)

        return sh
