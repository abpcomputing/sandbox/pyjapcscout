"""
Class used by PyJapcScout for data conversion from Java to Python and viceversa
"""

import jpype as jp
import numpy as np
import logging
import datascout

class PyJapcScoutConverter(object):
    """
    This class is used to speed up the extraction of data from Java
    objects.

    The main idea is that the check of the type is taking too much time,
    so I use instead the hashCode given by the Type class.

    Based on matlabJAPC extractor class

    + looking at https://gitlab.cern.ch/acc-logging-team/nxcals/-/blob/develop/common/src/main/java/cern/nxcals/api/converters/ImmutableDataToAvroConverter.java
    + looking at https://issues.cern.ch/browse/NXCALS-4620 for function list

    In principle, this class could be used independtly from PyJapcScout and used in conbination to plane PyJapc
    to convert JAPC values in a different way...
    """

    def __init__(self):
        """
        Creates a PyJapcScoutConverter
        This class is used to handle the conversion between JAPC-like values to Python and vice-versa.

        It expects that the java virtual machine is already initialised...

        """

        cern = jp.JPackage("cern")

        #from cern.japc.value.Type
        self.SIMPLE            = cern.japc.value.Type.SIMPLE.hashCode()
        self.MAP               = cern.japc.value.Type.MAP.hashCode()
        
        #from cern.japc.value.ValueType
        self.UNDEFINED         = cern.japc.value.ValueType.UNDEFINED.hashCode()
        self.BOOLEAN           = cern.japc.value.ValueType.BOOLEAN.hashCode()
        self.BYTE              = cern.japc.value.ValueType.BYTE.hashCode()
        self.DOUBLE            = cern.japc.value.ValueType.DOUBLE.hashCode()
        self.FLOAT             = cern.japc.value.ValueType.FLOAT.hashCode()
        self.INT               = cern.japc.value.ValueType.INT.hashCode()
        self.LONG              = cern.japc.value.ValueType.LONG.hashCode()
        self.SHORT             = cern.japc.value.ValueType.SHORT.hashCode()
        self.STRING            = cern.japc.value.ValueType.STRING.hashCode()
        self.BOOLEAN_ARRAY     = cern.japc.value.ValueType.BOOLEAN_ARRAY.hashCode()
        self.BYTE_ARRAY        = cern.japc.value.ValueType.BYTE_ARRAY.hashCode()
        self.DOUBLE_ARRAY      = cern.japc.value.ValueType.DOUBLE_ARRAY.hashCode()
        self.FLOAT_ARRAY       = cern.japc.value.ValueType.FLOAT_ARRAY.hashCode()
        self.INT_ARRAY         = cern.japc.value.ValueType.INT_ARRAY.hashCode()
        self.LONG_ARRAY        = cern.japc.value.ValueType.LONG_ARRAY.hashCode()
        self.SHORT_ARRAY       = cern.japc.value.ValueType.SHORT_ARRAY.hashCode()
        self.STRING_ARRAY      = cern.japc.value.ValueType.STRING_ARRAY.hashCode()
        self.BOOLEAN_ARRAY_2D  = cern.japc.value.ValueType.BOOLEAN_ARRAY_2D.hashCode()
        self.BYTE_ARRAY_2D     = cern.japc.value.ValueType.BYTE_ARRAY_2D.hashCode()
        self.DOUBLE_ARRAY_2D   = cern.japc.value.ValueType.DOUBLE_ARRAY_2D.hashCode()
        self.FLOAT_ARRAY_2D    = cern.japc.value.ValueType.FLOAT_ARRAY_2D.hashCode()
        self.INT_ARRAY_2D      = cern.japc.value.ValueType.INT_ARRAY_2D.hashCode()
        self.LONG_ARRAY_2D     = cern.japc.value.ValueType.LONG_ARRAY_2D.hashCode()
        self.SHORT_ARRAY_2D    = cern.japc.value.ValueType.SHORT_ARRAY_2D.hashCode()
        self.STRING_ARRAY_2D   = cern.japc.value.ValueType.STRING_ARRAY_2D.hashCode()
        self.ENUM              = cern.japc.value.ValueType.ENUM.hashCode()
        self.ENUM_ARRAY        = cern.japc.value.ValueType.ENUM_ARRAY.hashCode()
        self.ENUM_ARRAY_2D     = cern.japc.value.ValueType.ENUM_ARRAY_2D.hashCode()
        self.ENUM_SET          = cern.japc.value.ValueType.ENUM_SET.hashCode()
        self.ENUM_SET_ARRAY    = cern.japc.value.ValueType.ENUM_SET_ARRAY.hashCode()
        self.ENUM_SET_ARRAY_2D = cern.japc.value.ValueType.ENUM_SET_ARRAY_2D.hashCode()
        self.DISCRETE_FUNCTION = cern.japc.value.ValueType.DISCRETE_FUNCTION.hashCode()
        self.DISCRETE_FUNCTION_LIST= cern.japc.value.ValueType.DISCRETE_FUNCTION_LIST.hashCode()

        logging.basicConfig()
        self.log = logging.getLogger(__package__)

    def JAcquiredValueToPy(self, acquiredData, unixtime=True, timeZone="utc"):
        """
        Converts a JAPC-acquired JAVA value into a Python one.

        :param acquiredData: the data acquired from JAPC
        :param unixtime: If to use unixtime instead of datetime in the header (default=True)
        :param timeZone: The default time zone for datetime objects in the header (default="utc")
        :return:
        """
        header = self._convertValueHeaderToPy(acquiredData.getHeader(), unixtime=unixtime, timeZone=timeZone)
        try:
            exception = self._convertValueExceptionToPy(acquiredData.getException())
        except:
            # hopefully we are dealing with a cern.japc.core.spi.AcquiredParameterValueImpl
            exception = ''

        if exception == '':
            value = self._convertValueValueToPy(acquiredData.getValue())
        else:
            self.log.error('Error occurred acquiring ' + acquiredData.getParameterName()+': ' + exception)
            value = self._createEmptyPyValueFromDesc(acquiredData.getDescriptor())
        
        return  {'value': value, 'header': header, 'exception': exception}
    
    def _convertValueExceptionToPy(self, valueException):
        """ Converts a JAPC ValueException into Python

        :param valueException: The JAPC Exception
        :return: a string with the exception message obtained (if any)
        """
        if valueException == None:
            return ''
        else:
            return valueException.getMessage()

    def _convertValueHeaderToPy(self, valueHeader, unixtime=True, timeZone="utc"):
        """ Convert a `ValueHeader` object to a python dictionary

        NOTE: Copy-Paste from PyJapc in May 2021.
        NOTE2: Forcing to use unixtime=True (May 2021) as this is necessary for data saving (datetime not supported...)

        :param valueHeader: The JAPC Value Header object
        :param unixtime: (default=True)
        :param timeZone: The timezone used to convert the timestamps into datetime.
        :return: a dict with header informations
        """

        headerDict = dict()
        if not unixtime:
            self.log.warning('Only unixtime (in ns) allowed for the time being - PyJapcScout - May 2021')
            unixtime = True

        if unixtime:
            # May 2021: just keep timestamps as unixtime in ns as a numpy int64
            headerDict["acqStamp"]   = np.int64(valueHeader.getAcqStamp())   #/ 1e9
            headerDict["cycleStamp"] = np.int64(valueHeader.getCycleStamp()) #/ 1e9
            headerDict["setStamp"]   = np.int64(valueHeader.getSetStamp())   #/ 1e9
        else:
            
            headerDict["acqStamp"] = datascout.unixtime_to_datetime(valueHeader.getAcqStamp(),
                                                                    time_zone=timeZone)
            headerDict["cycleStamp"] = datascout.unixtime_to_datetime(valueHeader.getCycleStamp(),
                                                                    tz=timeZone)
            headerDict["setStamp"] = datascout.unixtime_to_datetime(valueHeader.getSetStamp(),
                                                                    tz=timeZone)
        # to np.bool_ , Davide, May 2021
        headerDict["isFirstUpdate"] = np.bool_(bool(valueHeader.isFirstUpdate()))
        headerDict["isImmediateUpdate"] = np.bool_(bool(valueHeader.isImmediateUpdate()))
        if valueHeader.getSelector() is not None:
            headerDict["selector"] = valueHeader.getSelector().toString()
        else:
            headerDict["selector"] = '' # 'None' not allowed! Davide, May 2021
        return headerDict

    def _convertValueValueToPy(self, JValue):
        """ it extract all the fields from a MAP or the value from a SIMPLE JAPC parameter value

        Note: the chosen type conversion is "easily" compatible with saving to parquet,
        use in pandas, and awkward arrays.

        TODO: np.array(xxx, dtype=object) have some troubles with awkward arrays in case
            one device returns xxx = empty. One could think in those cases to return an empty string


        :param JValue: The acquired JAPC Value
        :return: the JAPC value converted into Pythonic object
        """


        #see if JValue is a map or a simple object  
        JValueTypeHash = JValue.getType().hashCode()
        if (JValueTypeHash == self.SIMPLE):
            PyValue = self._convertValueSimpleValueToPy(JValue)
        elif (JValueTypeHash == self.MAP):
            # since this is a big structure, I will give back a dict
            # with all the fields inside.
            PyValue = dict()
            
            # here the names of the fields
            allFieldNames = JValue.getNames()
            
            # now extract them all
            for fieldName in allFieldNames:
                self.log.debug('^--------------V\n  Extracting {0}'.format(fieldName))
                PyValue[fieldName] = self._convertValueSimpleValueToPy(JValue.get(fieldName))
        else:
            raise TypeError('PyJapcScoutExtractor::unknown Java Value type. Probably an OBJECT -> not supported')
        return PyValue
        
    def _convertValueSimpleValueToPy(self, JValue):
        """
        extracts a python understandable value from a Java Simple Parameter Value.

        All values will be numpy values (or arrays), or list/dict of numpy values (or arrays)
        Present implementation is very verbose/pedantic for academic reasons.

        :param JValue: The JAPC Simple Parameter Value to be converted
        :return: the JAPC value converted into Pythonic object
        """


        # get JAPC value type
        JType = JValue.getValueType().hashCode()
        
        # convert all value types
        ########## NUMERIC/SIMPLE -> TO NUMPY ###########
        if (JType == self.DOUBLE_ARRAY) or (JType == self.DOUBLE_ARRAY_2D):
            PyValue = np.array(JValue.getDoubles(), dtype=np.float64)
        elif (JType == self.DOUBLE ):
            PyValue = np.float64(JValue.getDouble())
            
        elif (JType == self.FLOAT_ARRAY) or (JType == self.FLOAT_ARRAY_2D):
            PyValue = np.array(JValue.getFloats(), dtype=np.float32)
        elif (JType == self.FLOAT ):
            PyValue = np.float32(JValue.getFloat())
            
        elif (JType == self.LONG_ARRAY) or (JType == self.LONG_ARRAY_2D):
            PyValue = np.array(JValue.getLongs(), dtype=np.int64)
        elif (JType == self.LONG ):
            PyValue = np.int64(JValue.getLong())
               
        elif (JType == self.INT_ARRAY) or (JType == self.INT_ARRAY_2D):
            PyValue = np.array(JValue.getInts(), dtype=np.int32)
        elif (JType == self.INT ):
            PyValue = np.int32(JValue.getInt())

        elif (JType == self.SHORT_ARRAY) or (JType == self.SHORT_ARRAY_2D):
            PyValue = np.array(JValue.getShorts(), dtype=np.int16)
        elif (JType == self.SHORT ):
            PyValue = np.int16(JValue.getShort())
            
        elif (JType == self.BYTE_ARRAY) or (JType == self.BYTE_ARRAY_2D):
            PyValue = np.array(JValue.getBytes(), dtype=np.int8)
        elif (JType == self.BYTE ):
            PyValue = np.int8(JValue.getByte())
        
        elif (JType == self.BOOLEAN_ARRAY) or (JType == self.BOOLEAN_ARRAY_2D):
            PyValue = np.array(JValue.getBooleans(), dtype=np.bool_)
        elif (JType == self.BOOLEAN):
            PyValue = np.bool_(JValue.getBoolean())

        ########## STRING -> TO (LIST) STRING(S) ###########
        elif (JType == self.STRING_ARRAY) or (JType == self.STRING_ARRAY_2D):
            PyValue = np.array( list(JValue.getStrings()), dtype = np.str_ )
        elif (JType == self.STRING):
            PyValue = str(JValue.getString())
        

        ########## ENUM -> TO (LIST or LIST of LISTs) dict{code, string} ###########
        elif (JType == self.ENUM_ARRAY) or (JType == self.ENUM_ARRAY_2D):
            PyValue = []
            for auxEnum in JValue.getEnumItems():
                PyValue.append( {'JAPC_ENUM': { 'code': np.int64(auxEnum.getCode()), 'string': str(auxEnum.getString()) } } ) # Add decorator - May 2021
            PyValue = np.array(PyValue, dtype=object)
        elif (JType == self.ENUM):
            auxEnum = JValue.getEnumItem()
            PyValue = {'JAPC_ENUM': { 'code': np.int64(auxEnum.getCode()), 'string': str(auxEnum.getString()) } } # Add decorator - May 2021
        elif (JType == self.ENUM_SET_ARRAY) or (JType == self.ENUM_SET_ARRAY_2D):
            PyValue = []
            for auxEnumSet in JValue.getEnumItemSets():
                auxValue = dict()
                auxValue['asLong']   = np.int64(auxEnumSet.asLong())
                auxValue['codes']    = np.array([np.int64(v.getCode()) for v in auxEnumSet], dtype=np.int64)
                auxValue['strings']  = np.array([str(v.getString()) for v in auxEnumSet], dtype=np.str_)
                PyValue.append( {'JAPC_ENUM_SET': auxValue } ) # Add decorator - May 2021
            PyValue = np.array(PyValue, dtype=object)
        elif (JType == self.ENUM_SET):
            auxEnumSet = JValue.getEnumItemSet()
            PyValue = dict()
            PyValue['asLong']   = np.int64(auxEnumSet.asLong())
            PyValue['codes']    = np.array([np.int64(v.getCode()) for v in auxEnumSet], dtype=np.int64)
            PyValue['strings']  = np.array([str(v.getString()) for v in auxEnumSet], dtype=np.str_)
            PyValue = {'JAPC_ENUM_SET': PyValue} # Add decorator - May 2021

        ########## DISCRETE_FUNCTION -> (LIST) dict{'X': np.float64, 'Y': np.float64}###########
        elif (JType == self.DISCRETE_FUNCTION):
            PyValue = dict()
            auxFunction = JValue.getDiscreteFunction()
            PyValue['X'] = np.array(auxFunction.getXArray(), dtype=np.float64)
            PyValue['Y'] = np.array(auxFunction.getYArray(), dtype=np.float64)
            PyValue = {'JAPC_FUNCTION': PyValue} # Add decorator - May 2021
        elif (JType == self.DISCRETE_FUNCTION_LIST):
            PyValue = []
            for auxFunction in JValue.getDiscreteFunctionList().getFunctions():
                auxValue = dict()
                auxValue['X'] = np.array(auxFunction.getXArray(), dtype=np.float64)
                auxValue['Y'] = np.array(auxFunction.getYArray(), dtype=np.float64)
                PyValue.append({'JAPC_FUNCTION': auxValue}) # Add decorator - May 2021
            PyValue = np.array(PyValue, dtype=object) # make it an array of object types
        else:
            raise TypeError('_convertValueSimpleValueToPy(): Not supported value type (' + str(JType.toString()) + ')! Unable to get current value for some field...')

        # if we are dealing with an array2D, then we can extract
        # correctly the associated matrix with the right dimensions.
        # Remember Java is somehow row-major, Matlab is column-major, Python is row-major!
        if (JValue.getValueType().isArray2d()):
            """
                From JAPC - https://acc-sources.cern.ch/gitlab.cern.ch/acc-co/japc/japc-core/-/blob/japc-value/src/java/cern/japc/value/ValueType.java#L28:35
                public static boolean isArray2dType(ValueType valueType) {
                    return (valueType.equals(BOOLEAN_ARRAY_2D) || valueType.equals(BYTE_ARRAY_2D)
                        || valueType.equals(DOUBLE_ARRAY_2D) || valueType.equals(FLOAT_ARRAY_2D)
                        || valueType.equals(INT_ARRAY_2D) || valueType.equals(LONG_ARRAY_2D)
                        || valueType.equals(SHORT_ARRAY_2D) || valueType.equals(STRING_ARRAY_2D)     -> warning STRING_ARRAY_2D!
                        || valueType.equals(ENUM_ARRAY_2D) || valueType.equals(ENUM_SET_ARRAY_2D));  -> also isEnumeric()
            """
            #if JType == self.STRING_ARRAY_2D or JValue.getValueType().isEnumeric():
            #    auxNColumns = JValue.getColumnCount()
            #    PyValue = [PyValue[i:i + auxNColumns] for i in range(0, len(PyValue), auxNColumns)]
            #else:
            #    # it should be a numpy object....
            #    auxNRows = JValue.getRowCount()
            #    auxNColumns = JValue.getColumnCount()
            #    PyValue = np.reshape(PyValue, (auxNRows, auxNColumns))
            # May 2021: it should always be like this now:
            auxNRows = JValue.getRowCount()
            auxNColumns = JValue.getColumnCount()
            PyValue = np.reshape(PyValue, (auxNRows, auxNColumns))

        return PyValue
    
    def _createEmptyPyValueFromDesc(self, JValueDescriptor):
        """
        Creates a "no_value" data starting from JValueDescriptor, if possible, or just returns an empty string.

        This function is used to defined the desired behavior in case a parameter is returned with an exception.
        In this case, clearly, no "real" data is available, so one has to decide what to return to the user.
        The adopted solution here is to at least try to reconstruct the "map" of all fields in case of MAP parameter value
        and to fill it with empty strings, or simply return an empty string in case of SIMPLE parameters or in case no
        Value Descriptor is available.

        :param JValueDescriptor: The JAPC Value Descriptor
        :return: a "standard" no value. It can be an empty string or a dict with keys corresponding to the fields of the
            (not) acquired parameter.
        """


        # to be seen what is best, for now we agreed to use an empty string -> '', 
        no_data_value = '' # it could be a np.NaN (as done before), but not a None !

        if JValueDescriptor == None:
            # we start badly... assume a simple value
            PyValue = no_data_value
        elif JValueDescriptor.getType().hashCode() == self.SIMPLE:
            PyValue = no_data_value
        elif JValueDescriptor.getType().hashCode() == self.MAP:
            PyValue = dict()
            for name in JValueDescriptor.getNames():
                PyValue[name] = no_data_value
        else:
            raise TypeError('Unknown ValueDescriptor type!')
        return PyValue
    
    def PyToJValue(self, PyValue, JValueDescriptor):
        """
        sets a python value (typically a numpy value or a dict of numpy values)
        into a Java Parameter Value

        if the JNewValue will have to be a Map Parameter Value, then PyValue has to be a
        dictionary with the necessary keys to fill the MAP parameter.

        :param PyValue: a python object to be converted into a JAPC-compatible one
        :param JValueDescriptor: The JAPC Value Descriptor for which the JAPC Value has to be created
        :return: The JAPC Value that can be SET to the control system
        """
        
        # import a SimpleParameterValueFactory
        cern = jp.JPackage("cern")
        SimpleParameterValueFactory = cern.japc.core.factory.SimpleParameterValueFactory
        MapParameterValueFactory    = cern.japc.core.factory.MapParameterValueFactory
        
        if JValueDescriptor == None:
            # try to just guess the type as SIMPLE and let
            JNewValue = self._convertSimplePyValueToSimpleJValue(PyValue, JValueDescriptor)
        else:
            # see if JNewValue is a map or a simple object
            JValueTypeHash = JValueDescriptor.getType().hashCode()
            if (JValueTypeHash == self.SIMPLE):
                JNewValue = self._convertSimplePyValueToSimpleJValue(PyValue, JValueDescriptor)
            elif (JValueTypeHash == self.MAP):
                # create a new Map Value based on descriptor
                JNewValue = MapParameterValueFactory.newValue(JValueDescriptor)

                # since this is a big structure, I need a struct as PyValue
                if isinstance(PyValue, dict):
                    raise TypeError('PyToJValue:: you are trying to set a MAP parameter with a simple value. Impossible!')
                    # now set all of fields
                    for auxFieldName, auxFieldValue in PyValue.items():
                        # get single field descriptor
                        auxJSingleValueDescriptor = JValueDescriptor.get(auxFieldName)
                        if auxJSingleValueDescriptor is None:
                            raise ValueError('PyToJValue:: no "' + auxFieldName + '" field in ' + str(JValueDescriptor.getName()))

                        # create simpleParameterValue with provided data
                        auxJSingleValue = self._convertSimplePyValueToSimpleJValue(auxFieldValue, auxJSingleValueDescriptor)
                        # insert this simpleParameterValue in the mapParameterValue
                        JNewValue.put(auxFieldName, auxJSingleValue)
                elif (PyValue == None) or (PyValue == '') and JValueDescriptor.getNames().length == 0:
                    self.log.info('Assuming this is a command... Try to continue...')
                    print('Not TESTED feature!')
                else:
                    raise TypeError('PyToJValue:: you are trying to set a MAP parameter with a simple value. Impossible!')
            else:
                raise TypeError("Unknown Parameter type: {0}. No idea how to set that.".format(JValueDescriptor.getType().toString()))
        return JNewValue
    
    def _getJavaValue(self, pyValueOrType):
        """
        Converts basic (numpy) python values to JPype type.
        if pyValueOrType is of class type, it returns the final JPype type

        This code is very similar to _getJavaValue(self, numpyType, pyValue=None) from PyJapc

        :param pyValueOrType:
        :return:
        """


        # Lookup table. Input = Python type, output = [Python scalar type, JPype Java Type]
        typeLookup = {
            int:    [jp.JInt],
            float:  [jp.JDouble],
            bool:   [jp.JBoolean],
            str:    [jp.JString],

            np.int_:    [jp.JInt],
            np.float_:  [jp.JDouble],
            np.bool_:   [jp.JBoolean],
            np.str_:    [jp.JString],

            np.int8:    [int, jp.JByte],
            np.int16:   [int, jp.JShort],
            np.int32:   [int, jp.JInt],
            np.int64:   [int, jp.JLong],

            # Java does not have unsigned types, take the next bigger ones instead.
            np.uint8:   [int, jp.JShort],
            np.uint16:  [int, jp.JInt],
            np.uint32:  [int, jp.JLong],
            np.uint64:  [int, jp.JLong],

            np.float32: [float, jp.JFloat],
            np.float64: [float, jp.JDouble],

            # This will deserve some more attention
            np.ndarray: [jp.JArray]
        }

        pyType = type(pyValueOrType) 

        # Convert input list / tuple to a numpy array
        # 
        # This comes from _convertPyToSimpleValFallback. 
        #  Assumption here is that inside the list/tuple there is a simple/numerical data
        if pyType in (list, tuple):
            pyValueOrType = np.array(pyValueOrType)
        
        pyType = type(pyValueOrType) 
        if pyType not in typeLookup:
            raise TypeError("Python type {0} can not be converted to a JPype type".format(pyType))
        
        if isinstance(pyValueOrType, type):
            return typeLookup[pyValueOrType][-1]
        elif isinstance(pyValueOrType, np.ndarray):
            # this is part of _convertPyToSimpleValFallback from PyJapc
            numpyDataType      = pyValueOrType.dtype.type
            numpyDataDimensions = pyValueOrType.ndim
            # In principle one could do:
            #   jValue = typeLookup[pyType](typeLookup[numpyDataType], numpyDataDimension)(pyVal.tolist())
            # but then I don't know how to inject it in JAPC if it turns out to be a 2D array... 
            # let's flatten it as done in PyJapc, and will think later to re-shape to the proper 2D shape...
            jValue = typeLookup[pyType][0](typeLookup[numpyDataType][-1], 1)(pyValueOrType.flatten().tolist())
            return jValue
        else:
            jValue = pyValueOrType
            for cast in typeLookup[pyType]:
                jValue = cast(jValue)
            return jValue

    def _getPyArrayToJavaDimensions(self, pyValue):
        """ Returns a _jarray.int[] with two elements if a 2D array or None if is a 1D object

        :param pyValue: a list, tuple, or numpy ndarray from which to extract the dimensions
        :return: a Java int array with two elements defining the dimensions of the 2D array or None if NOT a 2D array
        """
        pyType = type(pyValue) 
        if pyType in (list, tuple):
            pyValue = np.array(pyValue)
        
        if isinstance(pyValue, np.ndarray):
            numpyDataDimensions = pyValue.ndim
            if numpyDataDimensions < 2:
                return None
            elif numpyDataDimensions == 2:
                # Store array shape in JAPC friendly format
                jArrayShape = jp.JArray(jp.JInt)(pyValue.shape)
                return jArrayShape
            else:
                raise ValueError("JAPC does only support arrays with dimension >2, {0} were given.".format(
                    pyValue.ndim)
                )
        else:
            return None

    def _convertSimplePyValueToSimpleJValue(self, PyValue, JValueDescriptor):
        """
        sets a python understandable value into a Java Simple Parameter Value

        Code based on MATLAB version + inputs from PyJAPC

        :param PyValue: The Python value to be converted
        :param JValueDescriptor: The JAPC Parameter Value descriptor to be used to make the conversion
        :return:
        """

        # Python type of input variable
        pValT = type(PyValue)

        cern = jp.JPackage("cern")
        SimpleParameterValueFactory = cern.japc.core.factory.SimpleParameterValueFactory
        FunctionFactory             = cern.japc.value.factory.DomainValueFactory

        # In case the JValueType is not provided, I need to guess (could be improved)
        if JValueDescriptor == None:
            if isinstance(PyValue, dict):
                # ENUM or ENUMSET or DISCRETE_FUNCTION 
                if ('JAPC_FUNCTION' in PyValue.keys()) or ('X' in PyValue.keys()) and ('Y' in PyValue.keys()):
                    self.log.info('Guessed DISCRETE_FUNCTION')
                    JValueTypeHash = self.DISCRETE_FUNCTION
                elif ('JAPC_ENUM_SET' in PyValue.keys()) or ('asLong' in PyValue.keys()) or ('codes' in PyValue.keys()) or ('strings' in PyValue.keys()):
                    self.log.info('Guessed ENUM_SET')
                    JValueTypeHash = self.ENUM_SET  
                elif ('JAPC_ENUM' in PyValue.keys()) or ('code' in PyValue.keys()) or ('string' in PyValue.keys()):
                    self.log.info('Guessed ENUM')
                    JValueTypeHash = self.ENUM
                else:
                    raise(ValueError('SimplePyValueToSimpleJValue: Got a dict, but I cannot recognizes what inside...'))

            elif isinstance(PyValue, np.generic) or type(PyValue) in [int, float, str, bool]:
                # [TODO: VERY UGLY!!! MAYBE DEFINE GENERIC TYPE...] 
                # numpy scalar. assume double... 
                self.log.info('Guessed generic')
                JValueTypeHash = self.DOUBLE
            elif isinstance(PyValue, np.ndarray) and ((np.issubdtype(PyValue.dtype, np.number) or np.issubdtype(PyValue.dtype, np.flexible) or (PyValue.dtype == np.bool_))):
                self.log.info('Guessed generic numeric/string/boolean array')
                # numpy array of numbers
                if len(np.shape(PyValue)) > 1:
                    # 2D array
                    JValueTypeHash = self.DOUBLE_ARRAY_2D
                else:
                    JValueTypeHash = self.DOUBLE_ARRAY

            elif (isinstance(PyValue, np.ndarray) and  (PyValue.dtype == object)) or isinstance(PyValue, list): # list are in principle abbandoned...
                # could be strings (old way).. but also DISCRETE_FUNCTION LIST or ENUM_SET/ENUM array
                if len(PyValue) > 0:
                    if isinstance(PyValue[0], str):
                        # With data struct implemented in May2021, this should never happen... 
                        self.log.info('Guessed string array - early implementation (before May 2021)')
                        JValueTypeHash = self.STRING_ARRAY
                    elif isinstance(PyValue[0], dict):
                        # could be a DISCRETE_FUNCTION_LIST, ENUM_ARRAY,  ENUM_SET_ARRAY
                        if ('JAPC_FUNCTION' in PyValue[0].keys()) or (('X' in PyValue[0].keys()) and ('Y' in PyValue[0].keys())):
                            self.log.info('Guessed DISCRETE_FUNCTION_LIST')
                            JValueTypeHash = self.DISCRETE_FUNCTION_LIST
                        elif ('JAPC_ENUM' in PyValue[0].keys()) or ('code' in PyValue[0].keys()) or ('string' in PyValue[0].keys()):
                            self.log.info('Guessed ENUM_ARRAY')
                            JValueTypeHash = self.ENUM_ARRAY
                        elif ('JAPC_ENUM_SET' in PyValue[0].keys()) or ('asLong' in PyValue[0].keys()) or ('codes' in PyValue[0].keys()) or ('strings' in PyValue[0].keys()):
                            self.log.info('Guessed ENUM_SET_ARRAY')
                            JValueTypeHash = self.ENUM_SET_ARRAY
                        else:
                            ValueError('No descriptor provided, and I did not manage to guess the type...')
                    elif isinstance(PyValue[0], np.ndarray) or isinstance(PyValue[0], list):
                        # ENUM_ARRAY_2D, ENUM_SET_ARRAY_2D
                        if len(PyValue[0]) > 0:
                            if isinstance(PyValue[0][0], dict):
                                if ('JAPC_ENUM' in PyValue[0][0].keys()) or ('code' in PyValue[0][0].keys()) or ('string' in PyValue[0][0].keys()):
                                    self.log.info('Guessed ENUM_ARRAY_2D')
                                    JValueTypeHash = self.ENUM_ARRAY_2D
                                elif ('JAPC_ENUM_SET' in PyValue[0][0].keys()) or ('asLong' in PyValue[0][0].keys()) or ('codes' in PyValue[0][0].keys()) or ('strings' in PyValue[0][0].keys()):
                                    self.log.info('Guessed ENUM_SET_ARRAY_2D')
                                    JValueTypeHash = self.ENUM_SET_ARRAY_2D
                                else:
                                    ValueError('No descriptor provided, and I did not manage to guess the type from list of list...')
                            elif isinstance(PyValue[0][0], str):
                                # With data struct implemented in May2021, this should never happen... 
                                self.log.info('Guessed STRING_ARRAY_2D - early implementation (before May 2021)')
                                JValueTypeHash = self.STRING_ARRAY_2D
                            else:
                                ValueError('No descriptor provided, and I did not manage to guess the type from list of list...')
                        else:
                            raise ValueError('No descriptor and (probably) empty list provided... difficult to guess type...')
                    else:
                        try:
                            self.log.warning('Did not manage to guess data type... last desperate chance is that JAPC manages to do something...')
                            JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(PyValue)
                            return JNewValue
                        except:
                            self.log.error('I give up... You were trying to set a value of a parameter for which I do not have descriptor, I tried to guess the type, but failed...')
                            raise
                else:
                    raise ValueError('No descriptor and empty array/list provided or unknown dict... difficult to guess type...')
            else:
                try:
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(PyValue)
                    return JNewValue
                except:
                    self.log.error('I give up... You were trying to set a value of a parameter for which I do not have descriptor, I tried to guess the type, but failed...')
                    raise
        else:
            # Normal behavior. I can get valueType from descriptor
            JValueTypeHash = JValueDescriptor.getValueType().hashCode()
        
        # I also need to get the size, already in Java
        array2DSize = self._getPyArrayToJavaDimensions(PyValue)
        if array2DSize is not None:
            isArray2D = True
        else:
            isArray2D = False

        # properly create the new parameterValue

        ######## those simple values let handle by _getJavaValue directly ######
        # Maybe one could force the right destination type etc.. but too complicated...
        simpleTypes = (
            self.BYTE,      self.BYTE_ARRAY,    self.BYTE_ARRAY_2D,
            self.SHORT,     self.SHORT_ARRAY,   self.SHORT_ARRAY_2D,
            self.INT,       self.INT_ARRAY,     self.INT_ARRAY_2D,
            self.LONG,      self.LONG_ARRAY,    self.LONG_ARRAY_2D,
            self.FLOAT,     self.FLOAT_ARRAY,   self.FLOAT_ARRAY_2D,
            self.DOUBLE,    self.DOUBLE_ARRAY,  self.DOUBLE_ARRAY_2D,
            self.BOOLEAN,   self.BOOLEAN_ARRAY, self.BOOLEAN_ARRAY_2D,
            self.STRING,    self.STRING_ARRAY,  self.STRING_ARRAY_2D,
        )
        if (JValueTypeHash in simpleTypes):
            JValue = self._getJavaValue(PyValue)
            if isArray2D:
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JValue, array2DSize)
            else:
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JValue)
        ##### ENUMS ####
        elif (JValueTypeHash == self.ENUM):
            JNewValue = self._create_simple_enum_from_pyvalue(PyValue, JValueDescriptor)
        elif (JValueTypeHash == self.ENUM_ARRAY) or (JValueTypeHash == self.ENUM_ARRAY_2D):
            self.log.warning('PyJapcScout: TODO - code never tested: enum array')

            # make sure pyValue is a flat numpy array
            if isinstance(PyValue, np.ndarray):
                PyValue = np.array(PyValue)
            PyValue = PyValue.flatten()

            # prepare Java Array
            JNewValue = jp.JArray(jp.JPackage("cern").japc.value.EnumItem)(len(PyValue))
            # fill it with enums
            for i, enumData in enumerate(PyValue):
                JNewValue[i] = self._create_simple_enum_from_pyvalue(enumData, JValueDescriptor)

            # build up the actual new value as a simple parameter value array with proper dimensions
            if isArray2D:
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JNewValue, array2DSize)
            else:
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JNewValue)

        elif (JValueTypeHash == self.ENUM_SET):
            JNewValue = self._create_simple_enumset_from_pyvalue(PyValue, JValueDescriptor)
        elif (JValueTypeHash == self.ENUM_SET_ARRAY) or (JValueTypeHash == self.ENUM_SET_ARRAY_2D):
            self.log.warning('PyJapcScout: TODO - code never tested: enumset array')

            '''Note: copy paste of ENUM_ARRAY code!!! just different function to create enum'''
            # make sure pyValue is a flat numpy array
            if isinstance(PyValue, np.ndarray):
                PyValue = np.array(PyValue)
            PyValue = PyValue.flatten()

            # prepare Java Array
            JNewValue = jp.JArray(jp.JPackage("cern").japc.value.EnumItemSet)(len(PyValue))
            # fill it with enums
            for i, enumData in enumerate(PyValue):
                JNewValue[i] = self._create_simple_enumset_from_pyvalue(enumData, JValueDescriptor)

            # build up the actual new value as a simple parameter value array with proper dimensions
            if isArray2D:
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JNewValue, array2DSize)
            else:
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JNewValue)
        ##### DISCRETE_FUNCTION ####
        elif (JValueTypeHash == self.DISCRETE_FUNCTION):
            """
                % Note: In MATLAB we were used to handle this case:
                if isnumeric(PyValue)
                    # Per Georges -> if numeric, just try with an array of doubles...
                    warn('matlabJAPC: Something strange here... will try to continue....')
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(double(PyValue))
            """
            df = self._extract_discrete_function_from_pyvalue(PyValue)
            JNewValue = cern.japc.value.spi.value.simple.DiscreteFunctionValue(df)
        elif (JValueTypeHash == self.DISCRETE_FUNCTION_LIST):
            # Allocate JArray for DFs
            dfa = jp.JArray(jp.JPackage("cern").japc.value.DiscreteFunction)(len(PyValue))
            # Iterate over first dimension of user data
            for i, funcDat in enumerate(PyValue):
                dfa[i] = self._extract_discrete_function_from_pyvalue(funcDat)
            dfl = FunctionFactory.newDiscreteFunctionList(dfa)
            JNewValue = jp.JPackage("cern").japc.value.spi.value.simple.DiscreteFunctionListValue(dfl)
        else:
            raise TypeError('_convertSimplePyValueToSimpleJValue:: Not supported value type ('+str(JValueDescriptor.getValueType().toString())+'), or something else went wrong')
        
        return JNewValue
    
    # helper functions for _convertSimplePyValueToSimpleJValue
    def _extract_discrete_function_from_pyvalue(self, PyValue):
        """
        Build a JAPC DiscreteFunction from a Python value

        :param PyValue: can be a dict (with 'X' and 'Y' fields, or 'JAPC_FUNCTION' field (and then 'X' and 'Y')),
            or a 2D array with one (and only one!) dimension being equal 2
        :return: a JAPC DiscreteFunction
        """
        FunctionFactory = jp.JPackage("cern").japc.value.factory.DomainValueFactory
        if isinstance(PyValue, dict):
            if 'JAPC_FUNCTION' in PyValue:
                auxX = PyValue['JAPC_FUNCTION']['X']
                auxY = PyValue['JAPC_FUNCTION']['Y']
            else:
                auxX = PyValue['X']
                auxY = PyValue['Y']
        elif isinstance(PyValue, np.ndarray) and (PyValue.ndim == 2) and (2 in PyValue.shape): 
            if PyValue.shape[0] == 2:
                if PyValue.shape[1] == 2:
                    ValueError('2x2 array given... not sure how to guess what is X and what Y...')
                auxX = PyValue[0,:]
                auxY = PyValue[1,:]
            else:
                auxX = PyValue[:,0]
                auxY = PyValue[:,1]
        else:
            ValueError('I could not understand how to extract X and Y arrays for setting a JAPC_FUNCTION...')
        return FunctionFactory.newDiscreteFunction(np.array(auxX, dtype="double"), np.array(auxY, dtype="double"))

    def _create_simple_enum_from_pyvalue(self, PyValue, JValueDescriptor=None):
        ''' Creates a JAPC ENUM from Python value

        The input value can be:
        - a dict with key "JAPC_ENUM", which contains a dict that has they key "code"
        - a dict with simply the key "code"
        - an integer or a string corresponding to the enum code or value you want to set

        :param PyValue: dict or simple int or string
        :param JValueDescriptor: JAPC Value descriptor - if available (default=None)
        :return: a JAPC ENUM Java value
        '''

        cern = jp.JPackage("cern")
        SimpleParameterValueFactory = cern.japc.core.factory.SimpleParameterValueFactory

        # remove additional layer introduced in May2021
        if isinstance(PyValue, dict) and 'JAPC_ENUM' in PyValue:
            PyValue = PyValue['JAPC_ENUM']

        if JValueDescriptor == None:
            # Trying this - not TESTED
            self.log.warning('PyJapcScout: TODO - code never tested: enum without descriptor')
            if isinstance(PyValue, dict):
                if 'code' in PyValue:
                    auxPyValue = PyValue['code']
                elif 'string' in PyValue:
                    auxPyValue = PyValue['string']
                else:
                    raise ValueError('PyJapcScout: trying to set an ENUM from a dict, but I did not understand your input data...')
                JValue = self._getJavaValue(auxPyValue)
            else:
                JValue = self._getJavaValue(PyValue)
            JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JValue)
        else:
            auxEnumType = JValueDescriptor.getEnumType()
            if isinstance(PyValue, dict):
                if 'code' in PyValue:
                    auxPyValue = PyValue['code']
                elif 'string' in PyValue:
                    auxPyValue = PyValue['string']
                else:
                    raise ValueError('PyJapcScout: trying to set an ENUM from a dict, but I did not understand your input data...')
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(auxEnumType.valueOf(auxPyValue))
            else:
                # assume user gave me an integer-like or string like object
                JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(auxEnumType.valueOf(PyValue))
        return JNewValue

    def _create_simple_enumset_from_pyvalue(self, PyValue, JValueDescriptor=None):
        ''' Creates a JAPC ENUMSET from Python value

        The input value can be:
        - a dict with key "JAPC_ENUM_SET", which contains a dict that has they key "code"
        - a dict with simply the key "codes"
        - a dict with simply the key "strings"
        - an integer or a list if integers corresponding to the bits you want to set

        in PyJAPC it was done as:
        > parValNew = self._getSimpleValFromDesc(valueDescriptor)
        > # For now, enumsets can only be SET with an INT.
        > parValNew.setInt(pyVal)

        :param PyValue: dict or simple int or list of int
        :param JValueDescriptor: JAPC Value descriptor - if available (default=None)
        :return: a JAPC ENUM_SET Java value
        '''

        cern = jp.JPackage("cern")
        SimpleParameterValueFactory = cern.japc.core.factory.SimpleParameterValueFactory

        # remove additional layer introduced in May2021
        if isinstance(PyValue, dict) and 'JAPC_ENUM_SET' in PyValue:
            PyValue = PyValue['JAPC_ENUM_SET']

        if JValueDescriptor == None:
            # Trying this - not TESTED
            self.log.warning('PyJapcScout: TODO - code never tested: enumset without descriptor')
            if isinstance(PyValue, dict):
                if 'asLong' in PyValue:
                    auxEnumValueSetCode = PyValue['asLong']
                elif 'codes' in PyValue:
                    # Do a bit-wise or among oll given codes
                    auxEnumValueSetCode = np.bitwise_or.reduce(PyValue['codes'])
                else:
                    raise ValueError('PyJapcScout:: trying to set an ENUMSET without descriptor, but I did not understand your input data...')
            else:
                # assuming it is already the sum of all bits or a list of bits... let's see what happens:
                auxEnumValueSetCode = np.bitwise_or.reduce(PyValue)
            JValue = self._getJavaValue(auxEnumValueSetCode)
            JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JValue)
        else:
            # From the descriptor, one can already create an "empty" JAPC ENUM_SET, and set the appropriate bits
            auxEnumType = JValueDescriptor.getEnumType()
            JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JValueDescriptor.getValueType(), auxEnumType)

            # If it is a list/np.ndarray, try to make it look like a "standard" pyjapcscout dict
            if isinstance(PyValue, list) or isinstance(PyValue, np.ndarray):
                if all(isinstance(elem, int) for elem in PyValue):
                    PyValue = {'codes': PyValue}
                elif all(isinstance(elem, str) for elem in PyValue):
                    PyValue = {'strings': PyValue}
                else:
                    raise ValueError('PyJapcScout:: trying to set an ENUMSET, but I did not understand your input data...')

            # At this stage, either my data is a dict, or a simple value
            if isinstance(PyValue, dict):
                if 'asLong' in PyValue:
                    auxEnumValueSetCode = PyValue['asLong']
                elif 'codes' in PyValue:
                    auxEnumValueSetCode = np.bitwise_or.reduce(PyValue['codes'])
                elif 'strings' in PyValue:
                    auxEnumValueSetCode = 0
                    for string_value in set(PyValue['strings']):
                        auxEnumValueSetCode = auxEnumValueSetCode | auxEnumType.valueOf(string_value).getCode()
                else:
                    raise ValueError('PyJapcScout:: trying to set an ENUMSET, but I did not understand your input data...')
            elif isinstance(PyValue, str):
                auxEnumValueSetCode = auxEnumType.valueOf(PyValue).getCode()
            else:
                # assuming it is already the sum of all bits, or something that looks like that...
                auxEnumValueSetCode = np.bitwise_or.reduce(PyValue)

            # actual set into the java EnumSet:
            JNewValue.setEnumItemSet(cern.japc.value.spi.value.EnumItemSetImpl(auxEnumType, auxEnumValueSetCode))
        return JNewValue

    def JAvroRecordToPy(self, JRecord):
        '''
        It converts a single NXCALS Avro Record in a generic Python dict
        with all records fields converted to Python

        (see cern.nxcals.api.extraction.data.Avro.records)

        Args:
            record: Avro record to be converted.

        Returns:
            (dict): dictionary with all data hopefully converted as Python objects...
        '''
        java = jp.JPackage("java")
        org = jp.JPackage("org")
        output = dict()
        for _field in JRecord.getSchema().getFields():
            _name = _field.name()
            _value = JRecord.get(_name)
            if isinstance(_value, org.apache.avro.generic.GenericData.Record):
                # iterate for the time being...
                _value = self.JAvroRecordToPy(_value)
            elif isinstance(_value, org.apache.avro.generic.GenericData.Array):
                _value = np.array(_value)
                if _value.dtype == object:
                    if len(_value.flatten()) > 0 and isinstance(_value.flatten()[0], java.lang.CharSequence):
                        _value = _value.astype(np.str_)
                    else:
                        print(f'Unknown array type for {_name}: "{type(_value)}"... need to improve this function! write davide.gamba@cern.ch')
            elif isinstance(_value, java.lang.Long) or isinstance(_value, java.lang.Integer):
                _value = int(_value)
            elif isinstance(_value, java.lang.Boolean):
                _value = bool(_value)
            elif isinstance(_value, java.lang.Double) or isinstance(_value, java.lang.Float):
                _value = float(_value)
            elif isinstance(_value, java.lang.CharSequence):
                _value = str(_value)
            elif _value is None:
                _value = ''
            else:
                print(f'Unknown data type for {_name}: "{type(_value)}"... need to improve this function! write davide.gamba@cern.ch')
            output[_name] = _value

        # check if it was an array
        _auxkeys = output.keys()
        if len(_auxkeys) == 2 and 'dimensions' in _auxkeys and 'elements' in _auxkeys:
            #print('array detected...')
            _dim = output['dimensions']
            _ele = output['elements']
            if len(_dim) == 1:
                output = _ele
            elif len(_dim) == 2:
                auxNRows = _dim[0]
                auxNColumns = _dim[1]
                output = np.reshape(_ele, (auxNRows, auxNColumns))
            else:
                print('JAvroRecordToPy: need to improve something here... write davide.gamba@cern.ch')
        return output
     
    def PyAvroRecordToScoutDict(self, PyRecord, JValueDescriptor):
        '''
        It converts a single NXCALS Avro Record already converted in Python into a pyjapcscout-like
        acquisition dictionary.
        In order to do so, a Java Value Descriptor must be provided.

        Returns:
            (dict): dictionary with all data hopefully converted as standard pyjapcscout acquisition
        '''

        # I must have a descriptor to make sense
        if JValueDescriptor == None:
            # don't know how to treat data... return everything
            raise ValueError('I cannot convert a record to a PyJapcScout-like acquisition without a descriptor!')

        # check if device is the right one
        if (PyRecord['device'] + '/' + PyRecord['property']) not in JValueDescriptor.getName():
            print('No data in this record for this descriptor...')
            return None

        def _simpleExtract(record, field_name, JSimpleDescriptor):
            if field_name in record:
                # here we could also do some type manipulation...
                # _field_type = JSimpleDescriptor.getValueType()
                return record[field_name]
            else:
                raise KeyError("I could not find field {} in the present record".format(field_name))

        def _extractHeader(record):
            output = dict()
            standard_field_map = {'acqStamp':'acqStamp',
                            'cycleStamp':'cyclestamp',
                            'setStamp':'setStamp',
                            'isFirstUpdate':'isFirstUpdate',
                            'isImmediateUpdate':'isImmediateUpdate',
                            'selector':'selector'}
            for _field_name in standard_field_map.keys():
                if standard_field_map[_field_name] in record:
                    output[_field_name] = record[standard_field_map[_field_name]]
                else:
                    # not found this header info... set empty string..
                    output[_field_name] = ''
            return output

        output = dict()
        output['header'] = _extractHeader(PyRecord)
        output['exception']  = ''
        # extract actual value(s):
        JValueTypeHash = JValueDescriptor.getType().hashCode()
        if (JValueTypeHash == self.SIMPLE):
            try:
                output['value'] = _simpleExtract(PyRecord, JValueDescriptor.getName().split(sep='#')[1], JValueDescriptor)
            except KeyError as e:
                output['value'] = ''
                print(e)
                #output['exception'] += e.message+'\n'
        elif (JValueTypeHash == self.MAP):
            output['value'] = dict()
            _fields = JValueDescriptor.getNames()
            for _field in _fields:
                _field_desc = JValueDescriptor.get(_field)
                if _field_desc is not None:
                    try:
                        output['value'][_field] = _simpleExtract(PyRecord, _field, _field_desc)
                    except KeyError as e:
                        output['value'][_field] = ''
                        print(e)
                        #output['exception'] += e.message+'\n'
                else:
                    raise ValueError('Strange bug... check with davide.gamba@cern.ch')
        else:
            raise ValueError('Another strange bug... check with davide.gamba@cern.ch')
        return output



