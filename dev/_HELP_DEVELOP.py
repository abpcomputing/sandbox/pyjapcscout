# this file is just a place holder for simple examples used locally during development of pyjapcscout
# it is not meant to be clean/working/usable ...
#
# davide - Feb 2022

# to allow reloading apackage
from importlib import reload  # Python 3.4+

import help_functions
help_functions = reload(help_functions)
from help_functions import treatIPMData
from help_functions import setup_nice_plots
from help_functions import treatSchottkyData



## simple use




# For JAVA devlopm
# ent
# pyjapc and C

import jpype as jp
from pyjapcscout import PyJapcScout
myPyJapc = PyJapcScout()
myPyJapc.rbacLogin('dgamba')

def getSimpleNXCALS(self, parameterName, selectorOverride=None, cycleOverride=None, referenceTime_ns=None, part='value',
                  completeOutput=False):
    '''
    Based on https://gitlab.cern.ch/acc-logging-team/nxcals-examples/-/tree/master/extraction-api-thin-examples

    '''
    return


import jpype as jp
import datascout
cern = jp.JPackage("cern")
java = jp.JPackage("java")
System = jp.JClass("java.lang.System")
System.setProperty("service.url",
                   "https://cs-ccr-nxcals5.cern.ch:19093,https://cs-ccr-nxcals5.cern.ch:19094,https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals6.cern.ch:19094,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19094,https://cs-ccr-nxcals8.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19094");

SYSTEM_NAME = 'CMW'
SPARK_SERVERS_URL = "cs-ccr-nxcals5.cern.ch:15000,cs-ccr-nxcals6.cern.ch:15000,cs-ccr-nxcals7.cern.ch:15000,cs-ccr-nxcals8.cern.ch:15000"


startTime = datascout.string_to_unixtime('2021-09-14 16:09')
endTime = datascout.string_to_unixtime('2021-09-14 16:19')
startTime = datascout.string_to_unixtime('2021-11-10 16:50')
endTime = datascout.string_to_unixtime('2021-11-10 17:00')
parameterName = 'LNE.APULB.0030/Acquisition#totalIntensityCalcCh1'
parameterName = 'LNE.APULB.0030/Acquisition'
parameterName = 'LNA.TPU_INTSY-SD/Samples'

selector = 'LNA.USER.MD4'

parameterName_split = parameterName.split('#')
if len(parameterName_split) == 2:
    parameter, field = parameterName_split
    script = cern.nxcals.api.extraction.thin.data.builders.DevicePropertyDataQuery.builder().system(
        SYSTEM_NAME).startTime(startTime).endTime(endTime).entity().parameter(parameter).build()
    script += f'.select(\"acqStamp\",\"selector\",\"{field}\").where(\"selector == \'{selector}\'\")'
elif len(parameterName_split) == 1:
    script = cern.nxcals.api.extraction.thin.data.builders.DevicePropertyDataQuery.builder().system(
        SYSTEM_NAME).startTime(startTime).endTime(endTime).entity().parameter(parameterName).build()
    script += f'.where(\"selector == \'{selector}\'\")'
else:
    raise ValueError('Cannot understand your parameter name....')


compression = cern.nxcals.api.extraction.thin.AvroQuery.Codec.BZIP2
avroQuery = cern.nxcals.api.extraction.thin.AvroQuery.newBuilder().setScript(script).setCompression(compression).build()

extractionService = cern.nxcals.api.extraction.thin.ServiceFactory.createExtractionService(SPARK_SERVERS_URL)
if False:
    extractionService.getChannel().shutdown()

if False:
    extractionService.getChannel().isShutdown()
    extractionService.getChannel().isTerminated()
# perform query
avroData = extractionService.query(avroQuery)

# treat AVRO data as Avro records
records = cern.nxcals.api.extraction.data.Avro.records(avroData)
# One could in principle convert them to cmw records, but this conversion often fails... forgetting about it
#cmwRecords = cern.nxcals.api.extraction.data.Datax.records(avroData)

if len(records) > 0:
    lastRecord = records.get(len(records)-1)
else:
    print('no data found')
    #return None

output = dict()
for _field in lastRecord.getSchema().getFields():
    _name = _field.name()
    _value = lastRecord.get(_name)
    if isinstance(_value, java.lang.Long) or isinstance(_value, java.lang.Integer):
        _value = int(_value)
    elif isinstance(_value, java.lang.Boolean):
        _value = bool(_value)
    elif isinstance(_value, java.lang.Double) or isinstance(_value, java.lang.Float):
        _value = bool(_value)
    elif isinstance(_value, java.lang.CharSequence):
        _value = str(_value)
    elif _value is None:
        _value = ''
    else:
        print(f'Unknown data type for {_name}: "{type(_value)}"... need to improve this function! ')
    output[_name] = _value

import numpy as np
import jpype as jp
import datascout

b=parse_record(output)








script += ".select('acqStamp',\"totalIntensityCalcCh1\",\"selector\")"

script += f'.where(\"selector\" == \"{selector}\")'

script += f'.select(\"acqStamp\",\"totalIntensityCalcCh1\").where(\"selector\" == \"{selector}\")'

script += f'.select(\"acqStamp\",\"totalIntensityCalcCh1\",\"selector\").where(\"selector == \'{selector}\'\")'
script += f'.select(\"acqStamp\",\"totalIntensityCalcCh1\").where(\"selector == \'{selector}\'\")'

parameterName = 'LNE.APULB.0030/Acquisition#totalIntensityCalcCh1'






descript = ccs_descriptor.findValueDescriptor('LNA.BPM/SchottkyGuruSettings')
print([str(name) for name in descript.getNames()])


########################################
# For PjLSA
from pyjapcscout import PyJapcScout
import jpype as jp
myPyJapc = PyJapcScout(incaAcceleratorName='ELENA', defaultSelector='LNA.USER.MD5')
#self = PyJapcScout(incaAcceleratorName=None, defaultSelector='LNA.USER.MD5')
self=myPyJapc

self.getCycleNameFromUser('LNA.USER.MD5')
self.getUserFromCycleName('Short_isabella_2021')
lsaparams = self.findLSAParametersByName('SPSBEAM%')

print(self.getJavaValueDescriptor(lsaparams[0]).toString())
print(self.getJavaParameterDescriptor(lsaparams[0]).toString())



part="value"
parameterName = 'AIX.SRC-BEAM/Delay#delay'
value = 76

parameterName = 'AIX.W-SRC/Delay#delay'
value = 521

selectorOverride = 'LNA.USER.MD5'
selectorOverride = None
selectorOverride = ''
cycleOverride = 'Short_isabella_2021'
cycleOverride = None
description = 'test description'

referenceTime_ns = 1644496930440238525-24*60*60*1e9
referenceTime_ns = None
self.getSimpleTrim(parameterName, selectorOverride = selectorOverride, cycleOverride = cycleOverride, referenceTime_ns = referenceTime_ns, part=part)
self.setSimpleTrim(parameterName, value, description=description, selectorOverride=selectorOverride, cycleOverride=cycleOverride, part=part)


parameterName = 'LNS.SRC-BCTCALWF/FunctionSetting#functions'
value = self.getSimpleValue(parameterName, selectorOverride='')
valueComplete = self.getValues(parameterName, selectorOverride='')
valueCompleteTime = valueComplete[parameterName]['header']['acqStamp']

cycleName = cycleOverride
cycleName = ''

cern = jp.JPackage("cern")
serviceLocator   = cern.lsa.client.ServiceLocator
trimService      = serviceLocator.getService(cern.lsa.client.TrimService)
parameterService = serviceLocator.getService(cern.lsa.client.ParameterService)
contextService   = serviceLocator.getService(cern.lsa.client.ContextService)
TrimRequest      = cern.lsa.domain.settings.TrimRequest
ValueFactory     = cern.accsoft.commons.value.ValueFactory
ParameterValueConverter = cern.lsa.domain.exploitation.ParameterValueConverter


parameter = parameterService.findParameterByName(parameterName)
parameter.toString()


# get right beam process
if cycleName == '':
    acceleratorJObj = parameter.getAccelerator()
    cycle = contextService.findResidentNonMultiplexedContext(acceleratorJObj)
else:
    cycle = contextService.findStandAloneCycle(beamProcess)
cycle.toString()

part = cern.lsa.domain.settings.SettingPartEnum.VALUE
part = cern.lsa.domain.settings.SettingPartEnum.TARGET
part = cern.lsa.domain.settings.SettingPartEnum.CORRECTION

auxDescriptor = self.getJavaValueDescriptor(parameterName, forceCCSDescriptorSource=False, verbose=False)
# convert to Java (JAPC):
auxData = self._dataConverter.PyToJValue(value, auxDescriptor)
# convert to LSA value
auxData = ParameterValueConverter.convertToImmutableValue(parameter, auxData)
trimRequest = TrimRequest.builder().setContext(cycle).setDescription(description).setSettingPart(part)


if parameter.getValueType().isScalar():
    trimRequest = trimRequest.addScalar(parameter, cycle, auxData)
elif parameter.getValueType().isFunction():
    trimRequest = trimRequest.addFunction(parameter, auxData)
else:
    raise ValueError(f'Unknwon parameter type {parameter.getValueType().toString()}')

response=trimService.trimSettings(trimRequest.build())
response.isDrivePerformed()


%%%%%%% TESTS

TrimRequest.builder().addScalar()
trimRequest = TrimRequest.builder().setDescription(description).setSettingPart(part).addScalar(parameter, cycle, auxData)

TrimRequest.builder().addFunction()


auxData = ParameterValueConverter.convertToParameterValue(auxData)
auxData = ParameterValueConverter.convertToFailSafeImmutableValue(parameter, auxData)

trimRequest = TrimRequest.builder()
trimRequest = TrimRequest.builder().addFunction(parameter, '')
trimRequest = TrimRequest.builder().setSettingPart(part).addValue(parameter, cycle.getBeamProcesses()[0], auxData)


cern.lsa.domain.settings.BeamProces
cycle.getBeamProcesses().toString()

########################################
# for getting trim history
referenceTime_ns = 1644496930440238525

cern = jp.JPackage("cern")
java = jp.JPackage("java")
serviceLocator   = cern.lsa.client.ServiceLocator
contextService   = serviceLocator.getService(cern.lsa.client.ContextService)
parameterService = serviceLocator.getService(cern.lsa.client.ParameterService)
settingService   = serviceLocator.getService(cern.lsa.client.SettingService)
requestBuilder   = cern.lsa.domain.settings.ContextSettingsRequestBuilder()




# get parameter
parameter = parameterService.findParameterByName(parameterName)
parameter.toString()

# put parameter in a list of parameters..
parameters = java.util.LinkedList()
parameters.add(parameter)

# get right beam cycle name
if cycleName == '':
    acceleratorJObj = parameter.getAccelerator()
    cycle = contextService.findResidentNonMultiplexedContext(acceleratorJObj)
else:
    cycle = contextService.findStandAloneCycle(beamProcess)
cycle.toString()

# build up request
myRequest = requestBuilder.standAloneContext(cycle) \
    .parameters(parameters) \
    .at(java.util.Date(int(referenceTime_ns/10**6)).toInstant()) \
    .build()

# get parameter settings from trim history according to request
contextSettings = settingService.findContextSettings(myRequest)
parameterSettings = contextSettings.getParameterSettings(parameter)

output = []
if parameterSettings is not None:
    _allSettings=parameterSettings.getSettings()
    for _singleSetting in _allSettings:
        # extract header info
        creationStamp = np.int64(_singleSetting.getCreationDate().toInstant().toEpochMilli()*10**6)
        trimId        = np.int64(_singleSetting.getTrimId())
        beamProcess   = np.str_(_singleSetting.getBeamProcess().toString())

        # extract interesting part
        _singleValue = _singleSetting.getValue()
        #_singleValue = _singleSetting.getTargetValue()
        #_singleValue = _singleSetting.getCorrectionValue()

        # convert to Python
        _singleParamValue = ParameterValueConverter.convertToParameterValue(_singleValue)
        _singleParamValuePy = self._dataConverter._convertValueValueToPy(_singleParamValue)

        # wrap up
        output.append({'value': _singleParamValuePy, 'trimId': trimId, 'creationStamp':creationStamp})
if len(output) == 0:
    output = None
elif len(output) == 1:
    output = output[0]

return output





# for a range of trims
trimService      = serviceLocator.getService(cern.lsa.client.TrimService)
trimRequestBuilder = cern.lsa.domain.settings.TrimHeadersRequestBuilder()

trimListRequest = trimRequestBuilder.beamProcesses(cycle.getBeamProcesses()) \
    .parameters(parameters) \
    .build()
    
trimList = trimService.findTrimHeaders(trimListRequest)
trimListRequest = trimRequestBuilder.beamProcesses(cycle.getBeamProcesses()) \
    .startingFrom(java.util.Date(int(referenceTime_ns/10**6-60000*10)).toInstant()) \
    .parameters(parameters) \
    .maxLastTrimsNumber(java.lang.Integer(10)) \
    .build()
    
trimList = trimService.findTrimHeaders(trimListRequest)


contextService.
_context = cern.inca.client.impl.context.ContextUtilities.findCycle(LSAcycle);

_context = cycle
cip = cern.lsa.domain.settings.Settings.computeContextValue(_context, parameterSettings)

cern.lsa.domain.settings.Settings.computeContextValue(


output = dict()

a=parameterSetting.getSettings()
for b in a:
    c = ParameterValueConverter.convertToParameterValue(b.getValue())
    self._dataConverter._convertValueValueToPy(c)

    continue


    ParameterValueConverter.convertToFailSafeImmutableValue(parameter, b.getValue())
    ParameterValueConverter.convertToFailSafeImmutableValue(parameter, b.getValue())


self._dataConverter._convert

self._dataConverter.JAcquiredValueToPy(c)
self._dataConverter.JAcquired

ParameterValueConverter.convertToFailSafeImmutableValue(parameter, parameterSetting)
self._dataConverter.JAcquiredValueToPy(parameterSetting)
self._

if parameterSetting is None:
    continue
parameterSetting.getSetting(cycle)

immutableValue = cern.lsa.domain.settings.Settings.computeContextValue(cycle, settings)
immutableValue = cern.lsa.domain.settings.Settings.computeContextValue(cycle, contextSettings)

if settings is not None:
    settings = settings.getSettings()
    for setting in settings:
        setting

if (nargin == 2 | | isempty(date_ns))
    myRequest = requestBuilder.standAloneContext(standAloneContext).parameters(foundParams).build();
else
    myRequest = requestBuilder().standAloneContext(standAloneContext).at(
        java.util.Date(date_ns * 1e-6).toInstant).parameters(foundParams).build();
end
