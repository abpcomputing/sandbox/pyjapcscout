pyjapcscout
===========

Introduction
------------

The purpose of PyJapcScout is not to replace PyJapc, but is a wrapper on top of PyJapc to explore the possibility of a
more convenient interface for MD studies.
It also provides some additional features that can as well be useful for MD studies in the CERN injectors.

A key feature of this package is the dataformat which must be compatible with ``datascout`` package.
I.e. all values returned are (unless requested otherwise) simple python/numpy object that can be easily stored into
parquet files and/or used with standard data handling packages like ``pandas`` and ``awkward`` array.


Installation and use
------------

Using the `acc-py Python package index <https://wikis.cern.ch/display/ACCPY/Getting+started+with+acc-python#Gettingstartedwithacc-python-OurPythonPackageRepositoryrepo>`_
``pyjapcscout`` can be pip installed in a new acc-py virtual environment with::

    source /acc/local/share/python/acc-py/base/pro/setup.sh
    acc-py venv ./testvenv
    source ./testvenv/bin/activate
    python -m pip install pyjapcscout

At this point, within a python terminal, the simplest example can be to **GET a parameter** via JAPC:

.. testcode::

    from pyjapcscout import PyJapcScout
    myPyJapc = PyJapcScout(incaAcceleratorName='CTF')
    print(myPyJapc.getSimpleValue('CA.BHB0900/SettingPPM#current',selectorOverride=''))

.. testoutput::

    59.47

It can also be used (as a wrapper over pyccda) to **get basic info from CCDA**, for example look for devices:

.. testcode::

    myPyJapc.findDevicesByName('CK.PKI*A*ST')

.. testoutput::

    CK.PKI15A-ST [XenericSampler v8.7.1]: Treated Pow. Klyst.15 Incid. Ampl.
    CK.PKI11A-ST [XenericSampler v8.7.1]: Treated Pow. Klyst.11 Incid. Ampl.
    ['CK.PKI15A-ST', 'CK.PKI11A-ST']

Or to **get a trim** from the trim history (as a wrapper over pjlsa), for example:

.. testcode::

    import datascout
    referenceTime_ns = datascout.string_to_unixtime('2021-09-14 16:00')
    myPyJapc.getSimpleTrim('CA.BHB0900/SettingPPM#current', selectorOverride = '', referenceTime_ns = referenceTime_ns)

.. testoutput::

    61.500008


Documentation contents
----------------------

.. toctree::
    :maxdepth: 1
    :hidden:

    self

.. toctree::
    :caption: pyjapcscout usage
    :maxdepth: 1

    usage
    notebook

.. toctree::
    :caption: Reference docs
    :maxdepth: 1

    api
    genindex

