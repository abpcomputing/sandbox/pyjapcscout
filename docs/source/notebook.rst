.. _notebook:

Usage in Jupiter notebook
=====

Simple/possible example to start using PyJapc(Scout) from a Jupyter notebook.


Setup from scratch
-----

1. First, connect to your virtual machine (or some machine with access to TN).
2. Setup a new environment and install the minimum necessary

.. testcode::

    # start from ACC-PY base distribution
    source /acc/local/share/python/acc-py/base/pro/setup.sh
    # create and activate your own environmnet
    acc-py venv ./testvenv
    source ./testvenv/bin/activate
    # install desired software
    python -m pip install pyjapcscout
    python -m pip install jupyter

3. Start a Jupyter notebook

.. testcode::

    # activate your environment (if not done already)
    source ./testvenv/bin/activate
    # start Jupyter notebook
    jupyter notebook --no-mathjax --no-browser --ip 0.0.0.0 --port 8899 --notebook-dir=.

4. A web address similar to the following one should appear:

.. testoutput::

    [...]
    [I 13:23:19.644 NotebookApp] http://cwe-513-volXXX.cern.ch:8899/?token=aaaaaaaaaaaaaaaaaaaaaaaaaa
    [...]

which allows you to start working with your Jupyter notebook simply pointing your local browser to the mentioned addressed.
If no ``token`` is displayed, you might have to specify a notebook password, which you will then need to use to login
into the notebook-app in your browser:

.. testcode::

    jupyter notebook password

.. warning::

    Clearly, things can be bit more complicated if you need to jump over a few hosts to have access to the TN...

