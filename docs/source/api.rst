.. _API_docs:

pyjapcscout API documentation
==============================

.. rubric:: Modules

.. autosummary::
   :toctree: api

   .. Add the sub-packages that you wish to document below

   pyjapcscout
