.. _usage:

Simple Usage Examples
=====

The main features of ``pyjapcscout`` are shown in the following simple examples.


Setup
-----

.. testcode::

    from pyjapcscout import PyJapcScout
    myPyJapc = PyJapcScout(incaAcceleratorName='CTF', defaultSelector='SCT.USER.SETUP', 
        noSet=False, timeZone="utc", unixtime=True, logLevel=None)


GET/SET a simple parameter (e.g. a simple number)
-----

.. testcode::

    value = myPyJapc.getSimpleValue('CA.BHB0900/SettingPPM#current',selectorOverride='')
    myPyJapc.setSimpleValue('CA.BHB0900/SettingPPM#current', value, selectorOverride='')


GET/SET a list of parameters
-----

.. testcode::

    parameters = ['CA.BHB0400/SettingPPM#current', 'CA.BHB0900/SettingPPM#current']
    values = myPyJapc.getValues(parameters,selectorOverride='')
    print(values)
    myPyJapc.setValues(parameters, values, selectorOverride='')

MONITOR a list of parameters
-----

.. testcode::

    def mycallback(newdata, monitorHandler, *args, **kwargs):
        print('new data recieved by user')
        print(newdata)

    parameters = ['CA.BHB0400/SettingPPM#current', 'CA.BHB0900/SettingPPM#current', 'CA.BHB0400/Acquisition#currentAverage', 'CA.BHB0900/Acquisition']
    myMonitor = myPyJapc.createMonitor(parameters, onValueReceived=mycallback, selectorOverride = None,
                        groupStrategy = 'standard',
                        allowManyUpdatesPerCycle=False, strategyTimeout=-1, forceGetOnChangeAndConstantValues=False,
                        dataSanityCheck = PyJapcScout.simpleDataSanityCheck)

    myMonitor.startMonitor()
    [...]
    myMonitor.stopMonitor()

This **monitoring** object is where we have some new features with respect to plane PyJapc:

1. Possibility to save data as it comes:

.. doctest::

    >>> myMonitor.saveDataPath = './data/'
    >>> myMonitor.saveDataFormat = 'parquet' # or 'pickle'  ( maybe in the future 'mat' or other data formats)
    >>> myMonitor.saveData = True


2. Possibility of using more "complex" parameter grouping strategies:
    - 'standard': is a simple PyJapc group subscription, using the underlaying PushGroupSubscriptionStrategy provided by JAPC
    - 'extended': is the "fast" strategy developed for CTF3/CLEAR which allows, for example to have several updated from a device from the same cycle (i.e. with equal cycleStamps)
    - 'multiple': each parameter is subscribed as a single subscription in a standard PyJapc subscription. Note that the same user callback will be called in this case with the data of a single parameter.

3. The monitoring object has inside **last data received**:

.. doctest::

    >>> myMonitor.lastData # a dictionary with the last dataset received for all parameters.

4. The possibility to define a user-defined data sanity check function which allows to condition 
the **data saving** and the user-defined **callback function execution**:

.. doctest::

    >>> # Define simple sanity check function:
    >>> def simpleDataSanityCheck(data, *args):
    >>>    ''' Use/Save data only if there is no exception in any acquired parameter '''
    >>>    for key in data:
    >>>        if data[key]['exception'] != '':
    >>>            return False
    >>>    return True
    >>> # Add it to the already-created monitor:
    >>> myMonitor.dataSanityCheck = simpleDataSanityCheck

Note: the above simple function is also provided as static method of PyJapcScout: see ``PyJapcScout.simpleDataSanityCheck``

Digression on Data Format
-----

One of the reasons to prepare this interface is to allow to explore other data conversion from the JAPC/JAVA objects returned by JAPC into python-friendly data.
This mainly with the aim of ease the process of saving data, but also in using the data in scripts.
The general philosophy adopted is:

- Don't use tuples (they are not well digested by some save data formats)
- whatever you GET you can also SET it back
- numerical objects are all converted to numpy objects, preserving as much as possible the precision of the data type (e.g. a JAVA "short" goes into a "numpy.int16")
- headers, exceptions, etc are as much as possible preserved

The data coming/set from/to PyJapcScout is expected to be typically a dictionary. Inside there will only be `numpy values`, `numpy arrays` organised in nested `dictionaries`.

**NOTE:** The data format might change in the future as the purpose of this project is indeed to explore more convenient data formats in particular to save data and to ease the post-processing.

Data handling, including saving/loading
-----

.. warning::

    The class PyJapcScoutData, used in the very first version of PyJapcScout, has been removed! 

Data handling is presently done using sweet functions provided in a separate packaged called `datascout <https://gitlab.cern.ch/abpcomputing/sandbox/datascout.git>`_.
The idea is that data coming from PyJapcScout is "simple enough" that it can be easily saved to `parquet` (and `pickle`) using `datascout` methods, as well as to convert acquired data to well known data handling packages like:

- `pandas <https://pandas.pydata.org/>`_: `datascout.dict_to_pandas(<My dict acquired with PyJapcScout>)`
- `awkward <https://awkward-array.org/>`_: `datascout.dict_to_awkward(<My dict acquired with PyJapcScout>)`



Additional/Experimental features
-----

Wrapping over pyccda
^^^^^

At a first order, `pyccda <https://acc-py.web.cern.ch/gitlab/controls-configuration-service/controls-configuration-data-api/accsoft-ccs-ccda/docs/stable/>`_ is meant to allow one to have access to the data one typically look via `ccde web interface <https://ccde.cern.ch>`_.
During an **MD session**, one is often interested just to **search for a device** and/or to list the **properties** that such a device/class has.
``PyJapcScout`` provides simple wrapper functions over  to find **devices** and **device properties**:

.. doctest::

    >>> from pyjapcscout import PyJapcScout
    >>> myPyJapc = PyJapcScout()

    >>> # look for devices via CCDE matching a given pattern (all keyword arguments are optional)
    >>> pattern = 'AIX.SRC*'
    >>> myPyJapc.findDevicesByName(pattern, operationalOnly=True, simplifiedOutput=True, includeAlias=True, verbose=True)
    AIX.SRC-BEAM [LTIM v6.1.0]: None
    AIX.SRC-BCTCAL [LTIM v6.1.0]: None
    AIX.SRC [LTIM v6.1.0]: Elena Ion Source trigger
    AIX.SRC-LSRC [LTIM v6.1.0]: Injection trigger for OASIS
    AIX.SRC-BEAMON [LTIM v6.1.0]: Start injection kicker
    ['AIX.SRC-BEAM', 'AIX.SRC-BCTCAL', 'AIX.SRC', 'AIX.SRC-LSRC', 'AIX.SRC-BEAMON']

    >>> # find all classes via CCDE matching a given pattern (all keyword arguments are optional)
    >>> pattern = 'LTIM*'
    >>> myPyJapc.findDeviceClassByName(pattern, operationalOnly=True, simplifiedOutput=True, verbose=True)
    LTIM v21051 [FESA]: Local Timing, control over Central Timing Receiver (CTR) modules
    LTIM v21060 [FESA]: Local Timing, control over Central Timing Receiver (CTR) modules
    LTIM v21062 [FESA]: Local Timing, control over Central Timing Receiver (CTR) modules
    LTIM v21063 [FESA]: Local Timing, control over Central Timing Receiver (CTR) modules
    LTIMRNB v21060 [FESA]: Full template
    LTIM v21065 [FESA]: Local Timing, control over Central Timing Receiver (CTR) modules
    LTIMRNB v0.1.0 [FESA]: An Imported fesa class
    [...]

    >>> # get all properties of a given class (all keyword arguments are optional)
    >>> #  Note 1: one can/should specify a version, otherwise the last (operational) version is provided
    >>> name = 'LTIM'
    >>> myPyJapc.findDeviceClassProperties(name, version=None, operationalOnly=True, simplifiedOutput=True, verbose=True)
    --- Properties for class name=LTIM version=6.2.0 :
    Acquisition: None
      acqCHW [scalar]: raw hardware time of interrupt with respect to the begin of the cycle (in milliseconds) 
      acqCNano [scalar]: Time of interrupt with respect to the begin of the cycle (in nanoseconds) 
      acqC [scalar]: Time of interrupt with respect to the begin of the cycle (in milliseconds) 
      acqNANO [scalar]: UTC time of interrupt (in nanoseconds) 
      acqUTC [scalar]: UTC time of interrupt 
    ChannelInfo: None
      channel [scalar]: None
    ltimsInChannel [array]: None
      logicalUnitNumber [scalar]: None
      additionalOutputs [array]: None
    Clock: Defines which clock feeds the counter
    [...]

    >>> #  Note 2: sometimes there are classes which are marked as "development" by are used by "operational" devices
    >>> #          In this case, the function gives you a warning that no operational version of the class is found, but still
    >>> #          gives you the properties of the last available version it finds...
    >>> name = 'FGC_93'
    >>> myPyJapc.findDeviceClassProperties(name, version=None, operationalOnly=True, simplifiedOutput=True, verbose=True)
    WARNING:pyjapcscout:I did not find an operational class named FGC_93... I will try with non-operational ones...
    --- Properties for class name=FGC_93 version=0 :
    FGC: None
      FAULTS [scalar]: None
    FGC.FAULTS: None
      value [scalar]: None
    LOAD: None
      SELECT [scalar]: None
    LOAD.SELECT: None
    [...]


Wrapping over pjlsa
^^^^^

`pjlsa <https://gitlab.cern.ch/scripting-tools/pjlsa>`_ allows to GET/SET many settings directly in LSA, i.e. not necessarily on a *mapped cycle*.
Within ``PyJapcScout`` a few minimal functions are provided to cover the simplest needs for a typical MD.

.. warning::

    In order to profit of those features, your PyJapcScout **MUST** be configured to use InCA at startup!

1. **Obtain simple information**, like in which user a cycle is mapped and find LSA parameters (not necessarily defined in CCDE)

.. doctest::

    >>> from pyjapcscout import PyJapcScout
    >>> import datascout
    >>> import matplotlib.pyplot as plt
    >>> import copy
    >>> 
    >>> myPyJapc = PyJapcScout(incaAcceleratorName='ELENA', defaultSelector='LNA.USER.MD5')
    >>> 
    >>> # check which cycle is mapped to which user or viceversa...
    >>> user_to_look_at = 'LNA.USER.MD5'
    >>> cycle_name = myPyJapc.getCycleNameFromUser(user_to_look_at)
    >>> user_mapped = myPyJapc.getUserFromCycleName(cycle_name)
    >>> print(f'{user_to_look_at} is used by {cycle_name} which is indeed mapped on {user_mapped}')
    LNA.USER.MD5 is used by Hminus-Acceleration_Deceleration_Lajos which is indeed mapped on LNA.USER.MD5

    >>> # Look for parameters in LSA
    >>> lsaparams = myPyJapc.findLSAParametersByName('ELENABEAM/Q%')
    >>> print(lsaparams)
    ['ELENABEAM/QH#value', 'ELENABEAM/QV#value']

    >>> ##### For any JAPC-reachable parameter we can typically get a description
    >>> # get the **JAPC** **value** descriptor of one of such a parameter
    >>> myPyJapc.getJavaValueDescriptor(lsaparams[0], forceCCSDescriptorSource = False, verbose=True)
    Value descriptor   for ELENABEAM/QH
    Title=               [QH]
    Description=         [Horizontal tune]
    Type=                [Simple]
    ValueType=           [DiscreteFunction]
    RowCount=            [1]
    ColumnCount=         [1]
    Length=              [1]
    [...]

2. **Get** the last trim made via LSA on a given parameter, eventually with respect to a user-specified **reference time**:

.. doctest::

    >>> ##### GET a TRIM from history on "correction" part
    >>> parameterName = 'AIX.SRC-BEAM/Delay#delay'
    >>> part="correction"
    >>> 
    >>> referenceTime_ns = datascout.string_to_unixtime('2021-09-14 16:09')
    >>> old_value = myPyJapc.getSimpleTrim(parameterName, selectorOverride = '', referenceTime_ns = referenceTime_ns, part=part, completeOutput=True)
    >>> old_time = datascout.unixtime_to_string(old_value['creationStamp'], time_zone = 'cern')
    >>> old_value = old_value['value']
    >>> print(f'parameterName value was {old_value} on {old_time}')
    parameterName value was -1 on 2021-08-25 10:36:29.095000


3. **Trim** a list of LSA parameters in a single "SET" (i.e. for profiting of transactional features in LSA etc.)

    >>> cycleOverride = 'Short_isabella_2021'
    >>> part="correction"
    >>> parameterNames = ['ELENABEAM/QH#value', 'ELENABEAM/QV#value']
    >>> present_values = myPyJapc.getTrims(parameterNames, selectorOverride = None, cycleOverride = cycleOverride, part=part)
    >>> for value in present_values:
    >>>     print(f'{value} was trimmed on {datascout.unixtime_to_string(present_values[value]["header"]["creationStamp"], time_zone = "cern")}')
    ELENABEAM/QH#value was trimmed on 2022-03-03 16:28:45.845000
    ELENABEAM/QV#value was trimmed on 2022-03-03 16:28:45.845000
    >>> # Actually perform a TRIM. It will return True if all goes well... (RBAC might be needed ;))
    >>> myPyJapc.setTrims(parameterNames, present_values, description='setting back via pyjapcscout', selectorOverride=None, cycleOverride=cycleOverride, part=part)
    True


GET data from NXCALS
^^^^^

In principle, one could wonder how to get data of a given parameter from NXCALS at a given date (assuming it was logged), during an MD session and while getting other data from JAPC.
Among the different API to extract data from NXCALS, an "simple" one is `nxcals-thin <https://nxcals-docs.web.cern.ch/current/user-guide/extraction-api-thin/>`_.
This library works with JPype - as PyJapc - and therefore it can be easily installed/loaded in a user-made environment. 

.. warning::

    This is the most **experimental feature** of PyJapcScout: data format and the function (presently marked as "private") may change at anytime.
    One should also note that **nxcals-thin** API is not meant for big-data analysis. For actual offline data analysis, one should rely on other APIs!

The following example function might be helpful for simple query of a single parameter to NXCALS.
The returned format, once again, is very similar to the standard format provided by other functions of pyjapcscout, however data types are not strictly preserved.


.. doctest::

    >>> from pyjapcscout import PyJapcScout
    >>> import datascout
    >>>
    >>> myPyJapc = PyJapcScout(incaAcceleratorName='ELENA', defaultSelector='LNA.USER.MD4')
    >>> myPyJapc.rbacLogin()
    >>>
    >>> refTime_ns = datascout.string_to_unixtime('2021-11-10 17:00')
    >>> selector = 'LNA.USER.MD4'
    >>> parameterName = 'LNA.TPU_INTSY-SD/Samples#samples'
    >>> output = myPyJapc._getSimpleNXCALS(parameterName, selectorOverride=selector, referenceTime_ns=refTime_ns, maxAge_s = 100)
    >>> print(output)
    [[ 5734.39990234  9066.8828125   6411.25390625 ... 72535.0625
      64877.328125   82702.6875    ]]


Fine tuning functions
^^^^^

Additional `static` functions to tune the behavior PyJapcScout and to ease user experience:

- ``PyJapcScout.enableCCSDescriptors(enable=True)``: Adds the possibility to get parameter descriptors from CCDE directly, i.e. without LSA/InCA. 

    - **Note 1:** that this is enabled by default if no InCA server is specified, and it allows to properly SET parameters which would otherwise not be settable with plane PyJapc.
    - **Note 2:** this feature can only be enabled at start up of your Python session, i.e. basically before starting any JAPC-related activity in your Python session

- ``PyJapcScout.enableWildcardSelectors(enable=True)``: See `JAPC wikis <https://wikis.cern.ch/display/JAPC/Wildcard+Selectors>`_
- ``PyJapcScout.setLoggingLevel(logLevel=None)``: change logging level of java/python
- ``PyJapcScout.simpleDataSanityCheck(data)``: a simple example function that checks that no data was received with an exception (useful for monitoring)


PyJapcExt: an "extended" PyJapc
-----

Inside PyJapcScout it is also defined another PyJapc-like class: **PyJapcExt**. This is a sub-class of PyJapc with the only purpose to add the functionality of "fast group subscription" which was developed for the MATLAB-JAPC interface and with the possibility to obtain several acquisition per cycle (i.e. with same cyclestamp) as well as to solve some data synchronization issues when acquiring parameters coming from very different frontends. A simple example here:

.. testcode::

    from pyjapcscout import PyJapcExt

    myjapc = PyJapcExt(selector='LNA.USER.ZERO')
    oasisSignal = 'LNR.SCOPE01.CH01/Acquisition'
    selector    = 'LNA.USER.ALL'

    # callback function
    def callbackFunction(parameter, data):
        print('data received')

    allowManyUpdatesPerCycle = True
    strategyTimeout=1200
    handler = myjapc.subscribeParamExt([oasisSignal], onValueReceived=callbackFunction, timingSelectorOverride=selector, allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout)

    # Start/stop/clear subscription
    handler.startMonitoring()
    [...]
    handler.stopMonitoring()
    myjapc.clearSubscriptions()

.. warning::

    ``PyJapcExt`` is not intended to be used by the final USER. It was mainly a development step of the PyJapcScout project.


