###  stop python (otherwise python terminal gets stack and waits for Java to finish I don't know what...)
import jpype as jp
System = jp.JClass("java.lang.System")
System.exit(0)

### need to install 
python -m pip install pyarrow

### working example, but not working with parquet
from pyjapcscout import PyJapcScout
from pyjapcscout import PyJapcScoutData

myPyJapc = PyJapcScout(incaAcceleratorName='CTF', defaultSelector='SCT.USER.SETUP', noSet=False, timeZone="utc", unixtime=False, logLevel=None)


parameters = ['CA.BHB0400/SettingPPM#current', 'CA.BHB0900/SettingPPM#current', 'CA.BTV0125/Image', 'CA.BTV0125/Acquisition']
values = myPyJapc.getValues(parameters,selectorOverride='')
print(values)
myPyJapc.setValues(parameters, values, selectorOverride='')

def mycallback(newdata, *args, **kwargs):
    print('new data recieved by user')
    #print(newdata)

parameters = ['CA.BHB0400/SettingPPM#current', 'CA.BHB0900/SettingPPM#current', 'CA.BHB0400/Acquisition#currentAverage', 'CA.BHB0900/Acquisition']
myMonitor = myPyJapc.createMonitor(parameters, onValueReceived=mycallback, selectorOverride = None,
                        groupStrategy = 'standard',
                        allowManyUpdatesPerCycle=False, strategyTimeout=1200, forceGetOnChangeAndConstantValues=False)

myMonitor.startMonitor()
myMonitor.stopMonitor()

myMonitor.saveDataPath = './data/'
myMonitor.saveData = True
myMonitor.saveData = False
myMonitor.saveDataFormat = 'pickledict' # or 'parquet' or 'pickle' or 'mat' or 'pickledict'
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'mat' or 'pickledict'


myData = PyJapcScoutData()
myData.load('./data/', fileFormat='parquet')



myData = PyJapcScoutData(values)
myData = PyJapcScoutData(myMonitor.lastData)
myData.df['CA.BHB0400/SettingPPM#current'][0]['value']


fileformat = 'parquet'
fileformat = 'pickledict'
fileformat = 'pickle'

myData.save(folderPath = './data/', filename = 'cip4', fileFormat=fileformat)
myDataLoad = PyJapcScoutData()
myDataLoad.load('./data/', fileFormat=fileformat)
myDataLoad.load('./data/cip.pkl', fileFormat=fileformat)
myDataLoad2= PyJapcScoutData()
myDataLoad2.load('./data/cip2.pkl', fileFormat=fileformat)
myDataLoad2.load('./data/cip3.pkl', fileFormat=fileformat)

myDataLoad.df['CA.BHB0400/SettingPPM#current'][0]['value']

myDataLoad3.load('./data/cip4.parquet', fileFormat=fileformat)

with open('./data/cip2.pkl', 'rb') as handle:
    ccc = pickle.load(handle)

### Just some personal notes I used during development... 

import pyjapcscout
cipolla = pyjapcscout.PyJapcScout(incaAcceleratorName='CTF', defaultSelector='SCT.USER.SETUP')

def mycallback(*args, **kwargs):
    print('new data')
    print(args[0])

myMonitor = cipolla.createMonitor(['CA.BHB0400/Acquisition', 'CA.BHB0400/SettingPPM'], onValueReceived=mycallback, selectorOverride = None,
                        groupStrategy = 'standard',
                        allowManyUpdatesPerCycle=False, strategyTimeout=1200, forceGetOnChangeAndConstantValues=False)

myMonitor.startMonitor()
myMonitor.stopMonitor()



myMonitor2 = cipolla.createMonitor(['CA.BHB0400/Acquisition', 'CA.BHB0400/SettingPPM'], onValueReceived=mycallback, selectorOverride = None,
                        groupStrategy = 'extended',
                        allowManyUpdatesPerCycle=False, strategyTimeout=1200, forceGetOnChangeAndConstantValues=False)

myMonitor2.startMonitor()
myMonitor2.stopMonitor()


myMonitor3 = cipolla.createMonitor(['CA.BHB0400/Acquisition', 'CA.BHB0400/SettingPPM'], onValueReceived=mycallback, selectorOverride = None,
                        groupStrategy = 'multiple',
                        allowManyUpdatesPerCycle=False, strategyTimeout=1200, forceGetOnChangeAndConstantValues=False)

myMonitor3.startMonitor()
myMonitor3.stopMonitor()


outputData = pyjapcscout.PyJapcScoutData()
miao = myMonitor.lastData
for failsSafeValue in miao:
   outputData[failsSafeValue.getParameterName()] = myMonitor._dataConverter.JAcquiredValueToPy(failsSafeValue)



### Some testing

source /acc/local/share/python/acc-py/pro/setup.sh 
import pyjapc

myPyJapc = pyjapc.PyJapc('SCT.USER.SETUP')
value = myPyJapc.getParam('CA.BHB0400/Acquisition', noPyConversion = True)
value.getParameterName()

desc = value.getDescriptor()
desc.getType().toString()
desc.getType().hashCode()
# descriptor of field:
value.getDescriptor().get('currentAverage')
# value of field:
value.getValue().get('currentAverage')

# header
value.getHeader().toString()

# exception
value.getException() == None


value = myPyJapc.getParam('CA.BHB0400/Acquisition#currentAverage', noPyConversion = True)
desc = value.getDescriptor()
desc.getValueType().toString()
desc.getValueType().hashCode()
value.getValue()
a=value.getValue().getFloat()
type(a)
<class 'float'>
a=value.getValue().getDouble()
type(a)
<class 'float'>




# this works
import jpype as jp
factory = jp.JPackage("cern").japc.core.factory.SimpleParameterValueFactory
factory.newSimpleParameterValue(4)


miao = myPyJapc._getJapcPar('CA.BHB0400/Acquisition#currentAverage')
miao.valueDescriptor.toString()

miao = myPyJapc._getJapcPar('CA.BHB0400/Acquisition')
miao.valueDescriptor.toString()
miao.valueDescriptor.getNames() ->  all names of fields
miao.valueDescriptor.names -> also all names of fields
miao.valueDescriptor.get('current').getValueType().toString()

miao.valueDescriptor.namesAndValues['current'].getValueType().toString()
> 'float[]'
