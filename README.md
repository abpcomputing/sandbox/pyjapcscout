# PyJapcScout

PyJapcScout is a test bed to possibly propose improvement of PyJapc v2.

**NOTE1:** If you want to quickly try this package, go below to the "Getting started" section.

**NOTE2:** THIS IS WORK IN PROGRESS! The behavior of this package can change without notice.

davide.gamba@cern.ch - Mar 2021

## Purpose of this project

The purpose of PyJapcScout is not to replace [PyJapc](https://gitlab.cern.ch/scripting-tools/PyJapc), but to explore the possibility of a more convenient interface for MD studies.
We could then ask if features that are found useful could be part of the new version of PyJapc - see [wikis v3 discussion](https://wikis.cern.ch/display/ACCPY/PyJapc+v3+analysis+and+planning#PyJapcv3analysisandplanning-).

At the moment, PyJapcScout is basically a dressing of PyJapc with the following methods/examples:

### constructor:
```
myPyJapc = PyJapcScout(incaAcceleratorName='CTF', defaultSelector='SCT.USER.SETUP', noSet=False, timeZone="utc", unixtime=True, logLevel=None)
```

### GET/SET a simple parameter (e.g. a simple number)
```
value = myPyJapc.getSimpleValue('CA.BHB0900/SettingPPM#current',selectorOverride='')
print(value)
myPyJapc.setSimpleValue('CA.BHB0900/SettingPPM#current', value, selectorOverride='')
```

### GET/SET a list of parameters
```
parameters = ['CA.BHB0400/SettingPPM#current', 'CA.BHB0900/SettingPPM#current']
values = myPyJapc.getValues(parameters,selectorOverride='')
print(values)
myPyJapc.setValues(parameters, values, selectorOverride='')
```

### MONITOR a list of parameters
```
def mycallback(newdata, monitorHandler, *args, **kwargs):
    print('new data recieved by user')
    print(newdata)

parameters = ['CA.BHB0400/SettingPPM#current', 'CA.BHB0900/SettingPPM#current', 'CA.BHB0400/Acquisition#currentAverage', 'CA.BHB0900/Acquisition']
myMonitor = myPyJapc.createMonitor(parameters, onValueReceived=mycallback, selectorOverride = None,
                        groupStrategy = 'standard',
                        allowManyUpdatesPerCycle=False, strategyTimeout=-1, forceGetOnChangeAndConstantValues=False,
                        dataSanityCheck = PyJapcScout.simpleDataSanityCheck)

myMonitor.startMonitor()
myMonitor.stopMonitor()
```

This **monitoring** object is where we have some new features with respect to plane PyJapc:

1. Possibility to save data as it comes:
    ```
    myMonitor.saveDataPath = './data/'
    myMonitor.saveDataFormat = 'parquet' # or 'pickle'  ( maybe in the future 'mat' or other data formats)
    myMonitor.saveData = True
    ```
2. Possibility of using more "complex" parameter grouping strategies:
    - 'standard': is a simple PyJapc group subscription, using the underlaying PushGroupSubscriptionStrategy provided by JAPC
    - 'extended': is the "fast" strategy developed for CTF3/CLEAR which allows, for example to have several updated from a device from the same cycle (i.e. with equal cycleStamps)
    - 'multiple': each parameter is subscribed as a single subscription in a standard PyJapc subscription. 
        Note that the same user callback will be called in this case with the data of a single parameter.
3. The monitoring object has inside last data received:
    ```
    myMonitor.lastData # a dictionary with all data.
    ```
4. The possibility to define a user-defined data sanity check function which allows to condition the saving of the data and the user-defined callback function execution.

### Data format

One of the reasons to prepare this interface is to allow to explore other data conversion from the JAPC/JAVA objects returned by JAPC into python-friendly data.
This mainly with the aim of ease the process of saving data, but also in using the data in scripts.
The general philosophy adopted is:
- Don't use tuples (they are not well digested by some save data formats)
- whatever you GET you can also SET it back
- numerical objects are all converted to Numpy objects, preserving as much as possible the precision of the data type (e.g. a JAVA "short" goes into a "numpy.int16")
- headers, exceptions, etc are as much as possible preserved

The data coming/set from/to PyJapcScout is expected to be typically a dictionary. Inside there will only be `numpy values`, `numpy arrays` organised in nested `dictionaries`.

**NOTE:** The data format might change in the future as the purpose of this project is indeed to explore more convenient data formats in particular to save data and to ease the post-processing.

#### Data handling, including saving/loading

**Warning:** The class PyJapcScoutData, used in the very first version of PyJapcScout, has been removed! 

Data handling is presently done using sweet functions provided in a separate packaged called [datascout](https://gitlab.cern.ch/abpcomputing/sandbox/datascout.git).
The idea is that data coming from PyJapcScout is "simple enough" that it can be easily saved to `parquet` (and `pickle`) using `datascout` methods, as well as to convert acquired data to well known data handling packages like:
- [pandas](https://pandas.pydata.org/): `datascout.dict_to_pandas(<My dict acquired with PyJapcScout>)`
- [awkward](https://awkward-array.org/): `datascout.dict_to_awkward(<My dict acquired with PyJapcScout>)`

### PyJapcExt: an "extended" PyJapc

Inside PyJapcScout it is also defined another PyJapc-like class: **PyJapcExt**. This is a sub-class of PyJapc with the only purpose to add the functionality of "fast group subscription" which was developed for the MATLAB-JAPC interface and with the possibility to obtain several acquisition per cycle (i.e. with same cyclestamp) as well as to solve some data synchronization issues when acquiring parameters coming from very different frontends. A simple example here:
```
from pyjapcscout import PyJapcExt

myjapc = pyjapc.PyJapcExt(selector='LNA.USER.ZERO')
oasisSignal = 'LNR.SCOPE01.CH01/Acquisition'
selector    = 'LNA.USER.ALL'

# callback function
def callbackFunction(parameter, data):
    print('data received')

allowManyUpdatesPerCycle = True
strategyTimeout=1200
handler = myjapc.subscribeParamExt([oasisSignal], onValueReceived=callbackFunction, timingSelectorOverride=selector, allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout)

# Start/stop/clear subscription
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()
```

### Some additional static functions

Some small static functions have been defined to ease user experience.

- `PyJapcScout.enableCCSDescriptors(enable=True)`: Adds the possibility to get parameter descriptors from CCDE directly, i.e. without LSA/InCA.
- `PyJapcScout.enableWildcardSelectors(enable=True)`: See https://wikis.cern.ch/display/JAPC/Wildcard+Selectors
- `PyJapcScout.setLoggingLevel(logLevel=None)`: change logging level of java/python
- `PyJapcScout.simpleDataSanityCheck(data)`: a simple example function that checks that no data was received with an exception (useful for monitoring)


## Getting started

To start using it, assuming you are with a terminal on the technical network:
```
# Activate acc-py Python distribution:
source /acc/local/share/python/acc-py/base/pro/setup.sh

# create your own virtual environment in the folder "venv":
acc-py venv

# activate your new environment
source ./venv/bin/activate

# Install in your new environment PyJapcScout, two options
# 1. Direct installation (**NOT RECOMMENDED** (nor working) for the time being, but will be the official way in the future):
python -m pip install git+https://gitlab.cern.ch/abpcomputing/sandbox/pyjapcscout.git
# 2. Clone the projects (datascout and pyjapcscout) and install from folder in "editing" mode:
git clone https://gitlab.cern.ch/abpcomputing/sandbox/datascout.git datascout
cd datascout
python -m pip install -e .
git clone https://gitlab.cern.ch/abpcomputing/sandbox/pyjapcscout.git pyjapcscout
cd pyjapcscout
python -m pip install -e .

# In order to use parquet data format, you might also need to install pyarrow (it should not be needed anymore)
python -m pip install pyarrow

# Sometimes it is useful to have ipython to debug your scripts
python -m pip install ipython
```

At this point one can start using PyJapcScout in her/his python (started within the virtual environment created above):
```
from pyjapcscout import PyJapcScout
```

Eventually, PyJapcScout could be made available on acc-py repository (but probably not installed default acc-py distribution), so one will be able substitute last line of the installation with something like:
```
python -m pip install pyjapcscout
```
:::Warning
Sometimes you need to re-download the jars if you were to install pyjapcscout on an old environment. In this case, within your environment, you can try to force to updated the jars:
```
python -m cmmnbuild_dep_manager resolve
```
:::

### To uninstall PyJapcScout (and datascout)
```
python -m pip uninstall pyjapcscout
python -m pip uninstall datascout
```

## For the development of PyJapcScout

I mainly followed the instructions you can find on the [acc-py website](https://acc-py.web.cern.ch/gitlab/acc-co/devops/python/acc-py-devtools/docs/stable/init.html) 
and/or its [wikis](https://wikis.cern.ch/display/ACCPY/Development+HowTo%27s).

If you want to edit PyJapcScout and test it on your local environment, one can probably slightly change the last line of the installation procedure above with something like:
```
git clone https://gitlab.cern.ch/abpcomputing/sandbox/pyjapcscout.git pyjapcscout
cd pyjapcscout
python -m pip install -e .
```

For releasing a new version of pyjapcscout, see acc-py documentation on [wikis](https://wikis.cern.ch/display/ACCPY/Development+HowTo%27s).
Assuming you are on a TN machine with acc-py available:
```
acc-py check-setup
acc-py check-mypy
acc-py check-flake8
acc-py build
acc-py devrelease
```
 

#### Development TODO list

- Complete conversion function for SET of any kind of data type (e.g. enum, function list, ...)
- See if we could make the data structure somewhat compatible with what previously saved in MATLAB (e.g. mat files loading/saving)
- deal with no-selector exceptions... 
    ```
    parameterSelectorAdapter = cern.japc.core.Selectors.adapt(obj.parameterSelector)
    auxSelector = parameterSelectorAdapter.to(auxParameterHandle)
    ```
- Improve testings
- Debug, debug, debug, ....


#### Note on first development

I made the first development from my laptop (probably a bad idea?!)
```
# use acc-co pip repository:
pip install git+https://gitlab.cern.ch/acc-co/devops/python/acc-py-pip-config

# created (or checkout) the PyJapcScout git project
git clone https://gitlab.cern.ch/abpcomputing/sandbox/pyjapcscout.git pyjapcscout
# Note: to initialise the empty PyJapcScout project I did:
#acc-py init
#acc-py init-ci

# within the main PyJapcScout folder, created a virtual environment, and activated it
#### (Eventually: source /acc/local/share/python/acc-py/base/pro/setup.sh)
python -m venv ./venv --system-site-packages
source ./venv/bin/activate

# "installed" PyJapcScout in "editable" mode (-e) the package:
python -m pip install -e .
```


#### closing python
It looks like there is sum bug/feature that prevents to close python once you start using Jpype & C... See [jpype-issue817](https://github.com/jpype-project/jpype/issues/817).
To kill your python session, you can execute the following ugly lines:
```
import jpype as jp
System = jp.JClass("java.lang.System")
System.exit(0)
```
Note that this kills everything you are doing pretty badly...
