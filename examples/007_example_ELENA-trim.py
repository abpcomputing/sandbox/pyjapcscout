'''
Look at trim history. and send a trim to LSA (even to a non-resident context...
'''

from pyjapcscout import PyJapcScout
import datascout
import matplotlib.pyplot as plt
import copy

myPyJapc = PyJapcScout(incaAcceleratorName='ELENA', defaultSelector='LNA.USER.MD5')

# check which cycle is mapped to which user or viceversa...
user_to_look_at = 'LNA.USER.MD5'
cycle_name = myPyJapc.getCycleNameFromUser(user_to_look_at)
user_mapped = myPyJapc.getUserFromCycleName(cycle_name)
print(f'{user_to_look_at} is used by {cycle_name} which is indeed mapped on {user_mapped}')


##### Look for parameters in LSA
lsaparams = myPyJapc.findLSAParametersByName('ELENABEAM/Q%')
print(lsaparams)

##### For any JAPC-reachacble parameter we can typically get a description
#     note that those methods return a JAVA object to you...
# get the **JAPC** **parameter** descriptor of one of such a parameter
myPyJapc.getJavaParameterDescriptor(lsaparams[0], forceCCSDescriptorSource = False, verbose=True)
# get the **JAPC** **value** descriptor of one of such a parameter
myPyJapc.getJavaValueDescriptor(lsaparams[0], forceCCSDescriptorSource = False, verbose=True)


##### GET and Perform TRIMs on LSA parameters

#parameterName = 'AIX.SRC-BEAM/Delay#delay'
parameterName = 'ELENABEAM/QH#value'
cycleOverride = 'Short_isabella_2021'
part="value"
part="target"
part="correction"

referenceTime_ns = datascout.string_to_unixtime('2021-09-14 16:09')
old_value = myPyJapc.getSimpleTrim(parameterName, selectorOverride = None, cycleOverride = cycleOverride, referenceTime_ns = referenceTime_ns, part=part, completeOutput=True)
old_time = datascout.unixtime_to_string(old_value['header']['creationStamp'], time_zone = 'cern')
old_value = old_value['value']
present_value = myPyJapc.getSimpleTrim(parameterName, selectorOverride = None, cycleOverride = cycleOverride, part=part, completeOutput=True)
present_time = datascout.unixtime_to_string(present_value['header']['creationStamp'], time_zone = 'cern')
present_value = present_value['value']

# plot those two trims...
fig, ax = plt.subplots(1)
ax.plot(    old_value['JAPC_FUNCTION']['X'],     old_value['JAPC_FUNCTION']['Y'], label=old_time)
ax.plot(present_value['JAPC_FUNCTION']['X'], present_value['JAPC_FUNCTION']['Y'], label=present_time)

# now edit something
description = 'My test with pyjapcscout'
new_value = copy.deepcopy(present_value)
new_value['JAPC_FUNCTION']['Y'][1] = new_value['JAPC_FUNCTION']['Y'][1]+0.05
# might need RBAC for doing so:
myPyJapc.rbacLogin()
myPyJapc.setSimpleTrim(parameterName, new_value, description=description, selectorOverride=None, cycleOverride=cycleOverride, part=part)
myPyJapc.setSimpleTrim(parameterName, present_value, description='setting back via pyjapcscout', selectorOverride=None, cycleOverride=cycleOverride, part=part)


# can also work on a list of parameters
parameterNames = ['ELENABEAM/QH#value', 'ELENABEAM/QV#value']
present_values = myPyJapc.getTrims(parameterNames, selectorOverride = None, cycleOverride = cycleOverride, part=part)
for value in present_values:
    print(f'{value} was trimmed on {datascout.unixtime_to_string(present_values[value]["header"]["creationStamp"], time_zone = "cern")}')
myPyJapc.setTrims(parameterNames, present_values, description='setting back via pyjapcscout', selectorOverride=None, cycleOverride=cycleOverride, part=part)

new_values = myPyJapc.getTrims(parameterNames, selectorOverride = None, cycleOverride = cycleOverride, part=part)
for value in new_values:
    print(f'{value} was trimmed on {datascout.unixtime_to_string(new_values[value]["header"]["creationStamp"], time_zone = "cern")}')
present_value = present_value['value']