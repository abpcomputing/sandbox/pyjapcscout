'''
Treating beam intensity in CLEAR

- I reads an Oasis scope
- It uses calibration settings form a different device to compute actual beam charge

Example both in plan pyjapc and pyjapcscout
'''

import numpy as np

# set to False if you want ot use plane pyjapc
use_pyjapcscout = True

# define function to treat data
def treat_BCT_signal(bctData, gainSetting):
    avg_start = 20
    avg_end = 60
    turnsFactor  = 10.0 # beam charge / Calibration charge

    calibrations = {"6dB": 2.085, \
                    "12dB": 4.18, \
                    "18dB": 8.35, \
                    "20dB": 10.42, \
                    "26dB1": 20.97, \
                    "26dB2": 20.95, \
                    "32dB": 41.9, \
                    "40dB": 105.0}  # [V/nC]

    assert bctData['value_units'] == 'V'
    #Get the signals in Volts
    signal = np.asarray(bctData['value'],'float')*bctData['sensitivity'] + bctData['offset']

    # compute charge
    chargeRaw   = np.mean(signal.flatten()[avg_start:avg_end])
    charge      = turnsFactor*(chargeRaw / calibrations[gainSetting])

    return charge

# simple acquire data
if use_pyjapcscout:
    from pyjapcscout import PyJapcScout
    myPyJapc = PyJapcScout(incaAcceleratorName='CTF')
    # make sure scope settings are correct
    # myPyJapc.getSimpleValue('CE.SCOPE61.CH01/Sensibility#value', selectorOverride='')
    myPyJapc.setSimpleValue('CE.SCOPE61.CH01/Sensibility#value', 20, selectorOverride='')
    bctData = myPyJapc.getSimpleValue('CE.SCOPE61.CH01/Acquisition', selectorOverride='SCT.USER.SETUP')
    gainSetting = myPyJapc.getSimpleValue('CA.BCM01GAIN/Setting#enumValue', selectorOverride='')
else:
    from pyjapc import PyJapc
    myPyJapc = PyJapc(selector='SCT.USER.SETUP')
    # make sure scope settings are correct
    # myPyJapc.getParam('CE.SCOPE61.CH01/Sensibility#value', timingSelectorOverride='')
    myPyJapc.setParam('CE.SCOPE61.CH01/Sensibility#value', 20, timingSelectorOverride='')
    bctData = myPyJapc.getParam('CE.SCOPE61.CH01/Acquisition', timingSelectorOverride='SCT.USER.SETUP')
    gainSetting = myPyJapc.getParam('CA.BCM01GAIN/Setting#enumValue', timingSelectorOverride='')

# do data treatment and print output
print(f'Gun charge is {treat_BCT_signal(bctData, gainSetting):.3f} nC')
