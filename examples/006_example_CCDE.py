'''
Using CCDE interaction to get device lists, and properties...
'''

from pyjapcscout import PyJapcScout
myPyJapc = PyJapcScout()

# look for devices via CCDE matching a given pattern (all keyword arguments are optional)
pattern = 'AIX.SRC*'
myPyJapc.findDevicesByName(pattern, operationalOnly=True, simplifiedOutput=True, includeAlias=True, verbose=True)

# find all classes via CCDE matching a given pattern (all keyword arguments are optional)
pattern = 'LTIM*'
myPyJapc.findDeviceClassByName(pattern, operationalOnly=True, simplifiedOutput=True, verbose=True)

# get all properties of a given class (all keyword arguments are optional)
#  Note 1: one can/should specify a version, otherwise the last (operational) version is provided
name = 'LTIM'
myPyJapc.findDeviceClassProperties(name, version=None, operationalOnly=True, simplifiedOutput=True, verbose=True)
#  Note 2: sometimes there are classes which are marked as "development" by are used by "operational" devices
#          In this case, the function gives you a warning that no operational version of the class is found, but still
#          gives you the properties of the last available version it finds...
name = 'FGC_93'
myPyJapc.findDeviceClassProperties(name, version=None, operationalOnly=True, simplifiedOutput=True, verbose=True)
