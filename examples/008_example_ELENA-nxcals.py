'''
Look at NXCALS for single data...
'''

from pyjapcscout import PyJapcScout
import datascout
import matplotlib.pyplot as plt
import numpy as np
# just to make the plot nicer:
import matplotlib 
font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 22}
matplotlib.rc('font', **font)


myPyJapc = PyJapcScout(incaAcceleratorName='ELENA', defaultSelector='LNA.USER.MD4')
myPyJapc.rbacLogin()

# simple get latest value
refTime_ns = datascout.string_to_unixtime('2021-11-10 17:00')
selector = 'LNA.USER.MD4'
parameterName = 'LNA.TPU_INTSY-SD/Samples'
parameterName = 'LNA.TPU_INTSY-SD/Samples#samples'
parameterName = 'LNA.TPU_INTSY-SD/Samples'
parameterName = 'LNE.APULB.0030/Acquisition#totalIntensityPreferred'
output = myPyJapc.getSimpleNXCALS(parameterName, selectorOverride=selector, referenceTime_ns=refTime_ns, maxAge_s = 100)
print(output)

# get a "history" of a simple parameter
output = myPyJapc.getSimpleNXCALS(parameterName, selectorOverride=selector, referenceTime_ns=refTime_ns, maxAge_s = 600, returnAllRecords=True)
print(output)

# get of several parameters, as for a simple acquisition
parameterNames = ['LNA.TPU_INTSY-SD/Samples', 'LNE.APULB.0030/Acquisition#totalIntensityPreferred']
output = myPyJapc.getNXCALS(parameterNames, selectorOverride=selector, referenceTime_ns=refTime_ns, maxAge_s = 100)
print(output)

# or all records
parameterNames = ['LNA.TPU_INTSY-SD/Samples', 'LNE.APULB.0030/Acquisition#totalIntensityPreferred']
output = myPyJapc.getNXCALS(parameterNames, selectorOverride=selector, referenceTime_ns=refTime_ns, maxAge_s = 100, returnAllRecords=True)
print(output)

# Get some data more recent from LINAC4....
selector = 'PSB.USER.ALL'
parameterName = 'L4C.BPM.0607/Logging'
output = myPyJapc.getSimpleNXCALS(parameterName, selectorOverride=selector, maxAge_s = 60)
print(output)