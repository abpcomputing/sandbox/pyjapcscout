'''
Example script to see all LTIM of a given FEC, and print where they come out

Unfortunately, presently not everything is possible at the moment with plane pyccda
In this example we also use the CCDA REST API to get some data

For some additional data, we use the REST server (not really an API) of CCDE. In this case, ACW authentication is also needed...
   -> this is not very nice!!!! Use at your own risk!
'''
# pyccda doc:
#  https://acc-py.web.cern.ch/gitlab/controls-configuration-service/controls-configuration-data-api/accsoft-ccs-ccda/docs/stable/index.html
# CCDA REST API doc:
#  https://ccda.cern.ch:8900/api/swagger-ui.html
# CCDE REST API doc:
#  to be looked at...

import requests
import pyccda
import getpass

myPyCCDA = pyccda.SyncAPI()

# The FEC you want to look at:
timing_FEC = 'cfv-2010-cctftim2'

# prepare header for request to API
headers = {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache--url',
}
# GET all LTIM devices for the given FEC
allDevices = myPyCCDA.Device.search(query=f'serverName=="LTIM_DU.{timing_FEC}"')
allDevicesInfo = dict()
for device in allDevices:
    try:
        fesaDevice = myPyCCDA.FesaDevice.from_device(device)
        fesaDeviceFields = {field.field_name: field.field_value for field in fesaDevice.field_values}

        # pyccda is not providing us with hwFieldValues info for the time being...
        # ... we should be looking for hwFieldValues ...
        #  -> use REST API to go directly online
        response = requests.get(f"https://ccda.cern.ch:8900/api/fesa/device-field-values/{device.name}", headers=headers,  verify=False)
        data = response.json()
        name = device.name
        lun = data['hwFieldValues'][0]['lun']
        ch = data['hwFieldValues'][0]['channel']
        if lun not in allDevicesInfo:
            allDevicesInfo[lun] = dict()
        allDevicesInfo[lun][ch] = fesaDeviceFields
        allDevicesInfo[lun][ch]['LTIMname'] = name
    except:
        print(f"did not manage to get data for {device.name}")

# print data in some sorted way
for lun in sorted(allDevicesInfo.keys()):
   for ch in sorted(allDevicesInfo[lun].keys()):
       print(f"lun:{lun} ch:{ch} -> {allDevicesInfo[lun][ch]['LTIMname']} "
             f"\t start:{allDevicesInfo[lun][ch]['start']} "
             f"clock:{allDevicesInfo[lun][ch]['clock']} "
             f"load:{allDevicesInfo[lun][ch]['loadEvent']} "
             f"TGM:{allDevicesInfo[lun][ch]['conditionTgmGroup']} V:{allDevicesInfo[lun][ch]['conditionTgmValue']} "
             f"MODE:{allDevicesInfo[lun][ch]['mode']}")




# -------------------------------------------------------------------------------------------------------------------
#      USING CCDE INTERFACE
# -------------------------------------------------------------------------------------------------------------------
# to get the configure "signals is a bit more difficult"

# find the FEC
fec = myPyCCDA.Computer.find(name=timing_FEC)
# one can at least see which modules are installed in this FEC, but not much more via CCDA interface...
for module in fec.primary_crate.modules:
    print(f'{module.name} in lun {module.lun}')

# need to look into CCDE API (not stable!) to get some info
# get at least the FEC ID:
fec_id = fec.id

# first, get a ACW tocken
username = 'dgamba' #<- need to write your CERN username
resp = requests.post('https://ccda.cern.ch:8900/api/login', verify=False, json={"login": username, "password": getpass.getpass() })
acw_token = resp.json()['acwToken']
# put it in cookies
cookies = {'acw_token': acw_token}

# now can query the ccde server
headers = {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache--url',
}
response = requests.get(f"https://ccde.cern.ch/api/hardware/computer/configuration/physical/crates/search/computerId/{fec_id}", headers=headers, cookies=cookies, verify=False)
fec_data = response.json()

allModuleSignals = dict()
for module in fec_data['content'][0]['modules']:
    allModuleSignals[module['layoutName']] = dict()
    try:
        for signal in module['signals']:
            allModuleSignals[module['layoutName']][signal['connector']['name']] = signal['signal']
    except:
       print(f"did not manage to get data for {module['layoutName']}")

# print data in some sorted way
for module in sorted(allModuleSignals.keys()):
   for connection in sorted(allModuleSignals[module].keys()):
       print(f"mod:{module} -> {connection} -> {allModuleSignals[module][connection]}")


