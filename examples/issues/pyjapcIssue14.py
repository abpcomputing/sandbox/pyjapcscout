######################################################
# Tests on Kyrre's issue https://gitlab.cern.ch/scripting-tools/pyjapc/-/issues/14
# It issue can be solved using PyJapcExt provided within pyjapcscout package
#
# Davide - March 2021
######################################################
# Code adapted from: /clear/data/Users/kyrsjo/java-japc-test/Test.py

import pyjapc
import pyjapcscout
import datetime
import numpy as np

tStart = datetime.datetime.now()
tPrev = tStart
dataPrev = [None,None,None]

def callbackFunction(dataName, data):
    global tPrev
    global dataPrev
    assert len(data) == 3
    
    print("****************")
    print("Got stuff!")
    now = datetime.datetime.now()
    print(str(now) + \
          " dt = "      + str(now-tPrev) + \
          " dtStart = " + str(now-tStart)   )
    for i in range(3):
        if data[i] == None:
            print("No data for " + dataName[i])
        else:
            print(data[i]['firstSampleTime'])
        if dataPrev[i] == None or not np.array_equal(data[i], dataPrev[i]):
            dataPrev[i] = data[i]
            print("Replaced data for " + dataName[i])
        else:
            print("Data for " + dataName[i] + " were the same")
    print("****************")
    print("")
    tPrev = now


gunBCM_name = "CA.BPC0220D-SA/Samples"
vesperBCM_name = "CA.BPC0240D-SA/Samples"
laserEnergy_name = "CA.BPC0720D-SA/Samples" # This is another signal on the same FEC - it works
laserEnergy_name = "CE.SCOPE60.CH05/Acquisition" # This is a signal from another FEC - it doesn't work with standard group

selector = "SCT.USER.SETUP"
signalsToMonitor = [gunBCM_name, vesperBCM_name, laserEnergy_name]

# The standard subscription:
myjapc = pyjapc.PyJapc(selector='SCT.USER.ALL')
handler = myjapc.subscribeParam(   signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()

# The "Extended" subscription works most of the time:
myjapc = pyjapcscout.PyJapcExt(selector='SCT.USER.ALL')
allowManyUpdatesPerCycle = False
strategyTimeout=1300 # Very important here (well... it could probably be set to 0 if not mistaken)
handler = myjapc.subscribeParamExt(signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector, allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()