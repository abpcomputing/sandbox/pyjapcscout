import pyjapc


######################################################
# Single parameter acquisition multiple time per BP
######################################################
myjapc = pyjapc.PyJapcExt(selector='LNA.USER.ZERO')

oasisSignal = 'LNR.SCOPE01.CH01/Acquisition'
selector    = 'LNA.USER.ALL'

# callback function
def callbackFunction(parameter, data):
    print('ciao')

# The standard subscription:
handler = myjapc.subscribeParam(   [oasisSignal], onValueReceived=callbackFunction, timingSelectorOverride=selector)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()

# The "Extended" subscription
allowManyUpdatesPerCycle = True
strategyTimeout=1200
handler = myjapc.subscribeParamExt([oasisSignal], onValueReceived=callbackFunction, timingSelectorOverride=selector, allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()



######################################################
# Several parameters of different types
######################################################
myjapc = pyjapc.PyJapcExt(selector='PSB.USER.ZERO')
myjapc.rbacLogin(username='psbop', loginDialog=True)

selector = 'PSB.USER.MD1'
selector = 'PSB.USER.MD3'

signalsToMonitor = [ 'BR1.BCT-ST/Samples',
                    'BR1.XSK6L4/LOG.OASIS.I_MEAS',
                    'BR1.XSK2L4/LOG.OASIS.I_MEAS'
                    ]

marte = None
# callback function
def callbackFunction(parameter, data):
    global marte
    marte = data
    print('ciao2')
    assert len(data) == 3
    assert data[0] != None
    assert data[1] != None
    assert data[2] != None

# This does not work already if one doesn't have the proper RBAC
myjapc.getParam('BR1.XSK6L4/LOG.OASIS.I_MEAS', timingSelectorOverride=selector)

# The standard subscription:
handler = myjapc.subscribeParam(   signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()

# The "Extended" subscription
allowManyUpdatesPerCycle = False
strategyTimeout=1200
handler = myjapc.subscribeParamExt(signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector, allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()

######################################################
# Kyrre's issue https://gitlab.cern.ch/scripting-tools/pyjapc/-/issues/14
######################################################
# From: /clear/data/Users/kyrsjo/java-japc-test/Test.py
import pyjapc
import datetime
import numpy as np

myjapc = pyjapc.PyJapcExt(selector='SCT.USER.ALL')

tStart = datetime.datetime.now()
tPrev = tStart
dataPrev = [None,None,None]

def callbackFunction(dataName, data):
    global tPrev
    global dataPrev
    assert len(data) == 3
    
    print("****************")
    print("Got stuff!")
    now = datetime.datetime.now()
    print(str(now) + \
          " dt = "      + str(now-tPrev) + \
          " dtStart = " + str(now-tStart)   )
    for i in range(3):
        if data[i] == None:
            print("No data for " + dataName[i])
        else:
            print(data[i]['firstSampleTime'])
        if dataPrev[i] == None or not np.array_equal(data[i], dataPrev[i]):
            dataPrev[i] = data[i]
            print("Replaced data for " + dataName[i])
        else:
            print("Data for " + dataName[i] + " were the same")
    print("****************")
    print("")
    tPrev = now


gunBCM_name = "CA.BPC0220D-SA/Samples"
vesperBCM_name = "CA.BPC0240D-SA/Samples"
laserEnergy_name = "CA.BPC0720D-SA/Samples" # This is another signal on the same FEC - it works
laserEnergy_name = "CE.SCOPE60.CH05/Acquisition" # This is a signal from anoher FEC - it doesn't work with standard group

selector = "SCT.USER.SETUP"
signalsToMonitor = [gunBCM_name, vesperBCM_name, laserEnergy_name]

# The standard subscription:
handler = myjapc.subscribeParam(   signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()

# The "Extended" subscription
allowManyUpdatesPerCycle = False
strategyTimeout=1201 # Very important here (well... it could probably be set to 0 if not mistaken)
handler = myjapc.subscribeParamExt(signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector, allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()






######################################################
# Mixing non-ppm and cycle bound
# This works in any case!
######################################################
import pyjapc
import datetime

myjapc = pyjapc.PyJapcExt(selector='SCT.USER.ALL')

dataLast = None
def callbackFunction(dataName, data):
    global dataLast
    dataLast = data
    print("****************")
    print("Got stuff!")
    now = datetime.datetime.now()
    print(str(now))
    for i in range(3):
        if data[i] == None:
            print("No data for " + dataName[i])
        else:
            print("OK data for " + dataName[i])


samplerSignal = "CA.BPC0220D-SA/Samples"
dipoleAcq = "CA.BHB0400/Acquisition"
dipoleSet = "CA.BHB0400/SettingPPM"

selector = "SCT.USER.SETUP"
signalsToMonitor = [samplerSignal, dipoleAcq, dipoleSet]

# The standard subscription:
handler = myjapc.subscribeParam(   signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()

# The "Extended" subscription
allowManyUpdatesPerCycle = False
strategyTimeout=2400
handler = myjapc.subscribeParamExt(signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector, allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()




######################################################
# Mixing machines
######################################################
import pyjapc
import datetime

myjapc = pyjapc.PyJapcExt(selector='PSB.USER.ZERO')

dataLast = None
def callbackFunction(dataName, data):
    global dataLast
    dataLast = data
    for name in dataName:
        print("data? for " + name)


selector = "PSB.USER.ZERO"
signalsToMonitor = ['PR.BCT/HotspotIntensity']
handler1 = myjapc.subscribeParam(   signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector)

selector = "CPS.USER.ZERO"
signalsToMonitor = ['BR1.BCT-ST/Samples']
handler2 = myjapc.subscribeParam(   signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector)
handler1.startMonitoring()
handler2.startMonitoring()
handler1.stopMonitoring()
handler2.stopMonitoring()





# The "Extended" subscription
allowManyUpdatesPerCycle = False
strategyTimeout=2400
handler = myjapc.subscribeParamExt(signalsToMonitor, onValueReceived=callbackFunction, timingSelectorOverride=selector, allowManyUpdatesPerCycle=allowManyUpdatesPerCycle, strategyTimeout=strategyTimeout)
handler.startMonitoring()
handler.stopMonitoring()
myjapc.clearSubscriptions()





# for debugging
import jpype
import jpype.imports
from java.lang import System
cip = System.getProperty("java.class.path")
cip = cip.split(':')
for c in cip:
    if 'ctf' in c:
        print(c)
