'''
Look at NXCALS for single data...
'''

from pyjapcscout import PyJapcScout
import datascout
import matplotlib.pyplot as plt
import numpy as np
# just to make the plot nicer:
import matplotlib 
font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 22}
matplotlib.rc('font', **font)

# A simple comparison between the latest acquisition and some NXCALS acquistion
myPyJapc = PyJapcScout(incaAcceleratorName='AD', defaultSelector='ADE.USER.ADE')
myPyJapc.rbacLogin()
refTime_ns = datascout.string_to_unixtime('2021-11-10 23:40')
parameterName = 'ADE.BCCCA/Acquisition'
present_value = myPyJapc.getSimpleValue(parameterName)
nxcals_value = myPyJapc.getSimpleNXCALS(parameterName, referenceTime_ns=refTime_ns, maxAge_s = 200)

def my_plot_bccca(data):
    y_data = data['numOfCharges']
    x_data = data['samplingPeriodSec']*np.arange(len(y_data))
    plt.plot(x_data, y_data)

plt.figure(figsize=[8,5])
my_plot_bccca(present_value)
my_plot_bccca(nxcals_value)
# decorate plot
plt.legend(['latest', 'NXCALS'])
plt.ylim([-0.1e7, 4e7])
plt.xlabel('t [s]')
plt.ylabel('pbars [#]')
