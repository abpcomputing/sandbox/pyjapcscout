'''
Simple example setting a couple of values
'''

# import PyJapcScout
from pyjapcscout import PyJapcScout

# some other imports for user use
import glob
import numpy as np


myPyJapc = PyJapcScout(incaAcceleratorName='PSB')

myPyJapc.rbacLogin()
# i use a simplified monitor
signalsToMonitor = [                       
                    'BR1.BCT-ST/Samples',
                    'BR1.ONO4L1/LOG.OASIS.I_MEAS',
                    'BR1.ONO12L1/LOG.OASIS.I_MEAS'
                    ]

mySelector = 'PSB.USER.MD1'

# setup for new settings to be applied in the callback
myPyJapc.setDefaultSelector(mySelector)
initialValues = myPyJapc.getValues(['BR1.ONO4L1/REF.TABLE.FUNC#VALUE','BR1.ONO12L1/REF.TABLE.FUNC#VALUE'])
ONO4L1  = initialValues['BR1.ONO4L1/REF.TABLE.FUNC#VALUE']['value']
ONO12L1 = initialValues['BR1.ONO12L1/REF.TABLE.FUNC#VALUE']['value']
amplitude4L1=np.linspace(0,20,10)
amplitude12L1=np.linspace(0,10,10)
XX,YY = np.meshgrid(amplitude4L1,amplitude12L1)
values4L1=XX.flatten()
values12L1=YY.flatten()
playing=np.zeros(len(values4L1))
playing[::2]=1

def myCallback(data, h):
    global ONO4L1, ONO12L1, values4L1, values12L1, playing
    print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '/2021*'))) )
    indx = len(glob.glob(h.saveDataPath + '/2021*')) 
    ONO4L1['Y'][2:4]=values4L1[indx]
    ONO12L1['Y'][2:4]=values12L1[indx]
    # something like this I guess or even one-by-one this is not really an issue for us
    #  This wan't work:
    #    myPyJapc.setValues(['BR1.ONO4L1/REF.TABLE.FUNC#VALUE','BR1.ONO12L1/REF.TABLE.FUNC#VALUE'], [ONO4L1,ONO12L1])
    #  for those simple SETs of few parameters the "setSimpleValue" function is provided
    myPyJapc.setSimpleValue('BR1.ONO4L1/REF.TABLE.FUNC#VALUE', ONO4L1)
    myPyJapc.setSimpleValue('BR1.ONO12L1/REF.TABLE.FUNC#VALUE', ONO12L1)
    # in case we want to start with something more simple
    #if playing[indx]==1:
    #    myPyJapc.setValues('BR1.ONO4L1/REF.FUNC.PLAY#VALUE','ENABLED')
    #else:
    #    myPyJapc.setValues('BR1.ONO4L1/REF.FUNC.PLAY#VALUE','DISABLED')
    if indx == len(values12L1):
        h.stopMonitor()
        print('Measurement finished and monitor stopped.')
        print('Setting back initial values...')
        # to set many parameters, setValues expect a dictionary, similar (or the same as) what you received when you did a "GET"
        myPyJapc.setValues(['BR1.ONO4L1/REF.TABLE.FUNC#VALUE','BR1.ONO12L1/REF.TABLE.FUNC#VALUE'], initialValues)

myMonitor = myPyJapc.PyJapcScoutMonitor(mySelector, signalsToMonitor, onValueReceived=myCallback, selectorOverride = mySelector,
                        groupStrategy = 'extended',
                        allowManyUpdatesPerCycle=False, strategyTimeout=1200, forceGetOnChangeAndConstantValues=False)

# saving data configuration
myMonitor.saveDataPath = './Data_pickle/'
myMonitor.saveata = False
myMonitor.saveDataFormat = 'pickledict' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'

# start acquisition
myMonitor.startMonitor()
'''
if 0:
    ## for controlling data acquisition:
    #
    myMonitor.saveData = True
    #
    myMonitor.saveData = False
    
    ## to stop the monitor
    myMonitor.stopMonitor()

'''
