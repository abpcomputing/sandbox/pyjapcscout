'''
Plotting a signal from subscription using matplotlib
'''

# import PyJapcScout
from pyjapcscout import PyJapcScout
import matplotlib.pyplot as plt
import numpy as np
import threading
import time


# start PyJapcScout and so incaify Python instance
myPyJapc = PyJapcScout(incaAcceleratorName='CTF')

# Get and RBAC tocken
#myPyJapc.rbacLogin()

# Make a subscription
signalsToMonitor = ['CK.PKI15A-ST/Samples']
mySelector = 'SCT.USER.SETUP'

# prepare figure, adding a threading lock to it
fig, ax = plt.subplots(1)
line = ax.plot(np.zeros(10))[0]
fig._lock = threading.Lock()
fig.show()

data = myPyJapc.getValues(signalsToMonitor, selectorOverride=mySelector)

# simple function to update the data of a line
def updatePlot(fig, line, xData, yData):
    with fig._lock:
        line.set_data(xData, yData)
        fig.axes[0].relim()
        fig.axes[0].autoscale_view()
        fig.canvas.draw()

# simple callback function
def myCallback(data, monitor, fig, line):
    signalName = list(data.keys())[0]
    yData = data[signalName]['value']['samples'].flatten()
    xData = data[signalName]['value']['firstSampleTime'] + data[signalName]['value']['samplingTrain']*np.arange(len(yData))
    updatePlot(fig, line, xData, yData)

# create a monitor
myMonitor = myPyJapc.createMonitor(signalsToMonitor, onValueReceived=lambda data, h: myCallback(data, h, fig, line), selectorOverride = mySelector)

# start monitoring
myMonitor.startMonitor()

#%%
# once you are done, stop monitoring...
myMonitor.stopMonitor()

