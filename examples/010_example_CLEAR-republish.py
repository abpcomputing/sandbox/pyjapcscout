'''
Profit of possibility to define a virtual device defined in CCDE to log on NXCALS a 

Based on RDA republisher by Michi Hostettler:
https://gitlab.cern.ch/acc-co/lhc/lhc-rda-republisher/-/tree/master/src/java/cern/lhc/rdarepublisher
and example use by Alexander Huschauer
https://gitlab.cern.ch/ahuschau/acc-app-wirescanner/-/blob/master/acc_app_wirescanner/rda_publishing/rda_publisher.py
'''

from pyjapcscout import PyJapcScout
import numpy as np

# Initialise PyJapc
myPyJapc = PyJapcScout(incaAcceleratorName='CTF', defaultSelector='SCT.USER.SETUP')
myPyJapc.rbacLogin(username='dgamba')

# GET  all necessary data to compute charge from Oasis signal
bctData = myPyJapc.getSimpleValue('CE.SCOPE61.CH01/Acquisition', completeOutput=True)
gainSet = myPyJapc.getSimpleValue('CA.BCM01GAIN/Setting#enumValue', selectorOverride='')
settings = myPyJapc.getSimpleValue('CA.BCMALLSET/Settings', selectorOverride='')
gain_meaning = dict(zip(settings['gainStrings'], settings['gainFactors']))

# Where to store the newly acquired data
ouputParameter = 'CA.BCMGUN-TEST/Settings#scalarDouble'

# define function to treat data
def treat_BCT_signal(bctData, gainSetting):
  avg_start = 20
  avg_end = 60
  turnsFactor = 10.0 # beam charge / Calibration charge

  assert bctData['value_units'] == 'V'
  #Get the signals in Volts
  signal = np.asarray(bctData['value'],'float')*bctData['sensitivity'] + bctData['offset']

  # compute charge
  chargeRaw = np.mean(signal.flatten()[avg_start:avg_end])
  charge = turnsFactor*(chargeRaw / gain_meaning[gainSetting])

  return charge

# compute charge form previously GET data
new_charge = treat_BCT_signal(bctData['value'], gainSet)
# copy headers from Oasis acquisition
new_time_stamps = { 'acqStamp': bctData['header']['acqStamp'], 'cycleStamp': bctData['header']['cycleStamp'] }

# re-inject newly computed value into "VIRTUAL" device
myPyJapc.setSimpleValue(ouputParameter, new_charge, dataFilter=new_time_stamps)

# in principle, that could have been logged on NXCALS as well... So let's GET the last value logged:
myPyJapc._getSimpleNXCALS(ouputParameter)

# NOTE: in the example above everything is done on default selector "SCT.USER.SETUP", so the data is logged on this user, but nothing prevents one to specify a different user (or no user)...
# NOTE2: CA.BCMGUN-TEST is a "virtual" device defined in CCDE and "hosted" by "lhc_rda_republisher" server. Both device and server could have disappeared by the time you run this example....
