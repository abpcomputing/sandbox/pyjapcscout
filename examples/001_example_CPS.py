'''
Getting inspired by an example of Foteini Asvesta from 16/02/21
but used for CPS.

TESTED.
'''

# import PyJapcScout
from pyjapcscout import PyJapcScout

# some other imports for user use
import glob


# start PyJapcScout and so incaify Python instance
# matlabJapc.staticINCAify('PSB');
myPyJapc = PyJapcScout(incaAcceleratorName='PS')

# Get and RBAC tocken
# matlabJapc.staticRBACAuthenticate()
myPyJapc.rbacLogin()

# Make a subscription
signalsToMonitor = [                       
                    'BR.QFO/LOG.OASIS.I_MEAS',
                    'BR.QDE/LOG.OASIS.I_MEAS',
                    'BR1.QCF/LOG.OASIS.I_MEAS', 
                    'BR1.QCD/LOG.OASIS.I_MEAS', 
                    'BR1.QCD3/LOG.OASIS.I_MEAS',                     
                    'BR1.QCD14/LOG.OASIS.I_MEAS', 
                    'BR2.QCF/LOG.OASIS.I_MEAS',                          
                    'BR2.QCD/LOG.OASIS.I_MEAS', 
                    'BR2.QCD3/LOG.OASIS.I_MEAS',                     
                    'BR2.QCD14/LOG.OASIS.I_MEAS', 
                    'BR3.QCF/LOG.OASIS.I_MEAS',                          
                    'BR3.QCD/LOG.OASIS.I_MEAS', 
                    'BR3.QCD3/LOG.OASIS.I_MEAS',                     
                    'BR3.QCD14/LOG.OASIS.I_MEAS', 
                    'BR4.QCF/LOG.OASIS.I_MEAS',                          
                    'BR4.QCD/LOG.OASIS.I_MEAS', 
                    'BR4.QCD3/LOG.OASIS.I_MEAS',                     
                    'BR4.QCD14/LOG.OASIS.I_MEAS', 
                    'BR14.MPS/LOG.OASIS.I_MEAS',
                    'BR23.MPS/LOG.OASIS.I_MEAS'
                    ]

signalsToMonitor = ['PR.BCT-ST/Samples#cycleUserName']

mySelector = 'CPS.USER.ZERO'

# in MATLAB one could define several monitors within one monitor object. This feature is not there in PyJapcScout              
#mon = ones(length(signalsToMonitor),1)';

# In MATLAB the callback function is typically in a separate file - not needed here!
#function  myCallback(data,h)

#myMonitor = matlabJapcMonitor(mySelector, ...
#    signalsToMonitor, @(data,h)myCallback(data,h),...
#    'Compensation',... % comment
#    mon);
#myMonitor.isTryingToGetValues=1; #NOT AVAILABLE IN PYJAPCSCOUT
#myMonitor.useFastStrategy(true); # -> groupStrategy = 'extended', instead of default 'standard'
def myCallback(data, h):
    #disp(['Shot ' num2str(length(dir('2021*.mat')))]);
    print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '/20*'))) )

    indx = len(glob.glob(h.saveDataPath + '/20*')) + 1

    if indx == 10:
        h.stopMonitor()
        print('Measurement finished and monitor stopped.')

myMonitor = myPyJapc.createMonitor(signalsToMonitor,
                                    onValueReceived=myCallback,
                                    selectorOverride = mySelector, # change selector
                                    groupStrategy = 'extended', # use the "extended" group subscription strategy also used in MATLAB
                                    allowManyUpdatesPerCycle=False, # if to allow for many updates for the very same cycle (for CTF3-like machines)
                                    strategyTimeout=1200, # maximum waiting time after the first device has returned a value to complete a "cycle" dataset
                                    forceGetOnChangeAndConstantValues=False, # sometimes necessary to avoid loosing "firstupdate" of "constant"/"onchange" values.
                                    dataSanityCheck=None) # additional "callback"-like function to validate an acquisition before calling the actual callback/save data

# saving data configuration
myMonitor.saveDataPath = './Data/'
myMonitor.saveData = False
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'

# start acquisition
myMonitor.startMonitor()

if 0:
    ## for controlling data acquisition:
    #
    myMonitor.saveData = True
    #
    myMonitor.saveData = False
    
    ## to stop the monitor
    myMonitor.stopMonitor()

